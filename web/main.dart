import 'dart:html';
import 'dart:math' as math;
import 'package:trotter/trotter.dart' show Combinations;
import 'package:setalpha/setalpha.dart';

final rand = math.Random();

/// The colors available.
final colors = [Color.indianRed, Color.darkOliveGreen, Color.indigo];

/// The sprite sheet.
final CanvasElement sprites = (() {
  final side = 200;
  var spritesheet = CanvasElement(width: side * 3, height: side * 3);

  for (int column = 0; column < 3; column++) {
    String col = colors[column];

    for (int row = 0; row < 3; row++) {
      spritesheet.context2D
        ..save()
        ..lineCap = "round"
        ..lineJoin = "round"
        ..lineWidth = 2
        ..translate((column + 0.5) * side, (row + 0.5) * side);

      int vertices = row + 3;

      void paint(num ratio, String fill, String stroke) {
        spritesheet.context2D
          ..beginPath()
          ..fillStyle = fill
          ..strokeStyle = stroke;

        var segments = vertices * 2;
        for (int k = 0; k < segments; k++) {
          var t = math.pi * 2 / segments * k,
              r = [
                    [0.22, 0.5],
                    [0.32, 0.5],
                    [0.38, 0.5]
                  ][row][k % 2] *
                  side *
                  ratio,
              x = math.cos(t) * r,
              y = math.sin(t) * r;
          if (k == 0) {
            spritesheet.context2D.moveTo(x, y);
          } else {
            spritesheet.context2D.lineTo(x, y);
          }
        }

        spritesheet.context2D
          ..closePath()
          ..fill()
          ..stroke();
      }

      paint(0.85, Color.white, Color.none);
      paint(0.85, setAlpha(col, 0.5), Color.none);

      for (var r in [0.65, 0.45]) {
        paint(r, setAlpha(Color.white, 0.25), Color.none);
      }

      paint(0.85, Color.none, col);

      paint(0.5, Color.white, col);

      paint(0.25, col, Color.none);

      spritesheet.context2D.restore();
    }
  }
  return spritesheet;
})();

/// The sizes (as radii) available.
final rings = [1, 0.75, 0.5];

/// The size indicator sprite.
final CanvasElement circles = (() {
  var subImageWidth = sprites.width ~/ 3,
      subImageHeight = sprites.height ~/ 3,
      canvas = CanvasElement(width: subImageWidth, height: subImageHeight);

  canvas.context2D
    ..save()
    ..translate(canvas.width ~/ 2, canvas.height ~/ 2);

  for (var r in rings) {
    canvas.context2D
      ..strokeStyle = Color.darkGray
      ..fillStyle = Color.white
      ..beginPath()
      ..ellipse(0, 0, (r - 0.1) * subImageWidth / 2,
          (r - 0.1) * subImageHeight / 2, 0, 0, math.pi * 2, false)
      ..fill()
      ..stroke();
  }
  canvas.context2D.restore();
  return canvas;
})();

/// The size indicator sprite for selected items.
final CanvasElement selectedCircles = (() {
  var subImageWidth = sprites.width ~/ 3,
      subImageHeight = sprites.height ~/ 3,
      canvas = CanvasElement(width: subImageWidth, height: subImageHeight);

  canvas.context2D
    ..save()
    ..translate(canvas.width ~/ 2, canvas.height ~/ 2);

  for (var r in rings) {
    canvas.context2D
      ..strokeStyle = Color.black
      ..lineWidth = 1
      ..fillStyle = Color.lightYellow
      ..beginPath()
      ..ellipse(0, 0, (r - 0.1) * subImageWidth / 2,
          (r - 0.1) * subImageHeight / 2, 0, 0, math.pi * 2, false)
      ..fill()
      ..stroke();
  }
  canvas.context2D.restore();
  return canvas;
})();

/// Particles for conversational messages.
final particle = ["no", "a", "two", "three"];

/// An item, with properties.
class Item {
  Item(this.index);

  Item.fromAttributes(List<int> attributes) {
    if (attributes.length != 4 ||
        attributes.any((attribute) => attribute < 0 || attribute >= 3))
      throw Exception("Not acceptable attributes: $attributes");

    var powers = [27, 9, 3, 1];
    index = List.generate(4, (i) => attributes[i] * powers[i])
        .fold(0, (a, b) => a + b);
  }

  /// The index of the item in an arranced deck of items.
  int index;

  /// Convenience property-from-index getter.
  int get color => index ~/ 27;

  /// Convenience property-from-index getter.
  int get shape => index % 27 ~/ 9;

  /// Convenience property-from-index getter.
  int get motion => index % 27 % 9 ~/ 3;

  /// Convenience property-from-index getter.
  int get size => index % 27 % 9 % 3;

  /// Equality check.
  bool isSameAs(Item that) {
    return this.index == that.index;
  }

  @override
  String toString() => [
        ["red", "green", "purple"][color],
        ["triangle", "square", "pentagram"][shape],
        ["still", "clockwise", "counterclockwise"][motion],
        ["large", "medium", "small"][size]
      ].join("-");
}

/// The game of *Falco Shapes*.
class Game {
  Game(Element parent) {
    // Game board.
    var boardContainer = DivElement()
      ..id = "board-container"
      ..innerHtml = "<div class='game-heading'>Board</div>";
    boardCanvas = CanvasElement(width: sprites.width, height: sprites.height)
      ..style.cursor = "pointer"
      ..onClick.listen(click);
    boardContainer.children.add(boardCanvas);

    // Game messenger.
    var messagesContainer = DivElement()
      ..id = "messages-container"
      ..innerHtml = "<div class='game-heading'>Messages</div>";
    messages = DivElement()..id = "messages";
    messagesContainer.children
      ..add(messages)
      ..add(ButtonElement()
        ..innerHtml = "Show lines."
        ..onClick.listen((_) {
          while (!linesIndices.isEmpty) {
            foundLinesIndices.add(linesIndices.removeLast().toList());
          }
          winCheck();
        }))
      ..add(ButtonElement()
        ..innerHtml = "Show planes."
        ..onClick.listen((_) {
          while (!planesIndices.isEmpty) {
            foundPlanesIndices.add(planesIndices.removeLast().toList());
          }
          winCheck();
        }));
    messageParts = List<String>();

    // Lines found display.
    var linesFoundContainer = DivElement()
      ..id = "lines-found-container"
      ..innerHtml = "<div class='game-heading'>Lines found</div>";
    foundLinesCanvas = CanvasElement();
    linesFoundContainer.children.add(foundLinesCanvas);

    // Planes found display.
    var planesFoundContainer = DivElement()
      ..id = "planes-found-container"
      ..innerHtml = "<div class='game-heading'>Planes found</div>";
    foundPlanesCanvas = CanvasElement();
    planesFoundContainer.children.add(foundPlanesCanvas);

    // Putting everything together...
    parent.children
      ..add(boardContainer)
      ..add(messagesContainer)
      ..add(linesFoundContainer)
      ..add(planesFoundContainer);

    // Initiation of lines, planes and selections.
    linesIndices = List<Set<int>>();
    foundLinesIndices = List<List<int>>();
    planesIndices = List<Set<int>>();
    foundPlanesIndices = List<List<int>>();
    selectedPieceIndices = Set<int>();

    reset();
    window.animationFrame.then(loop);
  }

  /// The messages display.
  DivElement messages;

  /// The record of messages for this game.
  List<String> messageParts;

  /// Graphics display canvas.
  CanvasElement boardCanvas, foundLinesCanvas, foundPlanesCanvas;

  /// The nine random items on the board.
  List<Item> items;

  /// The indices of the items currently selected.
  Set<int> selectedPieceIndices;

  /// The sets of items that form lines.
  List<Set<int>> linesIndices;

  /// The sets of items that form lines.
  List<Set<int>> planesIndices;

  /// The indices of found sets.
  List<List<int>> foundLinesIndices, foundPlanesIndices;

  /// Sets up a new game.
  void reset() {
    messageParts.clear();
    messages.innerHtml = "";
    chooseRandomItems();
    selectedPieceIndices.clear();
    foundPlanesIndices.clear();
    foundLinesIndices.clear();
  }

  /// Displays a message to the user.
  void showMessage(String html) {
    messageParts.add(html);
    if (messageParts.length > 20) {
      messageParts = messageParts.sublist(messageParts.length - 20);
    }
    messages.innerHtml = messageParts.join();
    messages.scrollTop = messages.scrollHeight;
  }

  /// The piece required to form a set from [pair].
  Item missingPiece(Iterable<Item> pair) {
    if (pair.length != 2) throw Exception("missingPiece expecting two pieces.");
    List<int> h(int Function(Item) getAttribute) {
      var list = [0, 1, 2]
        ..remove(getAttribute(pair.first))
        ..remove(getAttribute(pair.last));
      return list;
    }

    return Item.fromAttributes([
      h((p) => p.color),
      h((p) => p.shape),
      h((p) => p.motion),
      h((p) => p.size)
    ]
        .map((list) => list.length == 1
            ? list.first
            : ([0, 1, 2]..removeWhere((i) => list.contains(i))).first)
        .toList());
  }

  /// Whether [triplet] constitutes a set.
  bool isLine(List<Item> triplet) {
    if (triplet.length != 3) return false;
    return triplet[1].isSameAs(missingPiece([triplet.first, triplet.last]));
  }

  /// Find all lines in the random items.
  void findLines() {
    linesIndices.clear();
    for (var si in Combinations(3, [0, 1, 2, 3, 4, 5, 6, 7, 8])()) {
      var triplet = si.map((i) => items[i]).toList();
      if (isLine(triplet)) {
        linesIndices.add(Set<int>.from(si));
      }
    }
  }

  /// Whether [quadruplet] constitutes a plane.
  bool isPlane(List<Item> quadruplet) {
    if (quadruplet.length != 4) return false;
    bool plane = false;
    for (var pair in Combinations(2, quadruplet)()) {
      var pair1 = List<Item>.from(pair),
          missing1 = missingPiece(pair1),
          pair2 = List<Item>.from(quadruplet)
            ..removeWhere((p) => pair1.contains(p)),
          missing2 = missingPiece(pair2);
      if (missing1.isSameAs(missing2)) {
        plane = true;
        break;
      }
    }
    return plane;
  }

  /// Finds all planes in the random items.
  void findPlanes() {
    planesIndices.clear();
    for (var pi in Combinations(4, [0, 1, 2, 3, 4, 5, 6, 7, 8])()) {
      var quadruplet = pi.map((i) => items[i]).toList();
      if (isPlane(quadruplet)) {
        planesIndices.add(Set<int>.from(pi));
      }
    }
  }

  /// Response to the board canvas being clicked.
  void click(MouseEvent me) {
    if (linesIndices.length == 0 && planesIndices.length == 0) {
      reset();
      return;
    }
    var transformRatioX = boardCanvas.width / boardCanvas.clientWidth,
        transformRatioY = boardCanvas.height / boardCanvas.clientHeight,
        xPixel = me.offset.x * transformRatioX,
        yPixel = me.offset.y * transformRatioY,
        row = yPixel ~/ (boardCanvas.height ~/ 3),
        col = xPixel ~/ (boardCanvas.width ~/ 3),
        index = col + row * 3;

    //showMessage("<p>You clicked ${pieces[index]}.</p>");
    if (selectedPieceIndices.contains(index)) {
      selectedPieceIndices.remove(index);
    } else {
      selectedPieceIndices.add(index);
    }

    if (selectedPieceIndices.length == 3) {
      // Check whether a line has been found.
      var triplet = selectedPieceIndices.map((i) => items[i]).toList();
      if (isLine(triplet)) {
        bool alreadyFound = true;
        for (var s in linesIndices) {
          if (s.union(selectedPieceIndices).length == 3) {
            alreadyFound = false;
            break;
          }
        }
        if (alreadyFound) {
          //showMessage("<p>A line (already found) is selected.</p>");
        } else {
          showMessage("<p>You found <b>a line</b>! Good job!</p>");
          foundLinesIndices.add(selectedPieceIndices.toList());
          linesIndices
              .removeWhere((s) => s.union(selectedPieceIndices).length == 3);
          selectedPieceIndices.clear();
          winCheck();
        }
      }
    } else if (selectedPieceIndices.length == 4) {
      // Check whether a plane has been found.
      var quadruplet = selectedPieceIndices.map((i) => items[i]).toList();
      if (isPlane(quadruplet)) {
        bool alreadyFound = true;
        for (var p in planesIndices) {
          if (p.union(selectedPieceIndices).length == 4) {
            alreadyFound = false;
            break;
          }
        }
        if (alreadyFound) {
          //showMessage("<p>You have selected a plane (already found).</p>");
        } else {
          showMessage("<p>You found <b>a plane</b>! Good job!</p>");
          foundPlanesIndices.add(selectedPieceIndices.toList());
          planesIndices
              .removeWhere((p) => p.union(selectedPieceIndices).length == 4);
          selectedPieceIndices.clear();
          winCheck();
        }
      }
    }
  }

  /// The game loop.
  void loop(num t) {
    final msPerRevolution = 10000;

    // Clear the canvases...
    boardCanvas.context2D
      ..fillStyle = Color.white
      ..fillRect(0, 0, boardCanvas.width, boardCanvas.height);

    foundLinesCanvas.context2D
      ..fillStyle = Color.white
      ..fillRect(0, 0, foundLinesCanvas.width, foundLinesCanvas.height);

    foundPlanesCanvas.context2D
      ..fillStyle = Color.white
      ..fillRect(0, 0, foundPlanesCanvas.width, foundPlanesCanvas.height);

    // Helper to draw the pieces...
    void drawPiece(CanvasElement canvas, int index, int row, int col) {
      var subImageWidth = sprites.width ~/ 3,
          subImageHeight = sprites.height ~/ 3,
          x = (col + 0.5) * subImageWidth,
          y = (row + 0.5) * subImageHeight,
          piece = items[index],
          angle = t % msPerRevolution / msPerRevolution * math.pi * 2,
          rotation = [0, angle, -angle][piece.motion],
          dilation = rings[piece.size];

      canvas.context2D
        ..save()
        ..translate(x, y);

      if (selectedPieceIndices.contains(index)) {
        canvas.context2D.drawImage(
            selectedCircles, -subImageWidth ~/ 2, -subImageHeight ~/ 2);
      } else {
        canvas.context2D
            .drawImage(circles, -subImageWidth ~/ 2, -subImageHeight ~/ 2);
      }

      canvas.context2D
        ..rotate(rotation)
        ..shadowColor = setAlpha(Color.black, 0.2)
        ..shadowOffsetX = 6
        ..shadowOffsetY = 6
        ..shadowBlur = 8
        ..drawImageScaledFromSource(
            sprites,
            piece.color * subImageWidth,
            piece.shape * subImageHeight,
            subImageWidth,
            subImageHeight,
            -subImageWidth / 2 * dilation,
            -subImageHeight / 2 * dilation,
            subImageWidth * dilation,
            subImageHeight * dilation)
        ..restore();
    }

    // Draw the pieces on the board...
    for (int i = 0; i < items.length; i++) {
      drawPiece(boardCanvas, i, i ~/ 3, i % 3);
    }

    // Show the lines already found...
    for (var row = 0; row < foundLinesIndices.length; row++) {
      var foundSet = foundLinesIndices[row];
      for (var col = 0; col < foundSet.length; col++) {
        var index = foundSet[col];
        drawPiece(foundLinesCanvas, index, row, col);
      }
    }

    // Show the planes already found...
    for (var row = 0; row < foundPlanesIndices.length; row++) {
      var foundPlane = foundPlanesIndices[row];
      for (var col = 0; col < foundPlane.length; col++) {
        var index = foundPlane[col];
        drawPiece(foundPlanesCanvas, index, row, col);
      }
    }

    window.animationFrame.then(loop);
  }

  /// Places random items on the board.
  ///
  /// Nine random items are selected such that there are two
  /// or three lines and two or three planes contained in the items.
  void chooseRandomItems() {
    do {
      items = (List.generate(81, (i) => i)..shuffle())
          .take(9)
          .map((i) => Item(i))
          .toList();
      findLines();
      findPlanes();
    } while (linesIndices.length < 2 ||
        linesIndices.length > 3 ||
        planesIndices.length < 2 ||
        planesIndices.length > 3);

    showMessage(
        "<p class='fancy'>Falco Shapes</p><p class='fancy-sub'>an implementation of Marsha Falco's game of <em>Set</em><br>by Richard Ambler, August 2018</p><p>Welcome to a new game of Falco Shapes!</p><p>There are <b>${particle[linesIndices.length]} lines</b> and <b>${particle[planesIndices.length]} planes</b> hidden in the  pieces on the board.</p><p>Find them by clicking on the pieces!</p>");

    foundLinesCanvas
      ..width = boardCanvas.width ~/ 3 * 3
      ..height = boardCanvas.height ~/ 3 * linesIndices.length;

    foundPlanesCanvas
      ..width = boardCanvas.width ~/ 3 * 4
      ..height = boardCanvas.height ~/ 3 * planesIndices.length;
  }

  /// Checks whether all the lines and planes have been found.
  void winCheck() {
    if (linesIndices.length == 0 && planesIndices.length == 0) {
      showMessage(
          "<p>That's all the lines and planes!</p><p>Click the board to start a new game!</p>");
    } else {
      var l = linesIndices.length, p = planesIndices.length;
      showMessage(
          "<p>Now there are <b>${particle[l]} line${l == 1 ? "" : "s"}</b> and <b>${particle[p]} plane${p == 1 ? "" : "s"}</b> remaining.</p>");
    }
  }
}

main() => Game(querySelector("#game"));

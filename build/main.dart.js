(function(){var supportsDirectProtoAccess=function(){var z=function(){}
z.prototype={p:{}}
var y=new z()
if(!(y.__proto__&&y.__proto__.p===z.prototype.p))return false
try{if(typeof navigator!="undefined"&&typeof navigator.userAgent=="string"&&navigator.userAgent.indexOf("Chrome/")>=0)return true
if(typeof version=="function"&&version.length==0){var x=version()
if(/^\d+\.\d+\.\d+\.\d+$/.test(x))return true}}catch(w){}return false}()
function map(a){a=Object.create(null)
a.x=0
delete a.x
return a}var A=map()
var B=map()
var C=map()
var D=map()
var E=map()
var F=map()
var G=map()
var H=map()
var J=map()
var K=map()
var L=map()
var M=map()
var N=map()
var O=map()
var P=map()
var Q=map()
var R=map()
var S=map()
var T=map()
var U=map()
var V=map()
var W=map()
var X=map()
var Y=map()
var Z=map()
function I(){}init()
function setupProgram(a,b,c){"use strict"
function generateAccessor(b0,b1,b2){var g=b0.split("-")
var f=g[0]
var e=f.length
var d=f.charCodeAt(e-1)
var a0
if(g.length>1)a0=true
else a0=false
d=d>=60&&d<=64?d-59:d>=123&&d<=126?d-117:d>=37&&d<=43?d-27:0
if(d){var a1=d&3
var a2=d>>2
var a3=f=f.substring(0,e-1)
var a4=f.indexOf(":")
if(a4>0){a3=f.substring(0,a4)
f=f.substring(a4+1)}if(a1){var a5=a1&2?"r":""
var a6=a1&1?"this":"r"
var a7="return "+a6+"."+f
var a8=b2+".prototype.g"+a3+"="
var a9="function("+a5+"){"+a7+"}"
if(a0)b1.push(a8+"$reflectable("+a9+");\n")
else b1.push(a8+a9+";\n")}if(a2){var a5=a2&2?"r,v":"v"
var a6=a2&1?"this":"r"
var a7=a6+"."+f+"=v"
var a8=b2+".prototype.s"+a3+"="
var a9="function("+a5+"){"+a7+"}"
if(a0)b1.push(a8+"$reflectable("+a9+");\n")
else b1.push(a8+a9+";\n")}}return f}function defineClass(a4,a5){var g=[]
var f="function "+a4+"("
var e="",d=""
for(var a0=0;a0<a5.length;a0++){var a1=a5[a0]
if(a1.charCodeAt(0)==48){a1=a1.substring(1)
var a2=generateAccessor(a1,g,a4)
d+="this."+a2+" = null;\n"}else{var a2=generateAccessor(a1,g,a4)
var a3="p_"+a2
f+=e
e=", "
f+=a3
d+="this."+a2+" = "+a3+";\n"}}if(supportsDirectProtoAccess)d+="this."+"$deferredAction"+"();"
f+=") {\n"+d+"}\n"
f+=a4+".builtin$cls=\""+a4+"\";\n"
f+="$desc=$collectedClasses."+a4+"[1];\n"
f+=a4+".prototype = $desc;\n"
if(typeof defineClass.name!="string")f+=a4+".name=\""+a4+"\";\n"
f+=g.join("")
return f}var z=supportsDirectProtoAccess?function(d,e){var g=d.prototype
g.__proto__=e.prototype
g.constructor=d
g["$is"+d.name]=d
return convertToFastObject(g)}:function(){function tmp(){}return function(a1,a2){tmp.prototype=a2.prototype
var g=new tmp()
convertToSlowObject(g)
var f=a1.prototype
var e=Object.keys(f)
for(var d=0;d<e.length;d++){var a0=e[d]
g[a0]=f[a0]}g["$is"+a1.name]=a1
g.constructor=a1
a1.prototype=g
return g}}()
function finishClasses(a5){var g=init.allClasses
a5.combinedConstructorFunction+="return [\n"+a5.constructorsList.join(",\n  ")+"\n]"
var f=new Function("$collectedClasses",a5.combinedConstructorFunction)(a5.collected)
a5.combinedConstructorFunction=null
for(var e=0;e<f.length;e++){var d=f[e]
var a0=d.name
var a1=a5.collected[a0]
var a2=a1[0]
a1=a1[1]
g[a0]=d
a2[a0]=d}f=null
var a3=init.finishedClasses
function finishClass(c2){if(a3[c2])return
a3[c2]=true
var a6=a5.pending[c2]
if(a6&&a6.indexOf("+")>0){var a7=a6.split("+")
a6=a7[0]
var a8=a7[1]
finishClass(a8)
var a9=g[a8]
var b0=a9.prototype
var b1=g[c2].prototype
var b2=Object.keys(b0)
for(var b3=0;b3<b2.length;b3++){var b4=b2[b3]
if(!u.call(b1,b4))b1[b4]=b0[b4]}}if(!a6||typeof a6!="string"){var b5=g[c2]
var b6=b5.prototype
b6.constructor=b5
b6.$isc=b5
b6.$deferredAction=function(){}
return}finishClass(a6)
var b7=g[a6]
if(!b7)b7=existingIsolateProperties[a6]
var b5=g[c2]
var b6=z(b5,b7)
if(b0)b6.$deferredAction=mixinDeferredActionHelper(b0,b6)
if(Object.prototype.hasOwnProperty.call(b6,"%")){var b8=b6["%"].split(";")
if(b8[0]){var b9=b8[0].split("|")
for(var b3=0;b3<b9.length;b3++){init.interceptorsByTag[b9[b3]]=b5
init.leafTags[b9[b3]]=true}}if(b8[1]){b9=b8[1].split("|")
if(b8[2]){var c0=b8[2].split("|")
for(var b3=0;b3<c0.length;b3++){var c1=g[c0[b3]]
c1.$nativeSuperclassTag=b9[0]}}for(b3=0;b3<b9.length;b3++){init.interceptorsByTag[b9[b3]]=b5
init.leafTags[b9[b3]]=false}}b6.$deferredAction()}if(b6.$isC)b6.$deferredAction()}var a4=Object.keys(a5.pending)
for(var e=0;e<a4.length;e++)finishClass(a4[e])}function finishAddStubsHelper(){var g=this
while(!g.hasOwnProperty("$deferredAction"))g=g.__proto__
delete g.$deferredAction
var f=Object.keys(g)
for(var e=0;e<f.length;e++){var d=f[e]
var a0=d.charCodeAt(0)
var a1
if(d!=="^"&&d!=="$reflectable"&&a0!==43&&a0!==42&&(a1=g[d])!=null&&a1.constructor===Array&&d!=="<>")addStubs(g,a1,d,false,[])}convertToFastObject(g)
g=g.__proto__
g.$deferredAction()}function mixinDeferredActionHelper(d,e){var g
if(e.hasOwnProperty("$deferredAction"))g=e.$deferredAction
return function foo(){if(!supportsDirectProtoAccess)return
var f=this
while(!f.hasOwnProperty("$deferredAction"))f=f.__proto__
if(g)f.$deferredAction=g
else{delete f.$deferredAction
convertToFastObject(f)}d.$deferredAction()
f.$deferredAction()}}function processClassData(b2,b3,b4){b3=convertToSlowObject(b3)
var g
var f=Object.keys(b3)
var e=false
var d=supportsDirectProtoAccess&&b2!="c"
for(var a0=0;a0<f.length;a0++){var a1=f[a0]
var a2=a1.charCodeAt(0)
if(a1==="l"){processStatics(init.statics[b2]=b3.l,b4)
delete b3.l}else if(a2===43){w[g]=a1.substring(1)
var a3=b3[a1]
if(a3>0)b3[g].$reflectable=a3}else if(a2===42){b3[g].$D=b3[a1]
var a4=b3.$methodsWithOptionalArguments
if(!a4)b3.$methodsWithOptionalArguments=a4={}
a4[a1]=g}else{var a5=b3[a1]
if(a1!=="^"&&a5!=null&&a5.constructor===Array&&a1!=="<>")if(d)e=true
else addStubs(b3,a5,a1,false,[])
else g=a1}}if(e)b3.$deferredAction=finishAddStubsHelper
var a6=b3["^"],a7,a8,a9=a6
var b0=a9.split(";")
a9=b0[1]?b0[1].split(","):[]
a8=b0[0]
a7=a8.split(":")
if(a7.length==2){a8=a7[0]
var b1=a7[1]
if(b1)b3.$S=function(b5){return function(){return init.types[b5]}}(b1)}if(a8)b4.pending[b2]=a8
b4.combinedConstructorFunction+=defineClass(b2,a9)
b4.constructorsList.push(b2)
b4.collected[b2]=[m,b3]
i.push(b2)}function processStatics(a4,a5){var g=Object.keys(a4)
for(var f=0;f<g.length;f++){var e=g[f]
if(e==="^")continue
var d=a4[e]
var a0=e.charCodeAt(0)
var a1
if(a0===43){v[a1]=e.substring(1)
var a2=a4[e]
if(a2>0)a4[a1].$reflectable=a2
if(d&&d.length)init.typeInformation[a1]=d}else if(a0===42){m[a1].$D=d
var a3=a4.$methodsWithOptionalArguments
if(!a3)a4.$methodsWithOptionalArguments=a3={}
a3[e]=a1}else if(typeof d==="function"){m[a1=e]=d
h.push(e)}else if(d.constructor===Array)addStubs(m,d,e,true,h)
else{a1=e
processClassData(e,d,a5)}}}function addStubs(b6,b7,b8,b9,c0){var g=0,f=g,e=b7[g],d
if(typeof e=="string")d=b7[++g]
else{d=e
e=b8}if(typeof d=="number"){f=d
d=b7[++g]}b6[b8]=b6[e]=d
var a0=[d]
d.$stubName=b8
c0.push(b8)
for(g++;g<b7.length;g++){d=b7[g]
if(typeof d!="function")break
if(!b9)d.$stubName=b7[++g]
a0.push(d)
if(d.$stubName){b6[d.$stubName]=d
c0.push(d.$stubName)}}for(var a1=0;a1<a0.length;g++,a1++)a0[a1].$callName=b7[g]
var a2=b7[g]
b7=b7.slice(++g)
var a3=b7[0]
var a4=(a3&1)===1
a3=a3>>1
var a5=a3>>1
var a6=(a3&1)===1
var a7=a3===3
var a8=a3===1
var a9=b7[1]
var b0=a9>>1
var b1=(a9&1)===1
var b2=a5+b0
var b3=b7[2]
if(typeof b3=="number")b7[2]=b3+c
if(b>0){var b4=3
for(var a1=0;a1<b0;a1++){if(typeof b7[b4]=="number")b7[b4]=b7[b4]+b
b4++}for(var a1=0;a1<b2;a1++){b7[b4]=b7[b4]+b
b4++}}var b5=2*b0+a5+3
if(a2){d=tearOff(a0,f,b7,b9,b8,a4)
b6[b8].$getter=d
d.$getterStub=true
if(b9)c0.push(a2)
b6[a2]=d
a0.push(d)
d.$stubName=a2
d.$callName=null}}function tearOffGetter(d,e,f,g,a0){return a0?new Function("funcs","applyTrampolineIndex","reflectionInfo","name","H","c","return function tearOff_"+g+y+++"(x) {"+"if (c === null) c = "+"H.cb"+"("+"this, funcs, applyTrampolineIndex, reflectionInfo, false, [x], name);"+"return new c(this, funcs[0], x, name);"+"}")(d,e,f,g,H,null):new Function("funcs","applyTrampolineIndex","reflectionInfo","name","H","c","return function tearOff_"+g+y+++"() {"+"if (c === null) c = "+"H.cb"+"("+"this, funcs, applyTrampolineIndex, reflectionInfo, false, [], name);"+"return new c(this, funcs[0], null, name);"+"}")(d,e,f,g,H,null)}function tearOff(d,e,f,a0,a1,a2){var g
return a0?function(){if(g===void 0)g=H.cb(this,d,e,f,true,[],a1).prototype
return g}:tearOffGetter(d,e,f,a1,a2)}var y=0
if(!init.libraries)init.libraries=[]
if(!init.mangledNames)init.mangledNames=map()
if(!init.mangledGlobalNames)init.mangledGlobalNames=map()
if(!init.statics)init.statics=map()
if(!init.typeInformation)init.typeInformation=map()
var x=init.libraries
var w=init.mangledNames
var v=init.mangledGlobalNames
var u=Object.prototype.hasOwnProperty
var t=a.length
var s=map()
s.collected=map()
s.pending=map()
s.constructorsList=[]
s.combinedConstructorFunction="function $reflectable(fn){fn.$reflectable=1;return fn};\n"+"var $desc;\n"
for(var r=0;r<t;r++){var q=a[r]
var p=q[0]
var o=q[1]
var n=q[2]
var m=q[3]
var l=q[4]
var k=!!q[5]
var j=l&&l["^"]
if(j instanceof Array)j=j[0]
var i=[]
var h=[]
processStatics(l,s)
x.push([p,o,i,h,n,j,k,m])}finishClasses(s)}I.cc=function(){}
var dart=[["","",,H,{"^":"",je:{"^":"c;a"}}],["","",,J,{"^":"",
q:function(a){return void 0},
cf:function(a,b,c,d){return{i:a,p:b,e:c,x:d}},
bw:function(a){var z,y,x,w,v
z=a[init.dispatchPropertyName]
if(z==null)if($.cd==null){H.iw()
z=a[init.dispatchPropertyName]}if(z!=null){y=z.p
if(!1===y)return z.i
if(!0===y)return a
x=Object.getPrototypeOf(a)
if(y===x)return z.i
if(z.e===x)throw H.b(P.d9("Return interceptor for "+H.d(y(a,z))))}w=a.constructor
v=w==null?null:w[$.$get$bN()]
if(v!=null)return v
v=H.iC(a)
if(v!=null)return v
if(typeof a=="function")return C.E
y=Object.getPrototypeOf(a)
if(y==null)return C.r
if(y===Object.prototype)return C.r
if(typeof w=="function"){Object.defineProperty(w,$.$get$bN(),{value:C.k,enumerable:false,writable:true,configurable:true})
return C.k}return C.k},
C:{"^":"c;",
O:function(a,b){return a===b},
gD:function(a){return H.aF(a)},
h:["bC",function(a){return"Instance of '"+H.aG(a)+"'"}],
"%":"Blob|CanvasGradient|CanvasPattern|CanvasRenderingContext2D|Client|DOMError|DOMImplementation|File|MediaError|Navigator|NavigatorConcurrentHardware|NavigatorUserMediaError|OverconstrainedError|PositionError|Range|SQLError|SVGAnimatedLength|SVGAnimatedLengthList|SVGAnimatedNumber|SVGAnimatedNumberList|SVGAnimatedString|WindowClient"},
f8:{"^":"C;",
h:function(a){return String(a)},
gD:function(a){return a?519018:218159},
$ist:1},
f9:{"^":"C;",
O:function(a,b){return null==b},
h:function(a){return"null"},
gD:function(a){return 0},
$isD:1},
bO:{"^":"C;",
gD:function(a){return 0},
h:["bE",function(a){return String(a)}]},
fs:{"^":"bO;"},
b_:{"^":"bO;"},
aV:{"^":"bO;",
h:function(a){var z=a[$.$get$cu()]
if(z==null)return this.bE(a)
return"JavaScript function for "+H.d(J.ay(z))},
$S:function(){return{func:1,opt:[,,,,,,,,,,,,,,,,]}},
$isaR:1},
aS:{"^":"C;$ti",
j:function(a,b){H.p(b,H.h(a,0))
if(!!a.fixed$length)H.w(P.I("add"))
a.push(b)},
ao:function(a,b){var z
if(!!a.fixed$length)H.w(P.I("remove"))
for(z=0;z<a.length;++z)if(J.ax(a[z],b)){a.splice(z,1)
return!0}return!1},
ag:function(a,b,c){var z,y,x,w,v
H.f(b,{func:1,ret:P.t,args:[H.h(a,0)]})
z=[]
y=a.length
for(x=0;x<y;++x){w=a[x]
if(!b.$1(w))z.push(w)
if(a.length!==y)throw H.b(P.O(a))}v=z.length
if(v===y)return
this.si(a,v)
for(x=0;x<z.length;++x)a[x]=z[x]},
B:function(a,b){var z,y
H.H(b,"$iso",[H.h(a,0)],"$aso")
if(!!a.fixed$length)H.w(P.I("addAll"))
for(z=b.length,y=0;y<b.length;b.length===z||(0,H.b9)(b),++y)a.push(b[y])},
bm:function(a,b,c){var z=H.h(a,0)
return new H.P(a,H.f(b,{func:1,ret:c,args:[z]}),[z,c])},
a5:function(a,b){var z,y
z=new Array(a.length)
z.fixed$length=Array
for(y=0;y<a.length;++y)this.F(z,y,H.d(a[y]))
return z.join(b)},
N:function(a){return this.a5(a,"")},
ce:function(a,b,c,d){var z,y,x
H.p(b,d)
H.f(c,{func:1,ret:d,args:[d,H.h(a,0)]})
z=a.length
for(y=b,x=0;x<z;++x){y=c.$2(y,a[x])
if(a.length!==z)throw H.b(P.O(a))}return y},
A:function(a,b){if(b>>>0!==b||b>=a.length)return H.a(a,b)
return a[b]},
ad:function(a,b,c){if(b<0||b>a.length)throw H.b(P.a9(b,0,a.length,"start",null))
if(c==null)c=a.length
else if(c<b||c>a.length)throw H.b(P.a9(c,b,a.length,"end",null))
if(b===c)return H.l([],[H.h(a,0)])
return H.l(a.slice(b,c),[H.h(a,0)])},
aY:function(a,b){return this.ad(a,b,null)},
gJ:function(a){if(a.length>0)return a[0]
throw H.b(H.bg())},
gZ:function(a){var z=a.length
if(z>0)return a[z-1]
throw H.b(H.bg())},
a4:function(a,b){var z,y
H.f(b,{func:1,ret:P.t,args:[H.h(a,0)]})
z=a.length
for(y=0;y<z;++y){if(b.$1(a[y]))return!0
if(a.length!==z)throw H.b(P.O(a))}return!1},
bx:function(a,b){var z,y,x,w
if(!!a.immutable$list)H.w(P.I("shuffle"))
z=a.length
for(;z>1;){y=C.w.cm(z);--z
x=a.length
if(z>=x)return H.a(a,z)
w=a[z]
if(y<0||y>=x)return H.a(a,y)
this.F(a,z,a[y])
this.F(a,y,w)}},
bw:function(a){return this.bx(a,null)},
v:function(a,b){var z
for(z=0;z<a.length;++z)if(J.ax(a[z],b))return!0
return!1},
h:function(a){return P.bL(a,"[","]")},
gC:function(a){return new J.bE(a,a.length,0,[H.h(a,0)])},
gD:function(a){return H.aF(a)},
gi:function(a){return a.length},
si:function(a,b){if(!!a.fixed$length)H.w(P.I("set length"))
if(b<0)throw H.b(P.a9(b,0,null,"newLength",null))
a.length=b},
m:function(a,b){if(typeof b!=="number"||Math.floor(b)!==b)throw H.b(H.ac(a,b))
if(b>=a.length||b<0)throw H.b(H.ac(a,b))
return a[b]},
F:function(a,b,c){H.z(b)
H.p(c,H.h(a,0))
if(!!a.immutable$list)H.w(P.I("indexed set"))
if(typeof b!=="number"||Math.floor(b)!==b)throw H.b(H.ac(a,b))
if(b>=a.length||b<0)throw H.b(H.ac(a,b))
a[b]=c},
$iso:1,
$ism:1,
l:{
f7:function(a,b){return J.aE(H.l(a,[b]))},
aE:function(a){H.by(a)
a.fixed$length=Array
return a}}},
jd:{"^":"aS;$ti"},
bE:{"^":"c;a,b,c,0d,$ti",
gu:function(){return this.d},
n:function(){var z,y,x
z=this.a
y=z.length
if(this.b!==y)throw H.b(H.b9(z))
x=this.c
if(x>=y){this.d=null
return!1}this.d=z[x]
this.c=x+1
return!0}},
aT:{"^":"C;",
a8:function(a){var z
if(a>=-2147483648&&a<=2147483647)return a|0
if(isFinite(a)){z=a<0?Math.ceil(a):Math.floor(a)
return z+0}throw H.b(P.I(""+a+".toInt()"))},
a7:function(a){if(a>0){if(a!==1/0)return Math.round(a)}else if(a>-1/0)return 0-Math.round(0-a)
throw H.b(P.I(""+a+".round()"))},
aa:function(a,b){var z,y,x,w
if(b<2||b>36)throw H.b(P.a9(b,2,36,"radix",null))
z=a.toString(b)
if(C.c.aN(z,z.length-1)!==41)return z
y=/^([\da-z]+)(?:\.([\da-z]+))?\(e\+(\d+)\)$/.exec(z)
if(y==null)H.w(P.I("Unexpected toString result: "+z))
x=J.ad(y)
z=x.m(y,1)
w=+x.m(y,3)
if(x.m(y,2)!=null){z+=x.m(y,2)
w-=x.m(y,2).length}return z+C.c.G("0",w)},
h:function(a){if(a===0&&1/a<0)return"-0.0"
else return""+a},
gD:function(a){return a&0x1FFFFFFF},
a_:function(a,b){if(typeof b!=="number")throw H.b(H.a1(b))
return a/b},
G:function(a,b){if(typeof b!=="number")throw H.b(H.a1(b))
return a*b},
w:function(a,b){var z=a%b
if(z===0)return 0
if(z>0)return z
if(b<0)return z-b
else return z+b},
t:function(a,b){if((a|0)===a)if(b>=1||b<-1)return a/b|0
return this.bc(a,b)},
k:function(a,b){return(a|0)===a?a/b|0:this.bc(a,b)},
bc:function(a,b){var z=a/b
if(z>=-2147483648&&z<=2147483647)return z|0
if(z>0){if(z!==1/0)return Math.floor(z)}else if(z>-1/0)return Math.ceil(z)
throw H.b(P.I("Result of truncating division is "+H.d(z)+": "+H.d(a)+" ~/ "+b))},
L:function(a,b){if(b<0)throw H.b(H.a1(b))
return b>31?0:a<<b>>>0},
ac:function(a,b){var z
if(b<0)throw H.b(H.a1(b))
if(a>0)z=this.aK(a,b)
else{z=b>31?31:b
z=a>>z>>>0}return z},
a3:function(a,b){var z
if(a>0)z=this.aK(a,b)
else{z=b>31?31:b
z=a>>z>>>0}return z},
aL:function(a,b){if(b<0)throw H.b(H.a1(b))
return this.aK(a,b)},
aK:function(a,b){return b>31?0:a>>>b},
aW:function(a,b){return(a&b)>>>0},
S:function(a,b){if(typeof b!=="number")throw H.b(H.a1(b))
return a<b},
ab:function(a,b){if(typeof b!=="number")throw H.b(H.a1(b))
return a>b},
$isaO:1,
$isr:1},
cG:{"^":"aT;",
gai:function(a){var z,y,x
z=a<0?-a-1:a
for(y=32;z>=4294967296;){z=this.k(z,4294967296)
y+=32}x=z|z>>1
x|=x>>2
x|=x>>4
x|=x>>8
x=(x|x>>16)>>>0
x=(x>>>0)-(x>>>1&1431655765)
x=(x&858993459)+(x>>>2&858993459)
x=252645135&x+(x>>>4)
x+=x>>>8
return y-(32-(x+(x>>>16)&63))},
$isk:1},
cF:{"^":"aT;"},
aU:{"^":"C;",
aN:function(a,b){if(b<0)throw H.b(H.ac(a,b))
if(b>=a.length)H.w(H.ac(a,b))
return a.charCodeAt(b)},
av:function(a,b){if(b>=a.length)throw H.b(H.ac(a,b))
return a.charCodeAt(b)},
H:function(a,b){H.u(b)
if(typeof b!=="string")throw H.b(P.co(b,null,null))
return a+b},
bz:function(a,b,c){var z
if(c>a.length)throw H.b(P.a9(c,0,a.length,null,null))
z=c+b.length
if(z>a.length)return!1
return b===a.substring(c,z)},
by:function(a,b){return this.bz(a,b,0)},
W:function(a,b,c){H.z(c)
if(c==null)c=a.length
if(b<0)throw H.b(P.bi(b,null,null))
if(b>c)throw H.b(P.bi(b,null,null))
if(c>a.length)throw H.b(P.bi(c,null,null))
return a.substring(b,c)},
bA:function(a,b){return this.W(a,b,null)},
cu:function(a){return a.toLowerCase()},
aU:function(a){var z,y,x,w,v
z=a.trim()
y=z.length
if(y===0)return z
if(this.av(z,0)===133){x=J.fa(z,1)
if(x===y)return""}else x=0
w=y-1
v=this.aN(z,w)===133?J.fb(z,w):y
if(x===0&&v===y)return z
return z.substring(x,v)},
G:function(a,b){var z,y
if(0>=b)return""
if(b===1||a.length===0)return a
if(b!==b>>>0)throw H.b(C.v)
for(z=a,y="";!0;){if((b&1)===1)y=z+y
b=b>>>1
if(b===0)break
z+=z}return y},
a6:function(a,b,c){var z=b-a.length
if(z<=0)return a
return this.G(c,z)+a},
bi:function(a,b,c){if(c>a.length)throw H.b(P.a9(c,0,a.length,null,null))
return H.iI(a,b,c)},
v:function(a,b){return this.bi(a,b,0)},
h:function(a){return a},
gD:function(a){var z,y,x
for(z=a.length,y=0,x=0;x<z;++x){y=536870911&y+a.charCodeAt(x)
y=536870911&y+((524287&y)<<10)
y^=y>>6}y=536870911&y+((67108863&y)<<3)
y^=y>>11
return 536870911&y+((16383&y)<<15)},
gi:function(a){return a.length},
$iscS:1,
$isj:1,
l:{
cH:function(a){if(a<256)switch(a){case 9:case 10:case 11:case 12:case 13:case 32:case 133:case 160:return!0
default:return!1}switch(a){case 5760:case 8192:case 8193:case 8194:case 8195:case 8196:case 8197:case 8198:case 8199:case 8200:case 8201:case 8202:case 8232:case 8233:case 8239:case 8287:case 12288:case 65279:return!0
default:return!1}},
fa:function(a,b){var z,y
for(z=a.length;b<z;){y=C.c.av(a,b)
if(y!==32&&y!==13&&!J.cH(y))break;++b}return b},
fb:function(a,b){var z,y
for(;b>0;b=z){z=b-1
y=C.c.aN(a,z)
if(y!==32&&y!==13&&!J.cH(y))break}return b}}}}],["","",,H,{"^":"",
bg:function(){return new P.bV("No element")},
f6:function(){return new P.bV("Too many elements")},
bJ:{"^":"o;"},
aq:{"^":"bJ;$ti",
gC:function(a){return new H.cL(this,this.gi(this),0,[H.af(this,"aq",0)])},
a5:function(a,b){var z,y,x,w
z=this.gi(this)
if(b.length!==0){if(z===0)return""
y=H.d(this.A(0,0))
if(z!==this.gi(this))throw H.b(P.O(this))
for(x=y,w=1;w<z;++w){x=x+b+H.d(this.A(0,w))
if(z!==this.gi(this))throw H.b(P.O(this))}return x.charCodeAt(0)==0?x:x}else{for(w=0,x="";w<z;++w){x+=H.d(this.A(0,w))
if(z!==this.gi(this))throw H.b(P.O(this))}return x.charCodeAt(0)==0?x:x}},
N:function(a){return this.a5(a,"")},
aV:function(a,b){return this.bD(0,H.f(b,{func:1,ret:P.t,args:[H.af(this,"aq",0)]}))},
a9:function(a,b){var z,y
z=H.l([],[H.af(this,"aq",0)])
C.b.si(z,this.gi(this))
for(y=0;y<this.gi(this);++y)C.b.F(z,y,this.A(0,y))
return z},
E:function(a){return this.a9(a,!0)}},
fH:{"^":"aq;a,b,c,$ti",
gbU:function(){var z,y
z=J.T(this.a)
y=this.c
if(y==null||y>z)return z
return y},
gc2:function(){var z,y
z=J.T(this.a)
y=this.b
if(y>z)return z
return y},
gi:function(a){var z,y,x
z=J.T(this.a)
y=this.b
if(y>=z)return 0
x=this.c
if(x==null||x>=z)return z-y
if(typeof x!=="number")return x.I()
return x-y},
A:function(a,b){var z,y
z=this.gc2()
if(typeof b!=="number")return H.a2(b)
y=z+b
if(b>=0){z=this.gbU()
if(typeof z!=="number")return H.a2(z)
z=y>=z}else z=!0
if(z)throw H.b(P.ap(b,this,"index",null,null))
return J.ba(this.a,y)},
l:{
fI:function(a,b,c,d){if(c!=null)if(b>c)H.w(P.a9(b,0,c,"start",null))
return new H.fH(a,b,c,[d])}}},
cL:{"^":"c;a,b,c,0d,$ti",
gu:function(){return this.d},
n:function(){var z,y,x,w
z=this.a
y=J.ad(z)
x=y.gi(z)
if(this.b!==x)throw H.b(P.O(z))
w=this.c
if(w>=x){this.d=null
return!1}this.d=y.A(z,w);++this.c
return!0}},
cN:{"^":"o;a,b,$ti",
gC:function(a){return new H.fi(J.S(this.a),this.b,this.$ti)},
gi:function(a){return J.T(this.a)},
A:function(a,b){return this.b.$1(J.ba(this.a,b))},
$aso:function(a,b){return[b]}},
cA:{"^":"cN;a,b,$ti"},
fi:{"^":"bM;0a,b,c,$ti",
n:function(){var z=this.b
if(z.n()){this.a=this.c.$1(z.gu())
return!0}this.a=null
return!1},
gu:function(){return this.a},
$asbM:function(a,b){return[b]}},
P:{"^":"aq;a,b,$ti",
gi:function(a){return J.T(this.a)},
A:function(a,b){return this.b.$1(J.ba(this.a,b))},
$asaq:function(a,b){return[b]},
$aso:function(a,b){return[b]}},
bY:{"^":"o;a,b,$ti",
gC:function(a){return new H.fR(J.S(this.a),this.b,this.$ti)}},
fR:{"^":"bM;a,b,$ti",
n:function(){var z,y
for(z=this.a,y=this.b;z.n();)if(y.$1(z.gu()))return!0
return!1},
gu:function(){return this.a.gu()}},
bf:{"^":"c;$ti"},
fy:{"^":"aq;a,$ti",
gi:function(a){return J.T(this.a)},
A:function(a,b){var z,y,x
z=this.a
y=J.ad(z)
x=y.gi(z)
if(typeof b!=="number")return H.a2(b)
return y.A(z,x-1-b)}}}],["","",,H,{"^":"",
ip:function(a){return init.types[H.z(a)]},
iA:function(a,b){var z
if(b!=null){z=b.x
if(z!=null)return z}return!!J.q(a).$isa7},
d:function(a){var z
if(typeof a==="string")return a
if(typeof a==="number"){if(a!==0)return""+a}else if(!0===a)return"true"
else if(!1===a)return"false"
else if(a==null)return"null"
z=J.ay(a)
if(typeof z!=="string")throw H.b(H.a1(a))
return z},
aF:function(a){var z=a.$identityHash
if(z==null){z=Math.random()*0x3fffffff|0
a.$identityHash=z}return z},
fu:function(a,b){var z,y
z=/^\s*[+-]?((0x[a-f0-9]+)|(\d+)|([a-z0-9]+))\s*$/i.exec(a)
if(z==null)return
if(3>=z.length)return H.a(z,3)
y=H.u(z[3])
if(y!=null)return parseInt(a,10)
if(z[2]!=null)return parseInt(a,16)
return},
ft:function(a){var z,y
if(!/^\s*[+-]?(?:Infinity|NaN|(?:\.\d+|\d+(?:\.\d*)?)(?:[eE][+-]?\d+)?)\s*$/.test(a))return
z=parseFloat(a)
if(isNaN(z)){y=C.c.aU(a)
if(y==="NaN"||y==="+NaN"||y==="-NaN")return z
return}return z},
aG:function(a){var z,y,x,w,v,u,t,s,r
z=J.q(a)
y=z.constructor
if(typeof y=="function"){x=y.name
w=typeof x==="string"?x:null}else w=null
if(w==null||z===C.x||!!J.q(a).$isb_){v=C.p(a)
if(v==="Object"){u=a.constructor
if(typeof u=="function"){t=String(u).match(/^\s*function\s*([\w$]*)\s*\(/)
s=t==null?null:t[1]
if(typeof s==="string"&&/^\w+$/.test(s))w=s}if(w==null)w=v}else w=v}w=w
if(w.length>1&&C.c.av(w,0)===36)w=C.c.bA(w,1)
r=H.ce(H.by(H.ak(a)),0,null)
return function(b,c){return b.replace(/[^<,> ]+/g,function(d){return c[d]||d})}(w+r,init.mangledGlobalNames)},
a2:function(a){throw H.b(H.a1(a))},
a:function(a,b){if(a==null)J.T(a)
throw H.b(H.ac(a,b))},
ac:function(a,b){var z,y
if(typeof b!=="number"||Math.floor(b)!==b)return new P.a3(!0,b,"index",null)
z=H.z(J.T(a))
if(!(b<0)){if(typeof z!=="number")return H.a2(z)
y=b>=z}else y=!0
if(y)return P.ap(b,a,"index",null,z)
return P.bi(b,"index",null)},
a1:function(a){return new P.a3(!0,a,null,null)},
b:function(a){var z
if(a==null)a=new P.cR()
z=new Error()
z.dartException=a
if("defineProperty" in Object){Object.defineProperty(z,"message",{get:H.e4})
z.name=""}else z.toString=H.e4
return z},
e4:function(){return J.ay(this.dartException)},
w:function(a){throw H.b(a)},
b9:function(a){throw H.b(P.O(a))},
V:function(a){var z,y,x,w,v,u,t,s,r,q,p,o,n,m,l
z=new H.iK(a)
if(a==null)return
if(typeof a!=="object")return a
if("dartException" in a)return z.$1(a.dartException)
else if(!("message" in a))return a
y=a.message
if("number" in a&&typeof a.number=="number"){x=a.number
w=x&65535
if((C.a.a3(x,16)&8191)===10)switch(w){case 438:return z.$1(H.bP(H.d(y)+" (Error "+w+")",null))
case 445:case 5007:return z.$1(H.cQ(H.d(y)+" (Error "+w+")",null))}}if(a instanceof TypeError){v=$.$get$cZ()
u=$.$get$d_()
t=$.$get$d0()
s=$.$get$d1()
r=$.$get$d5()
q=$.$get$d6()
p=$.$get$d3()
$.$get$d2()
o=$.$get$d8()
n=$.$get$d7()
m=v.K(y)
if(m!=null)return z.$1(H.bP(H.u(y),m))
else{m=u.K(y)
if(m!=null){m.method="call"
return z.$1(H.bP(H.u(y),m))}else{m=t.K(y)
if(m==null){m=s.K(y)
if(m==null){m=r.K(y)
if(m==null){m=q.K(y)
if(m==null){m=p.K(y)
if(m==null){m=s.K(y)
if(m==null){m=o.K(y)
if(m==null){m=n.K(y)
l=m!=null}else l=!0}else l=!0}else l=!0}else l=!0}else l=!0}else l=!0}else l=!0
if(l)return z.$1(H.cQ(H.u(y),m))}}return z.$1(new H.fP(typeof y==="string"?y:""))}if(a instanceof RangeError){if(typeof y==="string"&&y.indexOf("call stack")!==-1)return new P.cV()
y=function(b){try{return String(b)}catch(k){}return null}(a)
return z.$1(new P.a3(!1,null,null,typeof y==="string"?y.replace(/^RangeError:\s*/,""):y))}if(typeof InternalError=="function"&&a instanceof InternalError)if(typeof y==="string"&&y==="too much recursion")return new P.cV()
return a},
aP:function(a){var z
if(a==null)return new H.dB(a)
z=a.$cachedTrace
if(z!=null)return z
return a.$cachedTrace=new H.dB(a)},
iz:function(a,b,c,d,e,f){H.i(a,"$isaR")
switch(H.z(b)){case 0:return a.$0()
case 1:return a.$1(c)
case 2:return a.$2(c,d)
case 3:return a.$3(c,d,e)
case 4:return a.$4(c,d,e,f)}throw H.b(P.B("Unsupported number of arguments for wrapped closure"))},
aN:function(a,b){var z
H.z(b)
if(a==null)return
z=a.$identity
if(!!z)return z
z=function(c,d,e){return function(f,g,h,i){return e(c,d,f,g,h,i)}}(a,b,H.iz)
a.$identity=z
return z},
el:function(a,b,c,d,e,f,g){var z,y,x,w,v,u,t,s,r,q,p,o,n,m
z=b[0]
y=z.$callName
if(!!J.q(d).$ism){z.$reflectionInfo=d
x=H.fx(z).r}else x=d
w=e?Object.create(new H.fD().constructor.prototype):Object.create(new H.bF(null,null,null,null).constructor.prototype)
w.$initialize=w.constructor
if(e)v=function(){this.$initialize()}
else{u=$.W
if(typeof u!=="number")return u.H()
$.W=u+1
u=new Function("a,b,c,d"+u,"this.$initialize(a,b,c,d"+u+")")
v=u}w.constructor=v
v.prototype=w
if(!e){t=f.length==1&&!0
s=H.cs(a,z,t)
s.$reflectionInfo=d}else{w.$static_name=g
s=z
t=!1}if(typeof x=="number")r=function(h,i){return function(){return h(i)}}(H.ip,x)
else if(typeof x=="function")if(e)r=x
else{q=t?H.cr:H.bG
r=function(h,i){return function(){return h.apply({$receiver:i(this)},arguments)}}(x,q)}else throw H.b("Error in reflectionInfo.")
w.$S=r
w[y]=s
for(u=b.length,p=s,o=1;o<u;++o){n=b[o]
m=n.$callName
if(m!=null){n=e?n:H.cs(a,n,t)
w[m]=n}if(o===c){n.$reflectionInfo=d
p=n}}w["call*"]=p
w.$R=z.$R
w.$D=z.$D
return v},
ei:function(a,b,c,d){var z=H.bG
switch(b?-1:a){case 0:return function(e,f){return function(){return f(this)[e]()}}(c,z)
case 1:return function(e,f){return function(g){return f(this)[e](g)}}(c,z)
case 2:return function(e,f){return function(g,h){return f(this)[e](g,h)}}(c,z)
case 3:return function(e,f){return function(g,h,i){return f(this)[e](g,h,i)}}(c,z)
case 4:return function(e,f){return function(g,h,i,j){return f(this)[e](g,h,i,j)}}(c,z)
case 5:return function(e,f){return function(g,h,i,j,k){return f(this)[e](g,h,i,j,k)}}(c,z)
default:return function(e,f){return function(){return e.apply(f(this),arguments)}}(d,z)}},
cs:function(a,b,c){var z,y,x,w,v,u,t
if(c)return H.ek(a,b)
z=b.$stubName
y=b.length
x=a[z]
w=b==null?x==null:b===x
v=!w||y>=27
if(v)return H.ei(y,!w,z,b)
if(y===0){w=$.W
if(typeof w!=="number")return w.H()
$.W=w+1
u="self"+w
w="return function(){var "+u+" = this."
v=$.az
if(v==null){v=H.bd("self")
$.az=v}return new Function(w+H.d(v)+";return "+u+"."+H.d(z)+"();}")()}t="abcdefghijklmnopqrstuvwxyz".split("").splice(0,y).join(",")
w=$.W
if(typeof w!=="number")return w.H()
$.W=w+1
t+=w
w="return function("+t+"){return this."
v=$.az
if(v==null){v=H.bd("self")
$.az=v}return new Function(w+H.d(v)+"."+H.d(z)+"("+t+");}")()},
ej:function(a,b,c,d){var z,y
z=H.bG
y=H.cr
switch(b?-1:a){case 0:throw H.b(H.fA("Intercepted function with no arguments."))
case 1:return function(e,f,g){return function(){return f(this)[e](g(this))}}(c,z,y)
case 2:return function(e,f,g){return function(h){return f(this)[e](g(this),h)}}(c,z,y)
case 3:return function(e,f,g){return function(h,i){return f(this)[e](g(this),h,i)}}(c,z,y)
case 4:return function(e,f,g){return function(h,i,j){return f(this)[e](g(this),h,i,j)}}(c,z,y)
case 5:return function(e,f,g){return function(h,i,j,k){return f(this)[e](g(this),h,i,j,k)}}(c,z,y)
case 6:return function(e,f,g){return function(h,i,j,k,l){return f(this)[e](g(this),h,i,j,k,l)}}(c,z,y)
default:return function(e,f,g,h){return function(){h=[g(this)]
Array.prototype.push.apply(h,arguments)
return e.apply(f(this),h)}}(d,z,y)}},
ek:function(a,b){var z,y,x,w,v,u,t,s
z=$.az
if(z==null){z=H.bd("self")
$.az=z}y=$.cq
if(y==null){y=H.bd("receiver")
$.cq=y}x=b.$stubName
w=b.length
v=a[x]
u=b==null?v==null:b===v
t=!u||w>=28
if(t)return H.ej(w,!u,x,b)
if(w===1){z="return function(){return this."+H.d(z)+"."+H.d(x)+"(this."+H.d(y)+");"
y=$.W
if(typeof y!=="number")return y.H()
$.W=y+1
return new Function(z+y+"}")()}s="abcdefghijklmnopqrstuvwxyz".split("").splice(0,w-1).join(",")
z="return function("+s+"){return this."+H.d(z)+"."+H.d(x)+"(this."+H.d(y)+", "+s+");"
y=$.W
if(typeof y!=="number")return y.H()
$.W=y+1
return new Function(z+y+"}")()},
cb:function(a,b,c,d,e,f,g){var z,y
z=J.aE(H.by(b))
H.z(c)
y=!!J.q(d).$ism?J.aE(d):d
return H.el(a,z,c,y,!!e,f,g)},
u:function(a){if(a==null)return a
if(typeof a==="string")return a
throw H.b(H.Z(a,"String"))},
aQ:function(a){if(a==null)return a
if(typeof a==="number")return a
throw H.b(H.Z(a,"num"))},
ih:function(a){if(a==null)return a
if(typeof a==="boolean")return a
throw H.b(H.Z(a,"bool"))},
z:function(a){if(a==null)return a
if(typeof a==="number"&&Math.floor(a)===a)return a
throw H.b(H.Z(a,"int"))},
e0:function(a,b){throw H.b(H.Z(a,H.u(b).substring(3)))},
iG:function(a,b){var z=J.ad(b)
throw H.b(H.eh(a,z.W(b,3,z.gi(b))))},
i:function(a,b){if(a==null)return a
if((typeof a==="object"||typeof a==="function")&&J.q(a)[b])return a
H.e0(a,b)},
iy:function(a,b){var z
if(a!=null)z=(typeof a==="object"||typeof a==="function")&&J.q(a)[b]
else z=!0
if(z)return a
H.iG(a,b)},
by:function(a){if(a==null)return a
if(!!J.q(a).$ism)return a
throw H.b(H.Z(a,"List"))},
iB:function(a,b){if(a==null)return a
if(!!J.q(a).$ism)return a
if(J.q(a)[b])return a
H.e0(a,b)},
dT:function(a){var z
if("$S" in a){z=a.$S
if(typeof z=="number")return init.types[H.z(z)]
else return a.$S()}return},
b4:function(a,b){var z,y
if(a==null)return!1
if(typeof a=="function")return!0
z=H.dT(J.q(a))
if(z==null)return!1
y=H.dX(z,null,b,null)
return y},
f:function(a,b){var z,y
if(a==null)return a
if($.c7)return a
$.c7=!0
try{if(H.b4(a,b))return a
z=H.b7(b)
y=H.Z(a,z)
throw H.b(y)}finally{$.c7=!1}},
bu:function(a,b){if(a!=null&&!H.ca(a,b))H.w(H.Z(a,H.b7(b)))
return a},
dM:function(a){var z
if(a instanceof H.e){z=H.dT(J.q(a))
if(z!=null)return H.b7(z)
return"Closure"}return H.aG(a)},
iJ:function(a){throw H.b(new P.es(H.u(a)))},
dV:function(a){return init.getIsolateTag(a)},
l:function(a,b){a.$ti=b
return a},
ak:function(a){if(a==null)return
return a.$ti},
jP:function(a,b,c){return H.aw(a["$as"+H.d(c)],H.ak(b))},
b6:function(a,b,c,d){var z
H.u(c)
H.z(d)
z=H.aw(a["$as"+H.d(c)],H.ak(b))
return z==null?null:z[d]},
af:function(a,b,c){var z
H.u(b)
H.z(c)
z=H.aw(a["$as"+H.d(b)],H.ak(a))
return z==null?null:z[c]},
h:function(a,b){var z
H.z(b)
z=H.ak(a)
return z==null?null:z[b]},
b7:function(a){var z=H.am(a,null)
return z},
am:function(a,b){var z,y
H.H(b,"$ism",[P.j],"$asm")
if(a==null)return"dynamic"
if(a===-1)return"void"
if(typeof a==="object"&&a!==null&&a.constructor===Array)return a[0].builtin$cls+H.ce(a,1,b)
if(typeof a=="function")return a.builtin$cls
if(a===-2)return"dynamic"
if(typeof a==="number"){H.z(a)
if(b==null||a<0||a>=b.length)return"unexpected-generic-index:"+a
z=b.length
y=z-a-1
if(y<0||y>=z)return H.a(b,y)
return H.d(b[y])}if('func' in a)return H.i0(a,b)
if('futureOr' in a)return"FutureOr<"+H.am("type" in a?a.type:null,b)+">"
return"unknown-reified-type"},
i0:function(a,b){var z,y,x,w,v,u,t,s,r,q,p,o,n,m,l,k,j,i,h
z=[P.j]
H.H(b,"$ism",z,"$asm")
if("bounds" in a){y=a.bounds
if(b==null){b=H.l([],z)
x=null}else x=b.length
w=b.length
for(v=y.length,u=v;u>0;--u)C.b.j(b,"T"+(w+u))
for(t="<",s="",u=0;u<v;++u,s=", "){t+=s
z=b.length
r=z-u-1
if(r<0)return H.a(b,r)
t=C.c.H(t,b[r])
q=y[u]
if(q!=null&&q!==P.c)t+=" extends "+H.am(q,b)}t+=">"}else{t=""
x=null}p=!!a.v?"void":H.am(a.ret,b)
if("args" in a){o=a.args
for(z=o.length,n="",m="",l=0;l<z;++l,m=", "){k=o[l]
n=n+m+H.am(k,b)}}else{n=""
m=""}if("opt" in a){j=a.opt
n+=m+"["
for(z=j.length,m="",l=0;l<z;++l,m=", "){k=j[l]
n=n+m+H.am(k,b)}n+="]"}if("named" in a){i=a.named
n+=m+"{"
for(z=H.im(i),r=z.length,m="",l=0;l<r;++l,m=", "){h=H.u(z[l])
n=n+m+H.am(i[h],b)+(" "+H.d(h))}n+="}"}if(x!=null)b.length=x
return t+"("+n+") => "+p},
ce:function(a,b,c){var z,y,x,w,v,u
H.H(c,"$ism",[P.j],"$asm")
if(a==null)return""
z=new P.bX("")
for(y=b,x="",w=!0,v="";y<a.length;++y,x=", "){z.a=v+x
u=a[y]
if(u!=null)w=!1
v=z.a+=H.am(u,c)}v="<"+z.h(0)+">"
return v},
aw:function(a,b){if(a==null)return b
a=a.apply(null,b)
if(a==null)return
if(typeof a==="object"&&a!==null&&a.constructor===Array)return a
if(typeof a=="function")return a.apply(null,b)
return b},
aM:function(a,b,c,d){var z,y
if(a==null)return!1
z=H.ak(a)
y=J.q(a)
if(y[b]==null)return!1
return H.dP(H.aw(y[d],z),null,c,null)},
H:function(a,b,c,d){var z,y
H.u(b)
H.by(c)
H.u(d)
if(a==null)return a
z=H.aM(a,b,c,d)
if(z)return a
z=b.substring(3)
y=H.ce(c,0,null)
throw H.b(H.Z(a,function(e,f){return e.replace(/[^<,> ]+/g,function(g){return f[g]||g})}(z+y,init.mangledGlobalNames)))},
dP:function(a,b,c,d){var z,y
if(c==null)return!0
if(a==null){z=c.length
for(y=0;y<z;++y)if(!H.R(null,null,c[y],d))return!1
return!0}z=a.length
for(y=0;y<z;++y)if(!H.R(a[y],b,c[y],d))return!1
return!0},
jN:function(a,b,c){return a.apply(b,H.aw(J.q(b)["$as"+H.d(c)],H.ak(b)))},
dY:function(a){var z
if(typeof a==="number")return!1
if('futureOr' in a){z="type" in a?a.type:null
return a==null||a.builtin$cls==="c"||a.builtin$cls==="D"||a===-1||a===-2||H.dY(z)}return!1},
ca:function(a,b){var z,y,x
if(a==null){z=b==null||b.builtin$cls==="c"||b.builtin$cls==="D"||b===-1||b===-2||H.dY(b)
return z}z=b==null||b===-1||b.builtin$cls==="c"||b===-2
if(z)return!0
if(typeof b=="object"){z='futureOr' in b
if(z)if(H.ca(a,"type" in b?b.type:null))return!0
if('func' in b)return H.b4(a,b)}y=J.q(a).constructor
x=H.ak(a)
if(x!=null){x=x.slice()
x.splice(0,0,y)
y=x}z=H.R(y,null,b,null)
return z},
p:function(a,b){if(a!=null&&!H.ca(a,b))throw H.b(H.Z(a,H.b7(b)))
return a},
R:function(a,b,c,d){var z,y,x,w,v,u,t,s,r
if(a===c)return!0
if(c==null||c===-1||c.builtin$cls==="c"||c===-2)return!0
if(a===-2)return!0
if(a==null||a===-1||a.builtin$cls==="c"||a===-2){if(typeof c==="number")return!1
if('futureOr' in c)return H.R(a,b,"type" in c?c.type:null,d)
return!1}if(typeof a==="number")return!1
if(typeof c==="number")return!1
if(a.builtin$cls==="D")return!0
if('func' in c)return H.dX(a,b,c,d)
if('func' in a)return c.builtin$cls==="aR"
z=typeof a==="object"&&a!==null&&a.constructor===Array
y=z?a[0]:a
if('futureOr' in c){x="type" in c?c.type:null
if('futureOr' in a)return H.R("type" in a?a.type:null,b,x,d)
else if(H.R(a,b,x,d))return!0
else{if(!('$is'+"aC" in y.prototype))return!1
w=y.prototype["$as"+"aC"]
v=H.aw(w,z?a.slice(1):null)
return H.R(typeof v==="object"&&v!==null&&v.constructor===Array?v[0]:null,b,x,d)}}u=typeof c==="object"&&c!==null&&c.constructor===Array
t=u?c[0]:c
if(t!==y){s=H.b7(t)
if(!('$is'+s in y.prototype))return!1
r=y.prototype["$as"+s]}else r=null
if(!u)return!0
z=z?a.slice(1):null
u=c.slice(1)
return H.dP(H.aw(r,z),b,u,d)},
dX:function(a,b,c,d){var z,y,x,w,v,u,t,s,r,q,p,o,n,m,l
if(!('func' in a))return!1
if("bounds" in a){if(!("bounds" in c))return!1
z=a.bounds
y=c.bounds
if(z.length!==y.length)return!1}else if("bounds" in c)return!1
if(!H.R(a.ret,b,c.ret,d))return!1
x=a.args
w=c.args
v=a.opt
u=c.opt
t=x!=null?x.length:0
s=w!=null?w.length:0
r=v!=null?v.length:0
q=u!=null?u.length:0
if(t>s)return!1
if(t+r<s+q)return!1
for(p=0;p<t;++p)if(!H.R(w[p],d,x[p],b))return!1
for(o=p,n=0;o<s;++n,++o)if(!H.R(w[o],d,v[n],b))return!1
for(o=0;o<q;++n,++o)if(!H.R(u[o],d,v[n],b))return!1
m=a.named
l=c.named
if(l==null)return!0
if(m==null)return!1
return H.iE(m,b,l,d)},
iE:function(a,b,c,d){var z,y,x,w
z=Object.getOwnPropertyNames(c)
for(y=z.length,x=0;x<y;++x){w=z[x]
if(!Object.hasOwnProperty.call(a,w))return!1
if(!H.R(c[w],d,a[w],b))return!1}return!0},
jO:function(a,b,c){Object.defineProperty(a,H.u(b),{value:c,enumerable:false,writable:true,configurable:true})},
iC:function(a){var z,y,x,w,v,u
z=H.u($.dW.$1(a))
y=$.bt[z]
if(y!=null){Object.defineProperty(a,init.dispatchPropertyName,{value:y,enumerable:false,writable:true,configurable:true})
return y.i}x=$.bx[z]
if(x!=null)return x
w=init.interceptorsByTag[z]
if(w==null){z=H.u($.dO.$2(a,z))
if(z!=null){y=$.bt[z]
if(y!=null){Object.defineProperty(a,init.dispatchPropertyName,{value:y,enumerable:false,writable:true,configurable:true})
return y.i}x=$.bx[z]
if(x!=null)return x
w=init.interceptorsByTag[z]}}if(w==null)return
x=w.prototype
v=z[0]
if(v==="!"){y=H.bz(x)
$.bt[z]=y
Object.defineProperty(a,init.dispatchPropertyName,{value:y,enumerable:false,writable:true,configurable:true})
return y.i}if(v==="~"){$.bx[z]=x
return x}if(v==="-"){u=H.bz(x)
Object.defineProperty(Object.getPrototypeOf(a),init.dispatchPropertyName,{value:u,enumerable:false,writable:true,configurable:true})
return u.i}if(v==="+")return H.e_(a,x)
if(v==="*")throw H.b(P.d9(z))
if(init.leafTags[z]===true){u=H.bz(x)
Object.defineProperty(Object.getPrototypeOf(a),init.dispatchPropertyName,{value:u,enumerable:false,writable:true,configurable:true})
return u.i}else return H.e_(a,x)},
e_:function(a,b){var z=Object.getPrototypeOf(a)
Object.defineProperty(z,init.dispatchPropertyName,{value:J.cf(b,z,null,null),enumerable:false,writable:true,configurable:true})
return b},
bz:function(a){return J.cf(a,!1,null,!!a.$isa7)},
iD:function(a,b,c){var z=b.prototype
if(init.leafTags[a]===true)return H.bz(z)
else return J.cf(z,c,null,null)},
iw:function(){if(!0===$.cd)return
$.cd=!0
H.ix()},
ix:function(){var z,y,x,w,v,u,t,s
$.bt=Object.create(null)
$.bx=Object.create(null)
H.is()
z=init.interceptorsByTag
y=Object.getOwnPropertyNames(z)
if(typeof window!="undefined"){window
x=function(){}
for(w=0;w<y.length;++w){v=y[w]
u=$.e1.$1(v)
if(u!=null){t=H.iD(v,z[v],u)
if(t!=null){Object.defineProperty(u,init.dispatchPropertyName,{value:t,enumerable:false,writable:true,configurable:true})
x.prototype=u}}}}for(w=0;w<y.length;++w){v=y[w]
if(/^[A-Za-z_]/.test(v)){s=z[v]
z["!"+v]=s
z["~"+v]=s
z["-"+v]=s
z["+"+v]=s
z["*"+v]=s}}},
is:function(){var z,y,x,w,v,u,t
z=C.B()
z=H.au(C.y,H.au(C.D,H.au(C.o,H.au(C.o,H.au(C.C,H.au(C.z,H.au(C.A(C.p),z)))))))
if(typeof dartNativeDispatchHooksTransformer!="undefined"){y=dartNativeDispatchHooksTransformer
if(typeof y=="function")y=[y]
if(y.constructor==Array)for(x=0;x<y.length;++x){w=y[x]
if(typeof w=="function")z=w(z)||z}}v=z.getTag
u=z.getUnknownTag
t=z.prototypeForTag
$.dW=new H.it(v)
$.dO=new H.iu(u)
$.e1=new H.iv(t)},
au:function(a,b){return a(b)||b},
iI:function(a,b,c){var z=a.indexOf(b,c)
return z>=0},
av:function(a,b,c){var z
if(b instanceof H.cI){z=b.gbX()
z.lastIndex=0
return a.replace(z,c.replace(/\$/g,"$$$$"))}else{if(b==null)H.w(H.a1(b))
throw H.b("String.replaceAll(Pattern) UNIMPLEMENTED")}},
ep:{"^":"c;$ti",
h:function(a){return P.bS(this)},
$isbR:1},
eq:{"^":"ep;a,b,c,$ti",
gi:function(a){return this.a},
ak:function(a){if("__proto__"===a)return!1
return this.b.hasOwnProperty(a)},
m:function(a,b){if(!this.ak(b))return
return this.b5(b)},
b5:function(a){return this.b[H.u(a)]},
al:function(a,b){var z,y,x,w,v
z=H.h(this,1)
H.f(b,{func:1,ret:-1,args:[H.h(this,0),z]})
y=this.c
for(x=y.length,w=0;w<x;++w){v=y[w]
b.$2(v,H.p(this.b5(v),z))}}},
fw:{"^":"c;a,b,c,d,e,f,r,0x",l:{
fx:function(a){var z,y,x
z=a.$reflectionInfo
if(z==null)return
z=J.aE(z)
y=z[0]
x=z[1]
return new H.fw(a,z,(y&2)===2,y>>2,x>>1,(x&1)===1,z[2])}}},
fL:{"^":"c;a,b,c,d,e,f",
K:function(a){var z,y,x
z=new RegExp(this.a).exec(a)
if(z==null)return
y=Object.create(null)
x=this.b
if(x!==-1)y.arguments=z[x+1]
x=this.c
if(x!==-1)y.argumentsExpr=z[x+1]
x=this.d
if(x!==-1)y.expr=z[x+1]
x=this.e
if(x!==-1)y.method=z[x+1]
x=this.f
if(x!==-1)y.receiver=z[x+1]
return y},
l:{
Y:function(a){var z,y,x,w,v,u
a=a.replace(String({}),'$receiver$').replace(/[[\]{}()*+?.\\^$|]/g,"\\$&")
z=a.match(/\\\$[a-zA-Z]+\\\$/g)
if(z==null)z=H.l([],[P.j])
y=z.indexOf("\\$arguments\\$")
x=z.indexOf("\\$argumentsExpr\\$")
w=z.indexOf("\\$expr\\$")
v=z.indexOf("\\$method\\$")
u=z.indexOf("\\$receiver\\$")
return new H.fL(a.replace(new RegExp('\\\\\\$arguments\\\\\\$','g'),'((?:x|[^x])*)').replace(new RegExp('\\\\\\$argumentsExpr\\\\\\$','g'),'((?:x|[^x])*)').replace(new RegExp('\\\\\\$expr\\\\\\$','g'),'((?:x|[^x])*)').replace(new RegExp('\\\\\\$method\\\\\\$','g'),'((?:x|[^x])*)').replace(new RegExp('\\\\\\$receiver\\\\\\$','g'),'((?:x|[^x])*)'),y,x,w,v,u)},
bj:function(a){return function($expr$){var $argumentsExpr$='$arguments$'
try{$expr$.$method$($argumentsExpr$)}catch(z){return z.message}}(a)},
d4:function(a){return function($expr$){try{$expr$.$method$}catch(z){return z.message}}(a)}}},
fq:{"^":"F;a,b",
h:function(a){var z=this.b
if(z==null)return"NullError: "+H.d(this.a)
return"NullError: method not found: '"+z+"' on null"},
l:{
cQ:function(a,b){return new H.fq(a,b==null?null:b.method)}}},
fd:{"^":"F;a,b,c",
h:function(a){var z,y
z=this.b
if(z==null)return"NoSuchMethodError: "+H.d(this.a)
y=this.c
if(y==null)return"NoSuchMethodError: method not found: '"+z+"' ("+H.d(this.a)+")"
return"NoSuchMethodError: method not found: '"+z+"' on '"+y+"' ("+H.d(this.a)+")"},
l:{
bP:function(a,b){var z,y
z=b==null
y=z?null:b.method
return new H.fd(a,y,z?null:b.receiver)}}},
fP:{"^":"F;a",
h:function(a){var z=this.a
return z.length===0?"Error":"Error: "+z}},
iK:{"^":"e:13;a",
$1:function(a){if(!!J.q(a).$isF)if(a.$thrownJsError==null)a.$thrownJsError=this.a
return a}},
dB:{"^":"c;a,0b",
h:function(a){var z,y
z=this.b
if(z!=null)return z
z=this.a
y=z!==null&&typeof z==="object"?z.stack:null
z=y==null?"":y
this.b=z
return z},
$isU:1},
e:{"^":"c;",
h:function(a){return"Closure '"+H.aG(this).trim()+"'"},
gbt:function(){return this},
$isaR:1,
gbt:function(){return this}},
cX:{"^":"e;"},
fD:{"^":"cX;",
h:function(a){var z=this.$static_name
if(z==null)return"Closure of unknown static method"
return"Closure '"+z+"'"}},
bF:{"^":"cX;a,b,c,d",
O:function(a,b){if(b==null)return!1
if(this===b)return!0
if(!(b instanceof H.bF))return!1
return this.a===b.a&&this.b===b.b&&this.c===b.c},
gD:function(a){var z,y
z=this.c
if(z==null)y=H.aF(this.a)
else y=typeof z!=="object"?J.an(z):H.aF(z)
return(y^H.aF(this.b))>>>0},
h:function(a){var z=this.c
if(z==null)z=this.a
return"Closure '"+H.d(this.d)+"' of "+("Instance of '"+H.aG(z)+"'")},
l:{
bG:function(a){return a.a},
cr:function(a){return a.c},
bd:function(a){var z,y,x,w,v
z=new H.bF("self","target","receiver","name")
y=J.aE(Object.getOwnPropertyNames(z))
for(x=y.length,w=0;w<x;++w){v=y[w]
if(z[v]===a)return v}}}},
fM:{"^":"F;a",
h:function(a){return this.a},
l:{
Z:function(a,b){return new H.fM("TypeError: "+H.d(P.be(a))+": type '"+H.dM(a)+"' is not a subtype of type '"+b+"'")}}},
eg:{"^":"F;a",
h:function(a){return this.a},
l:{
eh:function(a,b){return new H.eg("CastError: "+H.d(P.be(a))+": type '"+H.dM(a)+"' is not a subtype of type '"+b+"'")}}},
fz:{"^":"F;a",
h:function(a){return"RuntimeError: "+H.d(this.a)},
l:{
fA:function(a){return new H.fz(a)}}},
fc:{"^":"cM;a,0b,0c,0d,0e,0f,r,$ti",
gi:function(a){return this.a},
gU:function(){return new H.ff(this,[H.h(this,0)])},
ak:function(a){var z,y
if(typeof a==="string"){z=this.b
if(z==null)return!1
return this.b2(z,a)}else if(typeof a==="number"&&(a&0x3ffffff)===a){y=this.c
if(y==null)return!1
return this.b2(y,a)}else return this.cg(a)},
cg:function(a){var z=this.d
if(z==null)return!1
return this.aO(this.aD(z,J.an(a)&0x3ffffff),a)>=0},
m:function(a,b){var z,y,x,w
if(typeof b==="string"){z=this.b
if(z==null)return
y=this.af(z,b)
x=y==null?null:y.b
return x}else if(typeof b==="number"&&(b&0x3ffffff)===b){w=this.c
if(w==null)return
y=this.af(w,b)
x=y==null?null:y.b
return x}else return this.ci(b)},
ci:function(a){var z,y,x
z=this.d
if(z==null)return
y=this.aD(z,J.an(a)&0x3ffffff)
x=this.aO(y,a)
if(x<0)return
return y[x].b},
F:function(a,b,c){var z,y,x,w,v,u
H.p(b,H.h(this,0))
H.p(c,H.h(this,1))
if(typeof b==="string"){z=this.b
if(z==null){z=this.aF()
this.b=z}this.aZ(z,b,c)}else if(typeof b==="number"&&(b&0x3ffffff)===b){y=this.c
if(y==null){y=this.aF()
this.c=y}this.aZ(y,b,c)}else{x=this.d
if(x==null){x=this.aF()
this.d=x}w=J.an(b)&0x3ffffff
v=this.aD(x,w)
if(v==null)this.aJ(x,w,[this.aG(b,c)])
else{u=this.aO(v,b)
if(u>=0)v[u].b=c
else v.push(this.aG(b,c))}}},
al:function(a,b){var z,y
H.f(b,{func:1,ret:-1,args:[H.h(this,0),H.h(this,1)]})
z=this.e
y=this.r
for(;z!=null;){b.$2(z.a,z.b)
if(y!==this.r)throw H.b(P.O(this))
z=z.c}},
aZ:function(a,b,c){var z
H.p(b,H.h(this,0))
H.p(c,H.h(this,1))
z=this.af(a,b)
if(z==null)this.aJ(a,b,this.aG(b,c))
else z.b=c},
bW:function(){this.r=this.r+1&67108863},
aG:function(a,b){var z,y
z=new H.fe(H.p(a,H.h(this,0)),H.p(b,H.h(this,1)))
if(this.e==null){this.f=z
this.e=z}else{y=this.f
z.d=y
y.c=z
this.f=z}++this.a
this.bW()
return z},
aO:function(a,b){var z,y
if(a==null)return-1
z=a.length
for(y=0;y<z;++y)if(J.ax(a[y].a,b))return y
return-1},
h:function(a){return P.bS(this)},
af:function(a,b){return a[b]},
aD:function(a,b){return a[b]},
aJ:function(a,b,c){a[b]=c},
bR:function(a,b){delete a[b]},
b2:function(a,b){return this.af(a,b)!=null},
aF:function(){var z=Object.create(null)
this.aJ(z,"<non-identifier-key>",z)
this.bR(z,"<non-identifier-key>")
return z}},
fe:{"^":"c;a,b,0c,0d"},
ff:{"^":"bJ;a,$ti",
gi:function(a){return this.a.a},
gC:function(a){var z,y
z=this.a
y=new H.fg(z,z.r,this.$ti)
y.c=z.e
return y}},
fg:{"^":"c;a,b,0c,0d,$ti",
gu:function(){return this.d},
n:function(){var z=this.a
if(this.b!==z.r)throw H.b(P.O(z))
else{z=this.c
if(z==null){this.d=null
return!1}else{this.d=z.a
this.c=z.c
return!0}}}},
it:{"^":"e:13;a",
$1:function(a){return this.a(a)}},
iu:{"^":"e:36;a",
$2:function(a,b){return this.a(a,b)}},
iv:{"^":"e:35;a",
$1:function(a){return this.a(H.u(a))}},
cI:{"^":"c;a,b,0c,0d",
h:function(a){return"RegExp/"+this.a+"/"},
gbX:function(){var z=this.c
if(z!=null)return z
z=this.b
z=H.cJ(this.a,z.multiline,!z.ignoreCase,!0)
this.c=z
return z},
$iscS:1,
l:{
cJ:function(a,b,c,d){var z,y,x,w
z=b?"m":""
y=c?"":"i"
x=d?"g":""
w=function(e,f){try{return new RegExp(e,f)}catch(v){return v}}(a,z+y+x)
if(w instanceof RegExp)return w
throw H.b(P.cE("Illegal RegExp pattern ("+String(w)+")",a,null))}}}}],["","",,H,{"^":"",
im:function(a){return J.f7(a?Object.keys(a):[],null)}}],["","",,H,{"^":"",
cO:function(a,b,c){var z=new DataView(a,b)
return z},
fm:function(a){return new Uint8Array(a)},
ab:function(a,b,c){if(a>>>0!==a||a>=c)throw H.b(H.ac(b,a))},
ji:{"^":"C;",$isiN:1,"%":"ArrayBuffer"},
fk:{"^":"C;","%":"DataView;ArrayBufferView;bT|dx|dy|fj|dz|dA|ai"},
bT:{"^":"fk;",
gi:function(a){return a.length},
$isa7:1,
$asa7:I.cc},
fj:{"^":"dy;",
m:function(a,b){H.ab(b,a,a.length)
return a[b]},
$asbf:function(){return[P.aO]},
$asE:function(){return[P.aO]},
$iso:1,
$aso:function(){return[P.aO]},
$ism:1,
$asm:function(){return[P.aO]},
"%":"Float32Array|Float64Array"},
ai:{"^":"dA;",
F:function(a,b,c){H.z(b)
H.z(c)
H.ab(b,a,a.length)
a[b]=c},
$asbf:function(){return[P.k]},
$asE:function(){return[P.k]},
$iso:1,
$aso:function(){return[P.k]},
$ism:1,
$asm:function(){return[P.k]}},
jj:{"^":"ai;",
m:function(a,b){H.ab(b,a,a.length)
return a[b]},
"%":"Int16Array"},
jk:{"^":"ai;",
m:function(a,b){H.ab(b,a,a.length)
return a[b]},
"%":"Int32Array"},
jl:{"^":"ai;",
m:function(a,b){H.ab(b,a,a.length)
return a[b]},
"%":"Int8Array"},
jm:{"^":"ai;",
m:function(a,b){H.ab(b,a,a.length)
return a[b]},
$isjy:1,
"%":"Uint16Array"},
jn:{"^":"ai;",
m:function(a,b){H.ab(b,a,a.length)
return a[b]},
"%":"Uint32Array"},
jo:{"^":"ai;",
gi:function(a){return a.length},
m:function(a,b){H.ab(b,a,a.length)
return a[b]},
"%":"CanvasPixelArray|Uint8ClampedArray"},
fl:{"^":"ai;",
gi:function(a){return a.length},
m:function(a,b){H.ab(b,a,a.length)
return a[b]},
"%":";Uint8Array"},
dx:{"^":"bT+E;"},
dy:{"^":"dx+bf;"},
dz:{"^":"bT+E;"},
dA:{"^":"dz+bf;"}}],["","",,P,{"^":"",
fU:function(){var z,y,x
z={}
if(self.scheduleImmediate!=null)return P.id()
if(self.MutationObserver!=null&&self.document!=null){y=self.document.createElement("div")
x=self.document.createElement("span")
z.a=null
new self.MutationObserver(H.aN(new P.fW(z),1)).observe(y,{childList:true})
return new P.fV(z,y,x)}else if(self.setImmediate!=null)return P.ie()
return P.ig()},
jC:[function(a){self.scheduleImmediate(H.aN(new P.fX(H.f(a,{func:1,ret:-1})),0))},"$1","id",4,0,5],
jD:[function(a){self.setImmediate(H.aN(new P.fY(H.f(a,{func:1,ret:-1})),0))},"$1","ie",4,0,5],
jE:[function(a){H.f(a,{func:1,ret:-1})
P.hQ(0,a)},"$1","ig",4,0,5],
i3:function(a,b){return new P.hM(a,[b])},
i5:function(a,b){if(H.b4(a,{func:1,args:[P.c,P.U]}))return b.co(a,null,P.c,P.U)
if(H.b4(a,{func:1,args:[P.c]}))return H.f(a,{func:1,ret:null,args:[P.c]})
throw H.b(P.co(a,"onError","Error handler must accept one Object or one Object and a StackTrace as arguments, and return a a valid result"))},
i4:function(){var z,y
for(;z=$.at,z!=null;){$.aK=null
y=z.b
$.at=y
if(y==null)$.aJ=null
z.a.$0()}},
jM:[function(){$.c8=!0
try{P.i4()}finally{$.aK=null
$.c8=!1
if($.at!=null)$.$get$bZ().$1(P.dQ())}},"$0","dQ",0,0,1],
dL:function(a){var z=new P.db(H.f(a,{func:1,ret:-1}))
if($.at==null){$.aJ=z
$.at=z
if(!$.c8)$.$get$bZ().$1(P.dQ())}else{$.aJ.b=z
$.aJ=z}},
ic:function(a){var z,y,x
H.f(a,{func:1,ret:-1})
z=$.at
if(z==null){P.dL(a)
$.aK=$.aJ
return}y=new P.db(a)
x=$.aK
if(x==null){y.b=z
$.aK=y
$.at=y}else{y.b=x.b
x.b=y
$.aK=y
if(y.b==null)$.aJ=y}},
iH:function(a){var z,y
z={func:1,ret:-1}
H.f(a,z)
y=$.A
if(C.d===y){P.bq(null,null,C.d,a)
return}y.toString
P.bq(null,null,y,H.f(y.bg(a),z))},
bp:function(a,b,c,d,e){var z={}
z.a=d
P.ic(new P.ia(z,e))},
dJ:function(a,b,c,d,e){var z,y
H.f(d,{func:1,ret:e})
y=$.A
if(y===c)return d.$0()
$.A=c
z=y
try{y=d.$0()
return y}finally{$.A=z}},
dK:function(a,b,c,d,e,f,g){var z,y
H.f(d,{func:1,ret:f,args:[g]})
H.p(e,g)
y=$.A
if(y===c)return d.$1(e)
$.A=c
z=y
try{y=d.$1(e)
return y}finally{$.A=z}},
ib:function(a,b,c,d,e,f,g,h,i){var z,y
H.f(d,{func:1,ret:g,args:[h,i]})
H.p(e,h)
H.p(f,i)
y=$.A
if(y===c)return d.$2(e,f)
$.A=c
z=y
try{y=d.$2(e,f)
return y}finally{$.A=z}},
bq:function(a,b,c,d){var z
H.f(d,{func:1,ret:-1})
z=C.d!==c
if(z)d=!(!z||!1)?c.bg(d):c.c7(d,-1)
P.dL(d)},
fW:{"^":"e:12;a",
$1:function(a){var z,y
z=this.a
y=z.a
z.a=null
y.$0()}},
fV:{"^":"e:34;a,b,c",
$1:function(a){var z,y
this.a.a=H.f(a,{func:1,ret:-1})
z=this.b
y=this.c
z.firstChild?z.removeChild(y):z.appendChild(y)}},
fX:{"^":"e:0;a",
$0:function(){this.a.$0()}},
fY:{"^":"e:0;a",
$0:function(){this.a.$0()}},
hP:{"^":"c;a,0b,c",
bK:function(a,b){if(self.setTimeout!=null)this.b=self.setTimeout(H.aN(new P.hR(this,b),0),a)
else throw H.b(P.I("`setTimeout()` not found."))},
l:{
hQ:function(a,b){var z=new P.hP(!0,0)
z.bK(a,b)
return z}}},
hR:{"^":"e:1;a,b",
$0:function(){var z=this.a
z.b=null
z.c=1
this.b.$0()}},
bm:{"^":"c;a,b",
h:function(a){return"IterationMarker("+this.b+", "+H.d(this.a)+")"},
l:{
jJ:function(a){return new P.bm(a,1)},
hu:function(){return C.L},
hv:function(a){return new P.bm(a,3)}}},
dC:{"^":"c;a,0b,0c,0d,$ti",
gu:function(){var z=this.c
if(z==null)return this.b
return H.p(z.gu(),H.h(this,0))},
n:function(){var z,y,x,w
for(;!0;){z=this.c
if(z!=null)if(z.n())return!0
else this.c=null
y=function(a,b,c){var v,u=b
while(true)try{return a(u,v)}catch(t){v=t
u=c}}(this.a,0,1)
if(y instanceof P.bm){x=y.b
if(x===2){z=this.d
if(z==null||z.length===0){this.b=null
return!1}if(0>=z.length)return H.a(z,-1)
this.a=z.pop()
continue}else{z=y.a
if(x===3)throw z
else{w=J.S(z)
if(!!w.$isdC){z=this.d
if(z==null){z=[]
this.d=z}C.b.j(z,this.a)
this.a=w.a
continue}else{this.c=w
continue}}}}else{this.b=y
return!0}}return!1}},
hM:{"^":"f4;a,$ti",
gC:function(a){return new P.dC(this.a(),this.$ti)}},
h8:{"^":"c;$ti"},
hL:{"^":"h8;a,$ti"},
as:{"^":"c;0a,b,c,d,e,$ti",
cl:function(a){if(this.c!==6)return!0
return this.b.b.aR(H.f(this.d,{func:1,ret:P.t,args:[P.c]}),a.a,P.t,P.c)},
cf:function(a){var z,y,x,w
z=this.e
y=P.c
x={futureOr:1,type:H.h(this,1)}
w=this.b.b
if(H.b4(z,{func:1,args:[P.c,P.U]}))return H.bu(w.cq(z,a.a,a.b,null,y,P.U),x)
else return H.bu(w.aR(H.f(z,{func:1,args:[P.c]}),a.a,null,y),x)}},
a_:{"^":"c;bb:a<,b,0c_:c<,$ti",
bq:function(a,b,c){var z,y,x,w
z=H.h(this,0)
H.f(a,{func:1,ret:{futureOr:1,type:c},args:[z]})
y=$.A
if(y!==C.d){y.toString
H.f(a,{func:1,ret:{futureOr:1,type:c},args:[z]})
if(b!=null)b=P.i5(b,y)}H.f(a,{func:1,ret:{futureOr:1,type:c},args:[z]})
x=new P.a_(0,$.A,[c])
w=b==null?1:3
this.b_(new P.as(x,w,a,b,[z,c]))
return x},
aS:function(a,b){return this.bq(a,null,b)},
b_:function(a){var z,y
z=this.a
if(z<=1){a.a=H.i(this.c,"$isas")
this.c=a}else{if(z===2){y=H.i(this.c,"$isa_")
z=y.a
if(z<4){y.b_(a)
return}this.a=z
this.c=y.c}z=this.b
z.toString
P.bq(null,null,z,H.f(new P.hh(this,a),{func:1,ret:-1}))}},
b7:function(a){var z,y,x,w,v,u
z={}
z.a=a
if(a==null)return
y=this.a
if(y<=1){x=H.i(this.c,"$isas")
this.c=a
if(x!=null){for(w=a;v=w.a,v!=null;w=v);w.a=x}}else{if(y===2){u=H.i(this.c,"$isa_")
y=u.a
if(y<4){u.b7(a)
return}this.a=y
this.c=u.c}z.a=this.ah(a)
y=this.b
y.toString
P.bq(null,null,y,H.f(new P.hm(z,this),{func:1,ret:-1}))}},
aI:function(){var z=H.i(this.c,"$isas")
this.c=null
return this.ah(z)},
ah:function(a){var z,y,x
for(z=a,y=null;z!=null;y=z,z=x){x=z.a
z.a=y}return y},
ay:function(a){var z,y,x,w
z=H.h(this,0)
H.bu(a,{futureOr:1,type:z})
y=this.$ti
x=H.aM(a,"$isaC",y,"$asaC")
if(x){z=H.aM(a,"$isa_",y,null)
if(z)P.dr(a,this)
else P.hi(a,this)}else{w=this.aI()
H.p(a,z)
this.a=4
this.c=a
P.aI(this,w)}},
az:[function(a,b){var z
H.i(b,"$isU")
z=this.aI()
this.a=8
this.c=new P.N(a,b)
P.aI(this,z)},function(a){return this.az(a,null)},"cv","$2","$1","gbP",4,2,30],
$isaC:1,
l:{
hi:function(a,b){var z,y,x
b.a=1
try{a.bq(new P.hj(b),new P.hk(b),null)}catch(x){z=H.V(x)
y=H.aP(x)
P.iH(new P.hl(b,z,y))}},
dr:function(a,b){var z,y
for(;z=a.a,z===2;)a=H.i(a.c,"$isa_")
if(z>=4){y=b.aI()
b.a=a.a
b.c=a.c
P.aI(b,y)}else{y=H.i(b.c,"$isas")
b.a=2
b.c=a
a.b7(y)}},
aI:function(a,b){var z,y,x,w,v,u,t,s,r,q,p,o,n,m
z={}
z.a=a
for(y=a;!0;){x={}
w=y.a===8
if(b==null){if(w){v=H.i(y.c,"$isN")
y=y.b
u=v.a
t=v.b
y.toString
P.bp(null,null,y,u,t)}return}for(;s=b.a,s!=null;b=s){b.a=null
P.aI(z.a,b)}y=z.a
r=y.c
x.a=w
x.b=r
u=!w
if(u){t=b.c
t=(t&1)!==0||t===8}else t=!0
if(t){t=b.b
q=t.b
if(w){p=y.b
p.toString
p=p==null?q==null:p===q
if(!p)q.toString
else p=!0
p=!p}else p=!1
if(p){H.i(r,"$isN")
y=y.b
u=r.a
t=r.b
y.toString
P.bp(null,null,y,u,t)
return}o=$.A
if(o==null?q!=null:o!==q)$.A=q
else o=null
y=b.c
if(y===8)new P.hp(z,x,b,w).$0()
else if(u){if((y&1)!==0)new P.ho(x,b,r).$0()}else if((y&2)!==0)new P.hn(z,x,b).$0()
if(o!=null)$.A=o
y=x.b
if(!!J.q(y).$isaC){if(y.a>=4){n=H.i(t.c,"$isas")
t.c=null
b=t.ah(n)
t.a=y.a
t.c=y.c
z.a=y
continue}else P.dr(y,t)
return}}m=b.b
n=H.i(m.c,"$isas")
m.c=null
b=m.ah(n)
y=x.a
u=x.b
if(!y){H.p(u,H.h(m,0))
m.a=4
m.c=u}else{H.i(u,"$isN")
m.a=8
m.c=u}z.a=m
y=m}}}},
hh:{"^":"e:0;a,b",
$0:function(){P.aI(this.a,this.b)}},
hm:{"^":"e:0;a,b",
$0:function(){P.aI(this.b,this.a.a)}},
hj:{"^":"e:12;a",
$1:function(a){var z=this.a
z.a=0
z.ay(a)}},
hk:{"^":"e:28;a",
$2:function(a,b){this.a.az(a,H.i(b,"$isU"))},
$1:function(a){return this.$2(a,null)}},
hl:{"^":"e:0;a,b,c",
$0:function(){this.a.az(this.b,this.c)}},
hp:{"^":"e:1;a,b,c,d",
$0:function(){var z,y,x,w,v,u,t
z=null
try{w=this.c
z=w.b.b.bp(H.f(w.d,{func:1}),null)}catch(v){y=H.V(v)
x=H.aP(v)
if(this.d){w=H.i(this.a.a.c,"$isN").a
u=y
u=w==null?u==null:w===u
w=u}else w=!1
u=this.b
if(w)u.b=H.i(this.a.a.c,"$isN")
else u.b=new P.N(y,x)
u.a=!0
return}if(!!J.q(z).$isaC){if(z instanceof P.a_&&z.gbb()>=4){if(z.gbb()===8){w=this.b
w.b=H.i(z.gc_(),"$isN")
w.a=!0}return}t=this.a.a
w=this.b
w.b=z.aS(new P.hq(t),null)
w.a=!1}}},
hq:{"^":"e:25;a",
$1:function(a){return this.a}},
ho:{"^":"e:1;a,b,c",
$0:function(){var z,y,x,w,v,u,t
try{x=this.b
w=H.h(x,0)
v=H.p(this.c,w)
u=H.h(x,1)
this.a.b=x.b.b.aR(H.f(x.d,{func:1,ret:{futureOr:1,type:u},args:[w]}),v,{futureOr:1,type:u},w)}catch(t){z=H.V(t)
y=H.aP(t)
x=this.a
x.b=new P.N(z,y)
x.a=!0}}},
hn:{"^":"e:1;a,b,c",
$0:function(){var z,y,x,w,v,u,t,s
try{z=H.i(this.a.a.c,"$isN")
w=this.c
if(w.cl(z)&&w.e!=null){v=this.b
v.b=w.cf(z)
v.a=!1}}catch(u){y=H.V(u)
x=H.aP(u)
w=H.i(this.a.a.c,"$isN")
v=w.a
t=y
s=this.b
if(v==null?t==null:v===t)s.b=w
else s.b=new P.N(y,x)
s.a=!0}}},
db:{"^":"c;a,0b"},
bW:{"^":"c;$ti",
gi:function(a){var z,y
z={}
y=new P.a_(0,$.A,[P.k])
z.a=0
this.ck(new P.fF(z,this),!0,new P.fG(z,y),y.gbP())
return y}},
fF:{"^":"e;a,b",
$1:function(a){H.p(a,H.af(this.b,"bW",0));++this.a.a},
$S:function(){return{func:1,ret:P.D,args:[H.af(this.b,"bW",0)]}}},
fG:{"^":"e:0;a,b",
$0:function(){this.b.ay(this.a.a)}},
fE:{"^":"c;$ti"},
N:{"^":"c;a,b",
h:function(a){return H.d(this.a)},
$isF:1},
hT:{"^":"c;",$isjB:1},
ia:{"^":"e:0;a,b",
$0:function(){var z,y,x
z=this.a
y=z.a
if(y==null){x=new P.cR()
z.a=x
z=x}else z=y
y=this.b
if(y==null)throw H.b(z)
x=H.b(z)
x.stack=y.h(0)
throw x}},
hC:{"^":"hT;",
cr:function(a){var z,y,x
H.f(a,{func:1,ret:-1})
try{if(C.d===$.A){a.$0()
return}P.dJ(null,null,this,a,-1)}catch(x){z=H.V(x)
y=H.aP(x)
P.bp(null,null,this,z,H.i(y,"$isU"))}},
cs:function(a,b,c){var z,y,x
H.f(a,{func:1,ret:-1,args:[c]})
H.p(b,c)
try{if(C.d===$.A){a.$1(b)
return}P.dK(null,null,this,a,b,-1,c)}catch(x){z=H.V(x)
y=H.aP(x)
P.bp(null,null,this,z,H.i(y,"$isU"))}},
c7:function(a,b){return new P.hE(this,H.f(a,{func:1,ret:b}),b)},
bg:function(a){return new P.hD(this,H.f(a,{func:1,ret:-1}))},
c8:function(a,b){return new P.hF(this,H.f(a,{func:1,ret:-1,args:[b]}),b)},
bp:function(a,b){H.f(a,{func:1,ret:b})
if($.A===C.d)return a.$0()
return P.dJ(null,null,this,a,b)},
aR:function(a,b,c,d){H.f(a,{func:1,ret:c,args:[d]})
H.p(b,d)
if($.A===C.d)return a.$1(b)
return P.dK(null,null,this,a,b,c,d)},
cq:function(a,b,c,d,e,f){H.f(a,{func:1,ret:d,args:[e,f]})
H.p(b,e)
H.p(c,f)
if($.A===C.d)return a.$2(b,c)
return P.ib(null,null,this,a,b,c,d,e,f)},
co:function(a,b,c,d){return H.f(a,{func:1,ret:b,args:[c,d]})}},
hE:{"^":"e;a,b,c",
$0:function(){return this.a.bp(this.b,this.c)},
$S:function(){return{func:1,ret:this.c}}},
hD:{"^":"e:1;a,b",
$0:function(){return this.a.cr(this.b)}},
hF:{"^":"e;a,b,c",
$1:function(a){var z=this.c
return this.a.cs(this.b,H.p(a,z),z)},
$S:function(){return{func:1,ret:-1,args:[this.c]}}}}],["","",,P,{"^":"",
cK:function(a,b){return new H.fc(0,0,[a,b])},
aW:function(a,b,c,d){return new P.dw(0,0,[d])},
f5:function(a,b,c){var z,y
if(P.c9(a)){if(b==="("&&c===")")return"(...)"
return b+"..."+c}z=[]
y=$.$get$aL()
C.b.j(y,a)
try{P.i2(a,z)}finally{if(0>=y.length)return H.a(y,-1)
y.pop()}y=P.cW(b,H.iB(z,"$iso"),", ")+c
return y.charCodeAt(0)==0?y:y},
bL:function(a,b,c){var z,y,x
if(P.c9(a))return b+"..."+c
z=new P.bX(b)
y=$.$get$aL()
C.b.j(y,a)
try{x=z
x.a=P.cW(x.gX(),a,", ")}finally{if(0>=y.length)return H.a(y,-1)
y.pop()}y=z
y.a=y.gX()+c
y=z.gX()
return y.charCodeAt(0)==0?y:y},
c9:function(a){var z,y
for(z=0;y=$.$get$aL(),z<y.length;++z)if(a===y[z])return!0
return!1},
i2:function(a,b){var z,y,x,w,v,u,t,s,r,q
z=a.gC(a)
y=0
x=0
while(!0){if(!(y<80||x<3))break
if(!z.n())return
w=H.d(z.gu())
C.b.j(b,w)
y+=w.length+2;++x}if(!z.n()){if(x<=5)return
if(0>=b.length)return H.a(b,-1)
v=b.pop()
if(0>=b.length)return H.a(b,-1)
u=b.pop()}else{t=z.gu();++x
if(!z.n()){if(x<=4){C.b.j(b,H.d(t))
return}v=H.d(t)
if(0>=b.length)return H.a(b,-1)
u=b.pop()
y+=v.length+2}else{s=z.gu();++x
for(;z.n();t=s,s=r){r=z.gu();++x
if(x>100){while(!0){if(!(y>75&&x>3))break
if(0>=b.length)return H.a(b,-1)
y-=b.pop().length+2;--x}C.b.j(b,"...")
return}}u=H.d(t)
v=H.d(s)
y+=v.length+u.length+4}}if(x>b.length+2){y+=5
q="..."}else q=null
while(!0){if(!(y>80&&b.length>3))break
if(0>=b.length)return H.a(b,-1)
y-=b.pop().length+2
if(q==null){y+=5
q="..."}}if(q!=null)C.b.j(b,q)
C.b.j(b,u)
C.b.j(b,v)},
aX:function(a,b){var z,y
z=P.aW(null,null,null,b)
for(y=J.S(a);y.n();)z.j(0,H.p(y.gu(),b))
return z},
bS:function(a){var z,y,x
z={}
if(P.c9(a))return"{...}"
y=new P.bX("")
try{C.b.j($.$get$aL(),a)
x=y
x.a=x.gX()+"{"
z.a=!0
a.al(0,new P.fh(z,y))
z=y
z.a=z.gX()+"}"}finally{z=$.$get$aL()
if(0>=z.length)return H.a(z,-1)
z.pop()}z=y.gX()
return z.charCodeAt(0)==0?z:z},
dw:{"^":"hr;a,0b,0c,0d,0e,0f,r,$ti",
aH:function(){return new P.dw(0,0,this.$ti)},
gC:function(a){return P.c5(this,this.r,H.h(this,0))},
gi:function(a){return this.a},
v:function(a,b){var z,y
if(typeof b==="string"&&b!=="__proto__"){z=this.b
if(z==null)return!1
return H.i(z[b],"$isb2")!=null}else if(typeof b==="number"&&(b&0x3ffffff)===b){y=this.c
if(y==null)return!1
return H.i(y[b],"$isb2")!=null}else return this.bQ(b)},
bQ:function(a){var z=this.d
if(z==null)return!1
return this.aC(this.b6(z,a),a)>=0},
j:function(a,b){var z,y
H.p(b,H.h(this,0))
if(typeof b==="string"&&b!=="__proto__"){z=this.b
if(z==null){z=P.c6()
this.b=z}return this.b0(z,b)}else if(typeof b==="number"&&(b&0x3ffffff)===b){y=this.c
if(y==null){y=P.c6()
this.c=y}return this.b0(y,b)}else return this.bM(b)},
bM:function(a){var z,y,x
H.p(a,H.h(this,0))
z=this.d
if(z==null){z=P.c6()
this.d=z}y=this.b1(a)
x=z[y]
if(x==null)z[y]=[this.ax(a)]
else{if(this.aC(x,a)>=0)return!1
x.push(this.ax(a))}return!0},
ao:function(a,b){if(typeof b==="string"&&b!=="__proto__")return this.ba(this.b,b)
else if(typeof b==="number"&&(b&0x3ffffff)===b)return this.ba(this.c,b)
else return this.bY(b)},
bY:function(a){var z,y,x
z=this.d
if(z==null)return!1
y=this.b6(z,a)
x=this.aC(y,a)
if(x<0)return!1
this.bd(y.splice(x,1)[0])
return!0},
aM:function(a){if(this.a>0){this.f=null
this.e=null
this.d=null
this.c=null
this.b=null
this.a=0
this.aw()}},
b0:function(a,b){H.p(b,H.h(this,0))
if(H.i(a[b],"$isb2")!=null)return!1
a[b]=this.ax(b)
return!0},
ba:function(a,b){var z
if(a==null)return!1
z=H.i(a[b],"$isb2")
if(z==null)return!1
this.bd(z)
delete a[b]
return!0},
aw:function(){this.r=this.r+1&67108863},
ax:function(a){var z,y
z=new P.b2(H.p(a,H.h(this,0)))
if(this.e==null){this.f=z
this.e=z}else{y=this.f
z.c=y
y.b=z
this.f=z}++this.a
this.aw()
return z},
bd:function(a){var z,y
z=a.c
y=a.b
if(z==null)this.e=y
else z.b=y
if(y==null)this.f=z
else y.c=z;--this.a
this.aw()},
b1:function(a){return J.an(a)&0x3ffffff},
b6:function(a,b){return a[this.b1(b)]},
aC:function(a,b){var z,y
if(a==null)return-1
z=a.length
for(y=0;y<z;++y)if(J.ax(a[y].a,b))return y
return-1},
l:{
c6:function(){var z=Object.create(null)
z["<non-identifier-key>"]=z
delete z["<non-identifier-key>"]
return z}}},
b2:{"^":"c;a,0b,0c"},
hy:{"^":"c;a,b,0c,0d,$ti",
gu:function(){return this.d},
n:function(){var z=this.a
if(this.b!==z.r)throw H.b(P.O(z))
else{z=this.c
if(z==null){this.d=null
return!1}else{this.d=H.p(z.a,H.h(this,0))
this.c=z.b
return!0}}},
l:{
c5:function(a,b,c){var z=new P.hy(a,b,[c])
z.c=a.e
return z}}},
hr:{"^":"fB;"},
f4:{"^":"o;"},
bQ:{"^":"hz;",$iso:1,$ism:1},
E:{"^":"c;$ti",
gC:function(a){return new H.cL(a,this.gi(a),0,[H.b6(this,a,"E",0)])},
A:function(a,b){return this.m(a,b)},
gJ:function(a){if(this.gi(a)===0)throw H.b(H.bg())
return this.m(a,0)},
v:function(a,b){var z,y,x
z=this.gi(a)
for(y=0;y<z;++y){x=this.m(a,y)
if(x==null?b==null:x===b)return!0
if(z!==this.gi(a))throw H.b(P.O(a))}return!1},
bm:function(a,b,c){var z=H.b6(this,a,"E",0)
return new H.P(a,H.f(b,{func:1,ret:c,args:[z]}),[z,c])},
a9:function(a,b){var z,y
z=H.l([],[H.b6(this,a,"E",0)])
C.b.si(z,this.gi(a))
for(y=0;y<this.gi(a);++y)C.b.F(z,y,this.m(a,y))
return z},
E:function(a){return this.a9(a,!0)},
h:function(a){return P.bL(a,"[","]")}},
cM:{"^":"bh;"},
fh:{"^":"e:24;a,b",
$2:function(a,b){var z,y
z=this.a
if(!z.a)this.b.a+=", "
z.a=!1
z=this.b
y=z.a+=H.d(a)
z.a=y+": "
z.a+=H.d(b)}},
bh:{"^":"c;$ti",
al:function(a,b){var z,y
H.f(b,{func:1,ret:-1,args:[H.af(this,"bh",0),H.af(this,"bh",1)]})
for(z=J.S(this.gU());z.n();){y=z.gu()
b.$2(y,this.m(0,y))}},
gi:function(a){return J.T(this.gU())},
h:function(a){return P.bS(this)},
$isbR:1},
fC:{"^":"c;$ti",
B:function(a,b){var z
for(z=J.S(H.H(b,"$iso",this.$ti,"$aso"));z.n();)this.j(0,z.gu())},
bs:function(a){var z
H.H(a,"$isK",this.$ti,"$asK")
z=this.aH()
z.B(0,this)
z.B(0,a)
return z},
a9:function(a,b){var z,y,x,w
z=H.l([],this.$ti)
C.b.si(z,this.a)
for(y=P.c5(this,this.r,H.h(this,0)),x=0;y.n();x=w){w=x+1
C.b.F(z,x,y.d)}return z},
E:function(a){return this.a9(a,!0)},
h:function(a){return P.bL(this,"{","}")},
A:function(a,b){var z,y,x
if(typeof b!=="number"||Math.floor(b)!==b)throw H.b(P.cn("index"))
if(b<0)H.w(P.a9(b,0,null,"index",null))
for(z=P.c5(this,this.r,H.h(this,0)),y=0;z.n();){x=z.d
if(b===y)return x;++y}throw H.b(P.ap(b,this,"index",null,y))},
$iso:1,
$isK:1},
fB:{"^":"fC;"},
hz:{"^":"c+E;"}}],["","",,P,{"^":"",
ey:function(a){var z=J.q(a)
if(!!z.$ise)return z.h(a)
return"Instance of '"+H.aG(a)+"'"},
ah:function(a,b,c){var z,y,x
z=[c]
y=H.l([],z)
for(x=J.S(a);x.n();)C.b.j(y,H.p(x.gu(),c))
if(b)return y
return H.H(J.aE(y),"$ism",z,"$asm")},
cT:function(a,b,c){return new H.cI(a,H.cJ(a,!1,!0,!1))},
be:function(a){if(typeof a==="number"||typeof a==="boolean"||null==a)return J.ay(a)
if(typeof a==="string")return JSON.stringify(a)
return P.ey(a)},
B:function(a){return new P.hg(a)},
aY:function(a,b,c,d){var z,y
H.f(b,{func:1,ret:d,args:[P.k]})
z=H.l([],[d])
C.b.si(z,a)
for(y=0;y<a;++y)C.b.F(z,y,b.$1(y))
return z},
iF:[function(a,b){var z,y
H.u(a)
H.f(b,{func:1,ret:P.r,args:[P.j]})
z=J.ee(a)
y=H.fu(z,null)
if(y==null)y=H.ft(z)
if(y!=null)return y
if(b==null)throw H.b(P.cE(a,null,null))
return b.$1(a)},function(a){return P.iF(a,null)},"$2","$1","bs",4,2,37],
G:{"^":"c;a,b,c",
P:function(a){var z,y,x
z=this.c
if(z===0)return this
y=!this.a
x=this.b
z=P.J(z,x)
return new P.G(z===0?!1:y,x,z)},
bS:function(a){var z,y,x,w,v,u,t,s,r
z=this.c
if(z===0)return $.$get$L()
y=z+a
x=this.b
w=new Uint16Array(y)
for(v=z-1,u=x.length,t=w.length;v>=0;--v){s=v+a
if(v>=u)return H.a(x,v)
r=x[v]
if(s<0||s>=t)return H.a(w,s)
w[s]=r}u=this.a
t=P.J(y,w)
return new P.G(t===0?!1:u,w,t)},
bT:function(a){var z,y,x,w,v,u,t,s,r,q
z=this.c
if(z===0)return $.$get$L()
y=z-a
if(y<=0)return this.a?$.$get$c3():$.$get$L()
x=this.b
w=new Uint16Array(y)
for(v=x.length,u=w.length,t=a;t<z;++t){s=t-a
if(t<0||t>=v)return H.a(x,t)
r=x[t]
if(s>=u)return H.a(w,s)
w[s]=r}u=this.a
s=P.J(y,w)
q=new P.G(s===0?!1:u,w,s)
if(u)for(t=0;t<a;++t){if(t>=v)return H.a(x,t)
if(x[t]!==0)return q.I(0,$.$get$aa())}return q},
L:function(a,b){var z,y,x,w,v
if(b<0)throw H.b(P.bb("shift-amount must be posititve "+b))
z=this.c
if(z===0)return this
y=C.a.k(b,16)
if(C.a.w(b,16)===0)return this.bS(y)
x=z+y+1
w=new Uint16Array(x)
P.dn(this.b,z,b,w)
z=this.a
v=P.J(x,w)
return new P.G(v===0?!1:z,w,v)},
ac:function(a,b){var z,y,x,w,v,u,t,s,r
if(typeof b!=="number")return b.S()
if(b<0)throw H.b(P.bb("shift-amount must be posititve "+b))
z=this.c
if(z===0)return this
y=C.a.k(b,16)
x=C.a.w(b,16)
if(x===0)return this.bT(y)
w=z-y
if(w<=0)return this.a?$.$get$c3():$.$get$L()
v=this.b
u=new Uint16Array(w)
P.h2(v,z,b,u)
z=this.a
t=P.J(w,u)
s=new P.G(t===0?!1:z,u,t)
if(z){z=v.length
if(y<0||y>=z)return H.a(v,y)
if((v[y]&C.a.L(1,x)-1)!==0)return s.I(0,$.$get$aa())
for(r=0;r<y;++r){if(r>=z)return H.a(v,r)
if(v[r]!==0)return s.I(0,$.$get$aa())}}return s},
au:function(a){return P.df(this.b,this.c,a.b,a.c)},
aj:function(a,b){var z,y
z=this.a
if(z===b.a){y=this.au(b)
return z?0-y:y}return z?-1:1},
at:function(a,b){var z,y,x,w,v
z=this.c
y=a.c
if(z<y)return a.at(this,b)
if(z===0)return $.$get$L()
if(y===0)return this.a===b?this:this.P(0)
x=z+1
w=new Uint16Array(x)
P.h0(this.b,z,a.b,y,w)
v=P.J(x,w)
return new P.G(v===0?!1:b,w,v)},
a1:function(a,b){var z,y,x,w
z=this.c
if(z===0)return $.$get$L()
y=a.c
if(y===0)return this.a===b?this:this.P(0)
x=new Uint16Array(z)
P.b0(this.b,z,a.b,y,x)
w=P.J(z,x)
return new P.G(w===0?!1:b,x,w)},
bL:function(a,b){var z,y,x,w,v,u,t,s,r,q,p
z=this.c
y=this.b
x=a.b
w=new Uint16Array(z)
v=a.c
if(z<v)v=z
for(u=y.length,t=x.length,s=w.length,r=0;r<v;++r){if(r>=u)return H.a(y,r)
q=y[r]
if(r>=t)return H.a(x,r)
p=x[r]
if(r>=s)return H.a(w,r)
w[r]=q&~p}for(r=v;r<z;++r){if(r<0||r>=u)return H.a(y,r)
t=y[r]
if(r>=s)return H.a(w,r)
w[r]=t}u=P.J(z,w)
return new P.G(u===0?!1:b,w,u)},
aW:function(a,b){var z,y
if(this.c===0||b.gcz())return $.$get$L()
b.gcw()
if(this.a){z=b
y=this}else{y=b
z=this}return z.bL(y.a1($.$get$aa(),!1),!1)},
H:function(a,b){var z
if(this.c===0)return b
if(b.c===0)return this
z=this.a
if(z===b.a)return this.at(b,z)
if(this.au(b)>=0)return this.a1(b,z)
return b.a1(this,!z)},
I:function(a,b){var z
if(this.c===0)return b.P(0)
if(b.c===0)return this
z=this.a
if(z!==b.a)return this.at(b,z)
if(this.au(b)>=0)return this.a1(b,z)
return b.a1(this,!z)},
G:function(a,b){var z,y,x,w,v,u,t,s,r
H.i(b,"$isa4")
z=this.c
y=b.c
if(z===0||y===0)return $.$get$L()
x=z+y
w=this.b
v=b.b
u=new Uint16Array(x)
for(t=v.length,s=0;s<y;){if(s>=t)return H.a(v,s)
P.dp(v[s],w,0,u,s,z);++s}t=this.a!==b.a
r=P.J(x,u)
return new P.G(r===0?!1:t,u,r)},
ae:function(a){var z,y,x,w,v
if(this.c<a.c)return $.$get$L()
this.b3(a)
z=$.dl
y=$.bk
if(typeof z!=="number")return z.I()
if(typeof y!=="number")return H.a2(y)
x=z-y
w=P.c0($.c2,y,z,x)
z=P.J(x,w)
v=new P.G(!1,w,z)
return this.a!==a.a&&z>0?v.P(0):v},
b9:function(a){var z,y,x,w
if(this.c<a.c)return this
this.b3(a)
z=$.c2
y=$.bk
x=P.c0(z,0,y,y)
y=P.J($.bk,x)
w=new P.G(!1,x,y)
z=$.dm
if(typeof z!=="number")return z.ab()
if(z>0)w=w.ac(0,z)
return this.a&&w.c>0?w.P(0):w},
b3:function(a){var z,y,x,w,v,u,t,s,r,q,p,o,n,m,l,k,j
z=this.c
if(z===$.di&&a.c===$.dk&&this.b===$.dh&&a.b===$.dj)return
y=a.b
x=a.c
w=x-1
if(w<0||w>=y.length)return H.a(y,w)
v=16-C.a.gai(y[w])
if(v>0){u=new Uint16Array(x+5)
t=P.dg(y,x,v,u)
s=new Uint16Array(z+5)
r=P.dg(this.b,z,v,s)}else{s=P.c0(this.b,0,z,z+2)
t=x
u=y
r=z}w=t-1
if(w<0||w>=u.length)return H.a(u,w)
q=u[w]
p=r-t
o=new Uint16Array(r)
n=P.c1(u,t,p,o)
w=s.length
m=r+1
if(P.df(s,r,o,n)>=0){if(r<0||r>=w)return H.a(s,r)
s[r]=1
P.b0(s,m,o,n,s)}else{if(r<0||r>=w)return H.a(s,r)
s[r]=0}l=new Uint16Array(t+2)
if(t<0||t>=l.length)return H.a(l,t)
l[t]=1
P.b0(l,t+1,u,t,l)
k=r-1
for(;p>0;){j=P.h1(q,s,k);--p
P.dp(j,l,0,s,p,t)
if(k<0||k>=w)return H.a(s,k)
if(s[k]<j){n=P.c1(l,t,p,o)
P.b0(s,m,o,n,s)
for(;--j,s[k]<j;)P.b0(s,m,o,n,s)}--k}$.dh=this.b
$.di=z
$.dj=y
$.dk=x
$.c2=s
$.dl=m
$.bk=t
$.dm=v},
gD:function(a){var z,y,x,w,v,u
z=new P.h3()
y=this.c
if(y===0)return 6707
x=this.a?83585:429689
for(w=this.b,v=w.length,u=0;u<y;++u){if(u>=v)return H.a(w,u)
x=z.$2(x,w[u])}return new P.h4().$1(x)},
O:function(a,b){if(b==null)return!1
return b instanceof P.G&&this.aj(0,b)===0},
t:function(a,b){if(b.c===0)throw H.b(C.f)
return this.ae(b)},
a_:function(a,b){H.i(b,"$isa4")
return C.e.a_(this.br(0),b.br(0))},
S:function(a,b){return this.aj(0,H.i(b,"$isa4"))<0},
ab:function(a,b){return this.aj(0,H.i(b,"$isa4"))>0},
w:function(a,b){var z
if(b.c===0)throw H.b(C.f)
z=this.b9(b)
if(z.a)z=b.a?z.I(0,b):z.H(0,b)
return z},
br:function(a){var z,y,x,w,v,u,t,s,r,q,p
z={}
y=this.c
if(y===0)return 0
x=new Uint8Array(8);--y
w=this.b
v=w.length
if(y<0||y>=v)return H.a(w,y)
u=16*y+C.a.gai(w[y])-53
if(u>971)return 1/0
if(this.a){if(7>=x.length)return H.a(x,7)
x[7]=128}t=u+1075
u=x.length
if(6>=u)return H.a(x,6)
x[6]=(t&15)<<4
if(7>=u)return H.a(x,7)
x[7]=(x[7]|C.a.a3(t,4))>>>0
z.a=0
z.b=0
z.c=y
s=new P.h5(z,this)
r=J.e5(s.$1(5),15)
x[6]=(x[6]|r)>>>0
for(q=5;q>=0;--q)C.J.F(x,q,s.$1(8))
p=new P.h6(x)
if(J.ax(s.$1(1),1))if((x[0]&1)===1)p.$0()
else if(z.a!==0)p.$0()
else for(q=z.c,y=q>=0;y;--q){if(q<0||q>=v)return H.a(w,q)
if(w[q]!==0){p.$0()
break}}y=x.buffer
y.toString
return H.cO(y,0,null).getFloat64(0,!0)},
h:function(a){var z,y,x,w,v,u,t
z=this.c
if(z===0)return"0"
if(z===1){if(this.a){z=this.b
if(0>=z.length)return H.a(z,0)
return C.a.h(-z[0])}z=this.b
if(0>=z.length)return H.a(z,0)
return C.a.h(z[0])}y=H.l([],[P.j])
z=this.a
x=z?this.P(0):this
for(;x.c>1;){w=$.$get$dd()
v=w.c===0
if(v)H.w(C.f)
u=J.ay(x.b9(w))
C.b.j(y,u)
t=u.length
if(t===1)C.b.j(y,"000")
if(t===2)C.b.j(y,"00")
if(t===3)C.b.j(y,"0")
if(v)H.w(C.f)
x=x.ae(w)}v=x.b
if(0>=v.length)return H.a(v,0)
C.b.j(y,C.a.h(v[0]))
if(z)C.b.j(y,"-")
return new H.fy(y,[H.h(y,0)]).N(0)},
$isa4:1,
l:{
J:function(a,b){var z,y
z=b.length
while(!0){if(typeof a!=="number")return a.ab()
if(a>0){y=a-1
if(y>=z)return H.a(b,y)
y=b[y]===0}else y=!1
if(!y)break;--a}return a},
c0:function(a,b,c,d){var z,y,x,w,v
z=typeof d==="number"&&Math.floor(d)===d?d:H.w(P.bb("Invalid length "+H.d(d)))
y=new Uint16Array(z)
if(typeof c!=="number")return c.I()
if(typeof b!=="number")return H.a2(b)
x=c-b
for(z=y.length,w=0;w<x;++w){v=b+w
if(v<0||v>=a.length)return H.a(a,v)
v=a[v]
if(w>=z)return H.a(y,w)
y[w]=v}return y},
c_:function(a){if(a===0)return $.$get$L()
if(a===1)return $.$get$aa()
if(a===2)return $.$get$dq()
if(Math.abs(a)<4294967296)return P.aH(C.a.a8(a))
if(typeof a==="number")return P.h_(a)
return P.aH(a)},
aH:function(a){var z,y,x,w,v,u
z=a<0
if(z){if(a===-9223372036854776e3){y=new Uint16Array(4)
if(3>=y.length)return H.a(y,3)
y[3]=32768
x=P.J(4,y)
return new P.G(x!==0||!1,y,x)}a=-a}if(a<65536){y=new Uint16Array(1)
if(0>=y.length)return H.a(y,0)
y[0]=a
x=P.J(1,y)
return new P.G(x===0?!1:z,y,x)}if(a<=4294967295){y=new Uint16Array(2)
x=y.length
if(0>=x)return H.a(y,0)
y[0]=a&65535
w=C.a.a3(a,16)
if(1>=x)return H.a(y,1)
y[1]=w
w=P.J(2,y)
return new P.G(w===0?!1:z,y,w)}x=C.a.k(C.a.gai(a)-1,16)
y=new Uint16Array(x+1)
for(x=y.length,v=0;a!==0;v=u){u=v+1
if(v>=x)return H.a(y,v)
y[v]=a&65535
a=C.a.k(a,65536)}x=P.J(x,y)
return new P.G(x===0?!1:z,y,x)},
h_:function(a){var z,y,x,w,v,u,t,s,r,q
if(isNaN(a)||a==1/0||a==-1/0)throw H.b(P.bb("Value must be finite: "+a))
z=a<0
if(z)a=-a
a=Math.floor(a)
if(a===0)return $.$get$L()
y=$.$get$de()
for(x=0;x<8;++x){y.length
if(x>=8)return H.a(y,x)
y[x]=0}w=y.buffer
w.toString
H.cO(w,0,null).setFloat64(0,a,!0)
v=(y[7]<<4>>>0)+(y[6]>>>4)-1075
u=new Uint16Array(4)
w=y[1]
t=y[0]
s=u.length
if(0>=s)return H.a(u,0)
u[0]=(w<<8>>>0)+t
t=y[3]
w=y[2]
if(1>=s)return H.a(u,1)
u[1]=(t<<8>>>0)+w
w=y[5]
t=y[4]
if(2>=s)return H.a(u,2)
u[2]=(w<<8>>>0)+t
t=y[6]
if(3>=s)return H.a(u,3)
u[3]=16|t&15
r=new P.G(!1,u,4)
if(v<0)q=r.ac(0,-v)
else q=v>0?r.L(0,v):r
if(z)return q.P(0)
return q},
c1:function(a,b,c,d){var z,y,x,w,v
if(b===0)return 0
if(c===0&&d===a)return b
for(z=b-1,y=a.length,x=d.length;z>=0;--z){w=z+c
if(z>=y)return H.a(a,z)
v=a[z]
if(w<0||w>=x)return H.a(d,w)
d[w]=v}for(z=c-1;z>=0;--z){if(z>=x)return H.a(d,z)
d[z]=0}return b+c},
dn:function(a,b,c,d){var z,y,x,w,v,u,t,s,r,q,p
z=C.a.k(c,16)
y=C.a.w(c,16)
x=16-y
w=C.a.L(1,x)-1
for(v=b-1,u=a.length,t=d.length,s=0;v>=0;--v){if(v>=u)return H.a(a,v)
r=a[v]
q=v+z+1
p=C.a.aL(r,x)
if(q<0||q>=t)return H.a(d,q)
d[q]=(p|s)>>>0
s=C.a.L(r&w,y)}if(z<0||z>=t)return H.a(d,z)
d[z]=s},
dg:function(a,b,c,d){var z,y,x,w,v
z=C.a.k(c,16)
if(C.a.w(c,16)===0)return P.c1(a,b,z,d)
y=b+z+1
P.dn(a,b,c,d)
for(x=d.length,w=z;--w,w>=0;){if(w>=x)return H.a(d,w)
d[w]=0}v=y-1
if(v<0||v>=x)return H.a(d,v)
if(d[v]===0)y=v
return y},
h2:function(a,b,c,d){var z,y,x,w,v,u,t,s,r,q,p
z=C.a.k(c,16)
y=C.a.w(c,16)
x=16-y
w=C.a.L(1,y)-1
v=a.length
if(z<0||z>=v)return H.a(a,z)
u=C.a.aL(a[z],y)
t=b-z-1
for(s=d.length,r=0;r<t;++r){q=r+z+1
if(q>=v)return H.a(a,q)
p=a[q]
q=C.a.L(p&w,x)
if(r>=s)return H.a(d,r)
d[r]=(q|u)>>>0
u=C.a.aL(p,y)}if(t<0||t>=s)return H.a(d,t)
d[t]=u},
df:function(a,b,c,d){var z,y,x,w,v
z=b-d
if(z===0)for(y=b-1,x=a.length,w=c.length;y>=0;--y){if(y>=x)return H.a(a,y)
v=a[y]
if(y>=w)return H.a(c,y)
z=v-c[y]
if(z!==0)return z}return z},
h0:function(a,b,c,d,e){var z,y,x,w,v,u
for(z=a.length,y=c.length,x=e.length,w=0,v=0;v<d;++v){if(v>=z)return H.a(a,v)
u=a[v]
if(v>=y)return H.a(c,v)
w+=u+c[v]
if(v>=x)return H.a(e,v)
e[v]=w&65535
w=w>>>16}for(v=d;v<b;++v){if(v<0||v>=z)return H.a(a,v)
w+=a[v]
if(v>=x)return H.a(e,v)
e[v]=w&65535
w=w>>>16}if(b<0||b>=x)return H.a(e,b)
e[b]=w},
b0:function(a,b,c,d,e){var z,y,x,w,v,u
for(z=a.length,y=c.length,x=e.length,w=0,v=0;v<d;++v){if(v>=z)return H.a(a,v)
u=a[v]
if(v>=y)return H.a(c,v)
w+=u-c[v]
if(v>=x)return H.a(e,v)
e[v]=w&65535
w=0-(C.a.a3(w,16)&1)}for(v=d;v<b;++v){if(v<0||v>=z)return H.a(a,v)
w+=a[v]
if(v>=x)return H.a(e,v)
e[v]=w&65535
w=0-(C.a.a3(w,16)&1)}},
dp:function(a,b,c,d,e,f){var z,y,x,w,v,u,t,s
if(a===0)return
for(z=b.length,y=d.length,x=0;--f,f>=0;e=t,c=w){w=c+1
if(c>=z)return H.a(b,c)
v=b[c]
if(e<0||e>=y)return H.a(d,e)
u=a*v+d[e]+x
t=e+1
d[e]=u&65535
x=C.a.k(u,65536)}for(;x!==0;e=t){if(e<0||e>=y)return H.a(d,e)
s=d[e]+x
t=e+1
d[e]=s&65535
x=C.a.k(s,65536)}},
h1:function(a,b,c){var z,y,x,w
z=b.length
if(c<0||c>=z)return H.a(b,c)
y=b[c]
if(y===a)return 65535
x=c-1
if(x<0||x>=z)return H.a(b,x)
w=C.a.t((y<<16|b[x])>>>0,a)
if(w>65535)return 65535
return w}}},
h3:{"^":"e:11;",
$2:function(a,b){a=536870911&a+b
a=536870911&a+((524287&a)<<10)
return a^a>>>6}},
h4:{"^":"e:3;",
$1:function(a){a=536870911&a+((67108863&a)<<3)
a^=a>>>11
return 536870911&a+((16383&a)<<15)}},
h5:{"^":"e:3;a,b",
$1:function(a){var z,y,x,w,v,u,t,s
for(z=this.a,y=this.b,x=y.c-1,y=y.b,w=y.length;v=z.b,v<a;){v=z.c
if(v<0){z.c=v-1
u=0
t=16}else{if(v>=w)return H.a(y,v)
u=y[v]
t=v===x?C.a.gai(u):16;--z.c}z.a=C.a.L(z.a,t)+u
z.b+=t}y=z.a
v-=a
s=C.a.ac(y,v)
z.a=y-C.a.L(s,v)
z.b=v
return s}},
h6:{"^":"e:1;a",
$0:function(){var z,y,x,w
for(z=this.a,y=1,x=0;x<8;++x){if(y===0)break
w=z[x]+y
z[x]=w&255
y=w>>>8}}},
a4:{"^":"c;"},
t:{"^":"c;"},
"+bool":0,
aO:{"^":"r;"},
"+double":0,
F:{"^":"c;"},
cR:{"^":"F;",
h:function(a){return"Throw of null."}},
a3:{"^":"F;a,b,c,d",
gaB:function(){return"Invalid argument"+(!this.a?"(s)":"")},
gaA:function(){return""},
h:function(a){var z,y,x,w,v,u
z=this.c
y=z!=null?" ("+z+")":""
z=this.d
x=z==null?"":": "+H.d(z)
w=this.gaB()+y+x
if(!this.a)return w
v=this.gaA()
u=P.be(this.b)
return w+v+": "+H.d(u)},
l:{
bb:function(a){return new P.a3(!1,null,null,a)},
co:function(a,b,c){return new P.a3(!0,a,b,c)},
cn:function(a){return new P.a3(!1,null,a,"Must not be null")}}},
bU:{"^":"a3;e,f,a,b,c,d",
gaB:function(){return"RangeError"},
gaA:function(){var z,y,x
z=this.e
if(z==null){z=this.f
y=z!=null?": Not less than or equal to "+H.d(z):""}else{x=this.f
if(x==null)y=": Not greater than or equal to "+H.d(z)
else if(x>z)y=": Not in range "+H.d(z)+".."+H.d(x)+", inclusive"
else y=x<z?": Valid value range is empty":": Only valid value is "+H.d(z)}return y},
l:{
fv:function(a){return new P.bU(null,null,!1,null,null,a)},
bi:function(a,b,c){return new P.bU(null,null,!0,a,b,"Value not in range")},
a9:function(a,b,c,d,e){return new P.bU(b,c,!0,a,d,"Invalid value")}}},
eZ:{"^":"a3;e,i:f>,a,b,c,d",
gaB:function(){return"RangeError"},
gaA:function(){if(J.cj(this.b,0))return": index must not be negative"
var z=this.f
if(z===0)return": no indices are valid"
return": index should be less than "+H.d(z)},
l:{
ap:function(a,b,c,d,e){var z=H.z(e!=null?e:J.T(b))
return new P.eZ(b,z,!0,a,c,"Index out of range")}}},
fQ:{"^":"F;a",
h:function(a){return"Unsupported operation: "+this.a},
l:{
I:function(a){return new P.fQ(a)}}},
fO:{"^":"F;a",
h:function(a){var z=this.a
return z!=null?"UnimplementedError: "+z:"UnimplementedError"},
l:{
d9:function(a){return new P.fO(a)}}},
bV:{"^":"F;a",
h:function(a){return"Bad state: "+this.a},
l:{
aj:function(a){return new P.bV(a)}}},
eo:{"^":"F;a",
h:function(a){var z=this.a
if(z==null)return"Concurrent modification during iteration."
return"Concurrent modification during iteration: "+H.d(P.be(z))+"."},
l:{
O:function(a){return new P.eo(a)}}},
fr:{"^":"c;",
h:function(a){return"Out of Memory"},
$isF:1},
cV:{"^":"c;",
h:function(a){return"Stack Overflow"},
$isF:1},
es:{"^":"F;a",
h:function(a){var z=this.a
return z==null?"Reading static variable during its initialization":"Reading static variable '"+z+"' during its initialization"}},
hg:{"^":"c;a",
h:function(a){return"Exception: "+this.a}},
eC:{"^":"c;a,b,c",
h:function(a){var z,y,x
z=this.a
y=z!=null&&""!==z?"FormatException: "+H.d(z):"FormatException"
x=this.b
if(typeof x!=="string")return y
if(x.length>78)x=C.c.W(x,0,75)+"..."
return y+"\n"+x},
l:{
cE:function(a,b,c){return new P.eC(a,b,c)}}},
f_:{"^":"c;",
h:function(a){return"IntegerDivisionByZeroException"}},
aR:{"^":"c;"},
k:{"^":"r;"},
"+int":0,
o:{"^":"c;$ti",
aV:["bD",function(a,b){var z=H.af(this,"o",0)
return new H.bY(this,H.f(b,{func:1,ret:P.t,args:[z]}),[z])}],
gi:function(a){var z,y
z=this.gC(this)
for(y=0;z.n();)++y
return y},
gV:function(a){var z,y
z=this.gC(this)
if(!z.n())throw H.b(H.bg())
y=z.gu()
if(z.n())throw H.b(H.f6())
return y},
A:function(a,b){var z,y,x
if(typeof b!=="number"||Math.floor(b)!==b)throw H.b(P.cn("index"))
if(b<0)H.w(P.a9(b,0,null,"index",null))
for(z=this.gC(this),y=0;z.n();){x=z.gu()
if(b===y)return x;++y}throw H.b(P.ap(b,this,"index",null,y))},
h:function(a){return P.f5(this,"(",")")}},
bM:{"^":"c;$ti"},
m:{"^":"c;$ti",$iso:1},
"+List":0,
D:{"^":"c;",
gD:function(a){return P.c.prototype.gD.call(this,this)},
h:function(a){return"null"}},
"+Null":0,
r:{"^":"c;"},
"+num":0,
c:{"^":";",
O:function(a,b){return this===b},
gD:function(a){return H.aF(this)},
h:function(a){return"Instance of '"+H.aG(this)+"'"},
toString:function(){return this.h(this)}},
K:{"^":"bJ;$ti"},
U:{"^":"c;"},
j:{"^":"c;",$iscS:1},
"+String":0,
bX:{"^":"c;X:a<",
gi:function(a){return this.a.length},
h:function(a){var z=this.a
return z.charCodeAt(0)==0?z:z},
l:{
cW:function(a,b,c){var z=J.S(b)
if(!z.n())return a
if(c.length===0){do a+=H.d(z.gu())
while(z.n())}else{a+=H.d(z.gu())
for(;z.n();)a=a+c+H.d(z.gu())}return a}}}}],["","",,W,{"^":"",
aA:function(a,b){var z=document.createElement("canvas")
if(b!=null)z.width=b
if(a!=null)z.height=a
return z},
ew:function(a,b,c){var z,y
z=document.body
y=(z&&C.l).M(z,a,b,c)
y.toString
z=W.n
z=new H.bY(new W.Q(y),H.f(new W.ex(),{func:1,ret:P.t,args:[z]}),[z])
return H.i(z.gV(z),"$isx")},
aB:function(a){var z,y,x
z="element tag unavailable"
try{y=J.ec(a)
if(typeof y==="string")z=a.tagName}catch(x){H.V(x)}return z},
bn:function(a,b){a=536870911&a+b
a=536870911&a+((524287&a)<<10)
return a^a>>>6},
dv:function(a,b,c,d){var z,y
z=W.bn(W.bn(W.bn(W.bn(0,a),b),c),d)
y=536870911&z+((67108863&z)<<3)
y^=y>>>11
return 536870911&y+((16383&y)<<15)},
dG:function(a){var z
if(a==null)return
if("postMessage" in a){z=W.hb(a)
if(!!J.q(z).$isao)return z
return}else return H.i(a,"$isao")},
dN:function(a,b){var z
H.f(a,{func:1,ret:-1,args:[b]})
z=$.A
if(z===C.d)return a
return z.c8(a,b)},
M:{"^":"x;","%":"HTMLAudioElement|HTMLBRElement|HTMLContentElement|HTMLDListElement|HTMLDataElement|HTMLDataListElement|HTMLDetailsElement|HTMLDialogElement|HTMLDirectoryElement|HTMLEmbedElement|HTMLFieldSetElement|HTMLFontElement|HTMLFrameElement|HTMLFrameSetElement|HTMLHRElement|HTMLHeadElement|HTMLHeadingElement|HTMLHtmlElement|HTMLIFrameElement|HTMLImageElement|HTMLInputElement|HTMLLIElement|HTMLLabelElement|HTMLLegendElement|HTMLLinkElement|HTMLMapElement|HTMLMarqueeElement|HTMLMediaElement|HTMLMenuElement|HTMLMetaElement|HTMLMeterElement|HTMLModElement|HTMLOListElement|HTMLObjectElement|HTMLOptGroupElement|HTMLOptionElement|HTMLOutputElement|HTMLParagraphElement|HTMLParamElement|HTMLPictureElement|HTMLPreElement|HTMLProgressElement|HTMLQuoteElement|HTMLScriptElement|HTMLShadowElement|HTMLSlotElement|HTMLSourceElement|HTMLSpanElement|HTMLStyleElement|HTMLTableCaptionElement|HTMLTableCellElement|HTMLTableColElement|HTMLTableDataCellElement|HTMLTableHeaderCellElement|HTMLTextAreaElement|HTMLTimeElement|HTMLTitleElement|HTMLTrackElement|HTMLUListElement|HTMLUnknownElement|HTMLVideoElement;HTMLElement"},
iL:{"^":"M;",
h:function(a){return String(a)},
"%":"HTMLAnchorElement"},
iM:{"^":"M;",
h:function(a){return String(a)},
"%":"HTMLAreaElement"},
cp:{"^":"M;",$iscp:1,"%":"HTMLBaseElement"},
bc:{"^":"M;",$isbc:1,"%":"HTMLBodyElement"},
ef:{"^":"M;","%":"HTMLButtonElement"},
bH:{"^":"M;",$isbH:1,"%":"HTMLCanvasElement"},
iO:{"^":"n;0i:length=","%":"CDATASection|CharacterData|Comment|ProcessingInstruction|Text"},
iP:{"^":"h9;0i:length=",
bu:function(a,b){var z=a.getPropertyValue(this.bO(a,b))
return z==null?"":z},
bO:function(a,b){var z,y
z=$.$get$ct()
y=z[b]
if(typeof y==="string")return y
y=this.c3(a,b)
z[b]=y
return y},
c3:function(a,b){var z
if(b.replace(/^-ms-/,"ms-").replace(/-([\da-z])/ig,function(c,d){return d.toUpperCase()}) in a)return b
z=P.et()+b
if(z in a)return z
return b},
gam:function(a){return a.height},
gan:function(a){return a.left},
gaT:function(a){return a.top},
gap:function(a){return a.width},
"%":"CSS2Properties|CSSStyleDeclaration|MSStyleCSSProperties"},
er:{"^":"c;",
gan:function(a){return this.bu(a,"left")}},
eu:{"^":"M;","%":"HTMLDivElement"},
iQ:{"^":"C;",
h:function(a){return String(a)},
"%":"DOMException"},
ev:{"^":"C;",
h:function(a){return"Rectangle ("+H.d(a.left)+", "+H.d(a.top)+") "+H.d(a.width)+" x "+H.d(a.height)},
O:function(a,b){var z
if(b==null)return!1
z=H.aM(b,"$isaZ",[P.r],"$asaZ")
if(!z)return!1
z=J.ae(b)
return a.left===z.gan(b)&&a.top===z.gaT(b)&&a.width===z.gap(b)&&a.height===z.gam(b)},
gD:function(a){return W.dv(a.left&0x1FFFFFFF,a.top&0x1FFFFFFF,a.width&0x1FFFFFFF,a.height&0x1FFFFFFF)},
gam:function(a){return a.height},
gan:function(a){return a.left},
gaT:function(a){return a.top},
gap:function(a){return a.width},
gp:function(a){return a.x},
gq:function(a){return a.y},
$isaZ:1,
$asaZ:function(){return[P.r]},
"%":";DOMRectReadOnly"},
h7:{"^":"bQ;b4:a<,b",
v:function(a,b){return J.ck(this.b,b)},
gi:function(a){return this.b.length},
m:function(a,b){var z=this.b
if(b>>>0!==b||b>=z.length)return H.a(z,b)
return H.i(z[b],"$isx")},
j:function(a,b){this.a.appendChild(b)
return b},
gC:function(a){var z=this.E(this)
return new J.bE(z,z.length,0,[H.h(z,0)])},
gJ:function(a){var z=this.a.firstElementChild
if(z==null)throw H.b(P.aj("No elements"))
return z},
$asE:function(){return[W.x]},
$aso:function(){return[W.x]},
$asm:function(){return[W.x]}},
x:{"^":"n;0ct:tagName=",
gc6:function(a){return new W.hc(a)},
gbh:function(a){return new W.h7(a,a.children)},
h:function(a){return a.localName},
M:["as",function(a,b,c,d){var z,y,x,w
if(c==null){z=$.cC
if(z==null){z=H.l([],[W.X])
y=new W.cP(z)
C.b.j(z,W.ds(null))
C.b.j(z,W.dD())
$.cC=y
d=y}else d=z
z=$.cB
if(z==null){z=new W.dE(d)
$.cB=z
c=z}else{z.a=d
c=z}}if($.a5==null){z=document
y=z.implementation.createHTMLDocument("")
$.a5=y
$.bK=y.createRange()
y=$.a5
y.toString
y=y.createElement("base")
H.i(y,"$iscp")
y.href=z.baseURI
$.a5.head.appendChild(y)}z=$.a5
if(z.body==null){z.toString
y=z.createElement("body")
z.body=H.i(y,"$isbc")}z=$.a5
if(!!this.$isbc)x=z.body
else{y=a.tagName
z.toString
x=z.createElement(y)
$.a5.body.appendChild(x)}if("createContextualFragment" in window.Range.prototype&&!C.b.v(C.G,a.tagName)){$.bK.selectNodeContents(x)
w=$.bK.createContextualFragment(b)}else{x.innerHTML=b
w=$.a5.createDocumentFragment()
for(;z=x.firstChild,z!=null;)w.appendChild(z)}z=$.a5.body
if(x==null?z!=null:x!==z)J.cm(x)
c.aX(w)
document.adoptNode(w)
return w},function(a,b,c){return this.M(a,b,c,null)},"cb",null,null,"gcB",5,5,null],
bv:function(a,b,c,d){a.textContent=null
a.appendChild(this.M(a,b,c,d))},
R:function(a,b){return this.bv(a,b,null,null)},
$isx:1,
"%":";Element"},
ex:{"^":"e:8;",
$1:function(a){return!!J.q(H.i(a,"$isn")).$isx}},
a6:{"^":"C;",$isa6:1,"%":"AbortPaymentEvent|AnimationEvent|AnimationPlaybackEvent|ApplicationCacheErrorEvent|AudioProcessingEvent|BackgroundFetchClickEvent|BackgroundFetchEvent|BackgroundFetchFailEvent|BackgroundFetchedEvent|BeforeInstallPromptEvent|BeforeUnloadEvent|BlobEvent|CanMakePaymentEvent|ClipboardEvent|CloseEvent|CustomEvent|DeviceMotionEvent|DeviceOrientationEvent|ErrorEvent|ExtendableEvent|ExtendableMessageEvent|FetchEvent|FontFaceSetLoadEvent|ForeignFetchEvent|GamepadEvent|HashChangeEvent|IDBVersionChangeEvent|InstallEvent|MIDIConnectionEvent|MIDIMessageEvent|MediaEncryptedEvent|MediaKeyMessageEvent|MediaQueryListEvent|MediaStreamEvent|MediaStreamTrackEvent|MessageEvent|MojoInterfaceRequestEvent|MutationEvent|NotificationEvent|OfflineAudioCompletionEvent|PageTransitionEvent|PaymentRequestEvent|PaymentRequestUpdateEvent|PopStateEvent|PresentationConnectionAvailableEvent|PresentationConnectionCloseEvent|ProgressEvent|PromiseRejectionEvent|PushEvent|RTCDTMFToneChangeEvent|RTCDataChannelEvent|RTCPeerConnectionIceEvent|RTCTrackEvent|ResourceProgressEvent|SecurityPolicyViolationEvent|SensorErrorEvent|SpeechRecognitionError|SpeechRecognitionEvent|SpeechSynthesisEvent|StorageEvent|SyncEvent|TrackEvent|TransitionEvent|USBConnectionEvent|VRDeviceEvent|VRDisplayEvent|VRSessionEvent|WebGLContextEvent|WebKitTransitionEvent;Event|InputEvent"},
ao:{"^":"C;",
be:["bB",function(a,b,c,d){H.f(c,{func:1,args:[W.a6]})
if(c!=null)this.bN(a,b,c,!1)}],
bN:function(a,b,c,d){return a.addEventListener(b,H.aN(H.f(c,{func:1,args:[W.a6]}),1),!1)},
$isao:1,
"%":"IDBOpenDBRequest|IDBRequest|IDBVersionChangeRequest|MIDIInput|MIDIOutput|MIDIPort|ServiceWorker;EventTarget"},
ja:{"^":"M;0i:length=","%":"HTMLFormElement"},
jb:{"^":"ht;",
gi:function(a){return a.length},
m:function(a,b){if(b>>>0!==b||b>=a.length)throw H.b(P.ap(b,a,null,null,null))
return a[b]},
gJ:function(a){if(a.length>0)return a[0]
throw H.b(P.aj("No elements"))},
A:function(a,b){if(b>>>0!==b||b>=a.length)return H.a(a,b)
return a[b]},
$isa7:1,
$asa7:function(){return[W.n]},
$asE:function(){return[W.n]},
$iso:1,
$aso:function(){return[W.n]},
$ism:1,
$asm:function(){return[W.n]},
$asag:function(){return[W.n]},
"%":"HTMLCollection|HTMLFormControlsCollection|HTMLOptionsCollection"},
jf:{"^":"C;",
h:function(a){return String(a)},
"%":"Location"},
jh:{"^":"ao;",
be:function(a,b,c,d){H.f(c,{func:1,args:[W.a6]})
if(b==="message")a.start()
this.bB(a,b,c,!1)},
"%":"MessagePort"},
a8:{"^":"fN;",
gbn:function(a){var z,y,x,w,v,u
if(!!a.offsetX)return new P.ar(a.offsetX,a.offsetY,[P.r])
else{z=a.target
if(!J.q(W.dG(z)).$isx)throw H.b(P.I("offsetX is only supported on elements"))
y=H.i(W.dG(z),"$isx")
z=a.clientX
x=a.clientY
w=[P.r]
v=y.getBoundingClientRect()
u=v.left
v=v.top
H.H(new P.ar(u,v,w),"$isar",w,"$asar")
if(typeof z!=="number")return z.I()
if(typeof x!=="number")return x.I()
return new P.ar(C.e.a8(z-u),C.e.a8(x-v),w)}},
$isa8:1,
"%":"DragEvent|MouseEvent|PointerEvent|WheelEvent"},
Q:{"^":"bQ;a",
gJ:function(a){var z=this.a.firstChild
if(z==null)throw H.b(P.aj("No elements"))
return z},
gV:function(a){var z,y
z=this.a
y=z.childNodes.length
if(y===0)throw H.b(P.aj("No elements"))
if(y>1)throw H.b(P.aj("More than one element"))
return z.firstChild},
B:function(a,b){var z,y,x,w
H.H(b,"$iso",[W.n],"$aso")
z=b.a
y=this.a
if(z!==y)for(x=z.childNodes.length,w=0;w<x;++w)y.appendChild(z.firstChild)
return},
gC:function(a){var z=this.a.childNodes
return new W.cD(z,z.length,-1,[H.b6(C.K,z,"ag",0)])},
gi:function(a){return this.a.childNodes.length},
m:function(a,b){var z=this.a.childNodes
if(b>>>0!==b||b>=z.length)return H.a(z,b)
return z[b]},
$asE:function(){return[W.n]},
$aso:function(){return[W.n]},
$asm:function(){return[W.n]}},
n:{"^":"ao;0cn:previousSibling=",
cp:function(a){var z=a.parentNode
if(z!=null)z.removeChild(a)},
h:function(a){var z=a.nodeValue
return z==null?this.bC(a):z},
$isn:1,
"%":"Document|DocumentFragment|DocumentType|HTMLDocument|ShadowRoot|XMLDocument;Node"},
fn:{"^":"hB;",
gi:function(a){return a.length},
m:function(a,b){if(b>>>0!==b||b>=a.length)throw H.b(P.ap(b,a,null,null,null))
return a[b]},
gJ:function(a){if(a.length>0)return a[0]
throw H.b(P.aj("No elements"))},
A:function(a,b){if(b>>>0!==b||b>=a.length)return H.a(a,b)
return a[b]},
$isa7:1,
$asa7:function(){return[W.n]},
$asE:function(){return[W.n]},
$iso:1,
$aso:function(){return[W.n]},
$ism:1,
$asm:function(){return[W.n]},
$asag:function(){return[W.n]},
"%":"NodeList|RadioNodeList"},
jt:{"^":"M;0i:length=","%":"HTMLSelectElement"},
fJ:{"^":"M;",
M:function(a,b,c,d){var z,y
if("createContextualFragment" in window.Range.prototype)return this.as(a,b,c,d)
z=W.ew("<table>"+b+"</table>",c,d)
y=document.createDocumentFragment()
y.toString
z.toString
new W.Q(y).B(0,new W.Q(z))
return y},
"%":"HTMLTableElement"},
jv:{"^":"M;",
M:function(a,b,c,d){var z,y,x,w
if("createContextualFragment" in window.Range.prototype)return this.as(a,b,c,d)
z=document
y=z.createDocumentFragment()
z=C.t.M(z.createElement("table"),b,c,d)
z.toString
z=new W.Q(z)
x=z.gV(z)
x.toString
z=new W.Q(x)
w=z.gV(z)
y.toString
w.toString
new W.Q(y).B(0,new W.Q(w))
return y},
"%":"HTMLTableRowElement"},
jw:{"^":"M;",
M:function(a,b,c,d){var z,y,x
if("createContextualFragment" in window.Range.prototype)return this.as(a,b,c,d)
z=document
y=z.createDocumentFragment()
z=C.t.M(z.createElement("table"),b,c,d)
z.toString
z=new W.Q(z)
x=z.gV(z)
y.toString
x.toString
new W.Q(y).B(0,new W.Q(x))
return y},
"%":"HTMLTableSectionElement"},
cY:{"^":"M;",$iscY:1,"%":"HTMLTemplateElement"},
fN:{"^":"a6;","%":"CompositionEvent|FocusEvent|KeyboardEvent|TextEvent|TouchEvent;UIEvent"},
fS:{"^":"ao;",
gbf:function(a){var z,y,x
z=P.r
y=new P.a_(0,$.A,[z])
x=H.f(new W.fT(new P.hL(y,[z])),{func:1,ret:-1,args:[P.r]})
this.bV(a)
this.bZ(a,W.dN(x,z))
return y},
bZ:function(a,b){return a.requestAnimationFrame(H.aN(H.f(b,{func:1,ret:-1,args:[P.r]}),1))},
bV:function(a){if(!!(a.requestAnimationFrame&&a.cancelAnimationFrame))return;(function(b){var z=['ms','moz','webkit','o']
for(var y=0;y<z.length&&!b.requestAnimationFrame;++y){b.requestAnimationFrame=b[z[y]+'RequestAnimationFrame']
b.cancelAnimationFrame=b[z[y]+'CancelAnimationFrame']||b[z[y]+'CancelRequestAnimationFrame']}if(b.requestAnimationFrame&&b.cancelAnimationFrame)return
b.requestAnimationFrame=function(c){return window.setTimeout(function(){c(Date.now())},16)}
b.cancelAnimationFrame=function(c){clearTimeout(c)}})(a)},
$isda:1,
"%":"DOMWindow|Window"},
fT:{"^":"e:23;a",
$1:function(a){var z=this.a
a=H.bu(H.aQ(a),{futureOr:1,type:H.h(z,0)})
z=z.a
if(z.a!==0)H.w(P.aj("Future already completed"))
z.ay(a)}},
dc:{"^":"n;",$isdc:1,"%":"Attr"},
jF:{"^":"ev;",
h:function(a){return"Rectangle ("+H.d(a.left)+", "+H.d(a.top)+") "+H.d(a.width)+" x "+H.d(a.height)},
O:function(a,b){var z
if(b==null)return!1
z=H.aM(b,"$isaZ",[P.r],"$asaZ")
if(!z)return!1
z=J.ae(b)
return a.left===z.gan(b)&&a.top===z.gaT(b)&&a.width===z.gap(b)&&a.height===z.gam(b)},
gD:function(a){return W.dv(a.left&0x1FFFFFFF,a.top&0x1FFFFFFF,a.width&0x1FFFFFFF,a.height&0x1FFFFFFF)},
gam:function(a){return a.height},
gap:function(a){return a.width},
gp:function(a){return a.x},
gq:function(a){return a.y},
"%":"ClientRect|DOMRect"},
jK:{"^":"hV;",
gi:function(a){return a.length},
m:function(a,b){if(b>>>0!==b||b>=a.length)throw H.b(P.ap(b,a,null,null,null))
return a[b]},
gJ:function(a){if(a.length>0)return a[0]
throw H.b(P.aj("No elements"))},
A:function(a,b){if(b>>>0!==b||b>=a.length)return H.a(a,b)
return a[b]},
$isa7:1,
$asa7:function(){return[W.n]},
$asE:function(){return[W.n]},
$iso:1,
$aso:function(){return[W.n]},
$ism:1,
$asm:function(){return[W.n]},
$asag:function(){return[W.n]},
"%":"MozNamedAttrMap|NamedNodeMap"},
fZ:{"^":"cM;b4:a<",
al:function(a,b){var z,y,x,w,v
H.f(b,{func:1,ret:-1,args:[P.j,P.j]})
for(z=this.gU(),y=z.length,x=this.a,w=0;w<z.length;z.length===y||(0,H.b9)(z),++w){v=z[w]
b.$2(v,x.getAttribute(v))}},
gU:function(){var z,y,x,w,v
z=this.a.attributes
y=H.l([],[P.j])
for(x=z.length,w=0;w<x;++w){if(w>=z.length)return H.a(z,w)
v=H.i(z[w],"$isdc")
if(v.namespaceURI==null)C.b.j(y,v.name)}return y},
$asbh:function(){return[P.j,P.j]},
$asbR:function(){return[P.j,P.j]}},
hc:{"^":"fZ;a",
m:function(a,b){return this.a.getAttribute(H.u(b))},
gi:function(a){return this.gU().length}},
hd:{"^":"bW;$ti",
ck:function(a,b,c,d){var z=H.h(this,0)
H.f(a,{func:1,ret:-1,args:[z]})
H.f(c,{func:1,ret:-1})
return W.bl(this.a,this.b,a,!1,z)}},
jG:{"^":"hd;a,b,c,$ti"},
he:{"^":"fE;a,b,c,d,e,$ti",
c4:function(){var z=this.d
if(z!=null&&this.a<=0)J.e8(this.b,this.c,z,!1)},
l:{
bl:function(a,b,c,d,e){var z=c==null?null:W.dN(new W.hf(c),W.a6)
z=new W.he(0,a,b,z,!1,[e])
z.c4()
return z}}},
hf:{"^":"e:22;a",
$1:function(a){return this.a.$1(H.i(a,"$isa6"))}},
b1:{"^":"c;a",
bI:function(a){var z,y
z=$.$get$c4()
if(z.a===0){for(y=0;y<262;++y)z.F(0,C.F[y],W.iq())
for(y=0;y<12;++y)z.F(0,C.j[y],W.ir())}},
Y:function(a){return $.$get$dt().v(0,W.aB(a))},
T:function(a,b,c){var z,y,x
z=W.aB(a)
y=$.$get$c4()
x=y.m(0,H.d(z)+"::"+b)
if(x==null)x=y.m(0,"*::"+b)
if(x==null)return!1
return H.ih(x.$4(a,b,c,this))},
$isX:1,
l:{
ds:function(a){var z,y
z=document.createElement("a")
y=new W.hG(z,window.location)
y=new W.b1(y)
y.bI(a)
return y},
jH:[function(a,b,c,d){H.i(a,"$isx")
H.u(b)
H.u(c)
H.i(d,"$isb1")
return!0},"$4","iq",16,0,9],
jI:[function(a,b,c,d){var z,y,x,w,v
H.i(a,"$isx")
H.u(b)
H.u(c)
z=H.i(d,"$isb1").a
y=z.a
y.href=c
x=y.hostname
z=z.b
w=z.hostname
if(x==null?w==null:x===w){w=y.port
v=z.port
if(w==null?v==null:w===v){w=y.protocol
z=z.protocol
z=w==null?z==null:w===z}else z=!1}else z=!1
if(!z)if(x==="")if(y.port===""){z=y.protocol
z=z===":"||z===""}else z=!1
else z=!1
else z=!0
return z},"$4","ir",16,0,9]}},
ag:{"^":"c;$ti",
gC:function(a){return new W.cD(a,this.gi(a),-1,[H.b6(this,a,"ag",0)])}},
cP:{"^":"c;a",
Y:function(a){return C.b.a4(this.a,new W.fp(a))},
T:function(a,b,c){return C.b.a4(this.a,new W.fo(a,b,c))},
$isX:1},
fp:{"^":"e:16;a",
$1:function(a){return H.i(a,"$isX").Y(this.a)}},
fo:{"^":"e:16;a,b,c",
$1:function(a){return H.i(a,"$isX").T(this.a,this.b,this.c)}},
hH:{"^":"c;",
bJ:function(a,b,c,d){var z,y,x
this.a.B(0,c)
z=b.aV(0,new W.hI())
y=b.aV(0,new W.hJ())
this.b.B(0,z)
x=this.c
x.B(0,C.H)
x.B(0,y)},
Y:function(a){return this.a.v(0,W.aB(a))},
T:["bF",function(a,b,c){var z,y
z=W.aB(a)
y=this.c
if(y.v(0,H.d(z)+"::"+b))return this.d.c5(c)
else if(y.v(0,"*::"+b))return this.d.c5(c)
else{y=this.b
if(y.v(0,H.d(z)+"::"+b))return!0
else if(y.v(0,"*::"+b))return!0
else if(y.v(0,H.d(z)+"::*"))return!0
else if(y.v(0,"*::*"))return!0}return!1}],
$isX:1},
hI:{"^":"e:17;",
$1:function(a){return!C.b.v(C.j,H.u(a))}},
hJ:{"^":"e:17;",
$1:function(a){return C.b.v(C.j,H.u(a))}},
hN:{"^":"hH;e,a,b,c,d",
T:function(a,b,c){if(this.bF(a,b,c))return!0
if(b==="template"&&c==="")return!0
if(a.getAttribute("template")==="")return this.e.v(0,b)
return!1},
l:{
dD:function(){var z,y,x,w,v
z=P.j
y=P.aX(C.i,z)
x=H.h(C.i,0)
w=H.f(new W.hO(),{func:1,ret:z,args:[x]})
v=H.l(["TEMPLATE"],[z])
y=new W.hN(y,P.aW(null,null,null,z),P.aW(null,null,null,z),P.aW(null,null,null,z),null)
y.bJ(null,new H.P(C.i,w,[x,z]),v,null)
return y}}},
hO:{"^":"e:21;",
$1:function(a){return"TEMPLATE::"+H.d(H.u(a))}},
hK:{"^":"c;",
Y:function(a){var z=J.q(a)
if(!!z.$iscU)return!1
z=!!z.$isy
if(z&&W.aB(a)==="foreignObject")return!1
if(z)return!0
return!1},
T:function(a,b,c){if(b==="is"||C.c.by(b,"on"))return!1
return this.Y(a)},
$isX:1},
cD:{"^":"c;a,b,c,0d,$ti",
n:function(){var z,y
z=this.c+1
y=this.b
if(z<y){this.d=J.e7(this.a,z)
this.c=z
return!0}this.d=null
this.c=y
return!1},
gu:function(){return this.d}},
ha:{"^":"c;a",$isao:1,$isda:1,l:{
hb:function(a){if(a===window)return H.i(a,"$isda")
else return new W.ha(a)}}},
X:{"^":"c;"},
hG:{"^":"c;a,b",$isjz:1},
dE:{"^":"c;a",
aX:function(a){new W.hS(this).$2(a,null)},
a2:function(a,b){if(b==null)J.cm(a)
else b.removeChild(a)},
c1:function(a,b){var z,y,x,w,v,u,t,s
z=!0
y=null
x=null
try{y=J.e9(a)
x=y.gb4().getAttribute("is")
w=function(c){if(!(c.attributes instanceof NamedNodeMap))return true
var r=c.childNodes
if(c.lastChild&&c.lastChild!==r[r.length-1])return true
if(c.children)if(!(c.children instanceof HTMLCollection||c.children instanceof NodeList))return true
var q=0
if(c.children)q=c.children.length
for(var p=0;p<q;p++){var o=c.children[p]
if(o.id=='attributes'||o.name=='attributes'||o.id=='lastChild'||o.name=='lastChild'||o.id=='children'||o.name=='children')return true}return false}(a)
z=w?!0:!(a.attributes instanceof NamedNodeMap)}catch(t){H.V(t)}v="element unprintable"
try{v=J.ay(a)}catch(t){H.V(t)}try{u=W.aB(a)
this.c0(H.i(a,"$isx"),b,z,v,u,H.i(y,"$isbR"),H.u(x))}catch(t){if(H.V(t) instanceof P.a3)throw t
else{this.a2(a,b)
window
s="Removing corrupted element "+H.d(v)
if(typeof console!="undefined")window.console.warn(s)}}},
c0:function(a,b,c,d,e,f,g){var z,y,x,w,v
if(c){this.a2(a,b)
window
z="Removing element due to corrupted attributes on <"+d+">"
if(typeof console!="undefined")window.console.warn(z)
return}if(!this.a.Y(a)){this.a2(a,b)
window
z="Removing disallowed element <"+H.d(e)+"> from "+H.d(b)
if(typeof console!="undefined")window.console.warn(z)
return}if(g!=null)if(!this.a.T(a,"is",g)){this.a2(a,b)
window
z="Removing disallowed type extension <"+H.d(e)+' is="'+g+'">'
if(typeof console!="undefined")window.console.warn(z)
return}z=f.gU()
y=H.l(z.slice(0),[H.h(z,0)])
for(x=f.gU().length-1,z=f.a;x>=0;--x){if(x>=y.length)return H.a(y,x)
w=y[x]
if(!this.a.T(a,J.ed(w),z.getAttribute(w))){window
v="Removing disallowed attribute <"+H.d(e)+" "+w+'="'+H.d(z.getAttribute(w))+'">'
if(typeof console!="undefined")window.console.warn(v)
z.getAttribute(w)
z.removeAttribute(w)}}if(!!J.q(a).$iscY)this.aX(a.content)},
$isjp:1},
hS:{"^":"e:32;a",
$2:function(a,b){var z,y,x,w,v,u
x=this.a
switch(a.nodeType){case 1:x.c1(a,b)
break
case 8:case 11:case 3:case 4:break
default:x.a2(a,b)}z=a.lastChild
for(x=a==null;null!=z;){y=null
try{y=J.eb(z)}catch(w){H.V(w)
v=H.i(z,"$isn")
if(x){u=v.parentNode
if(u!=null)u.removeChild(v)}else a.removeChild(v)
z=null
y=a.lastChild}if(z!=null)this.$2(z,a)
z=H.i(y,"$isn")}}},
h9:{"^":"C+er;"},
hs:{"^":"C+E;"},
ht:{"^":"hs+ag;"},
hA:{"^":"C+E;"},
hB:{"^":"hA+ag;"},
hU:{"^":"C+E;"},
hV:{"^":"hU+ag;"}}],["","",,P,{"^":"",
cz:function(){var z=$.cy
if(z==null){z=J.bC(window.navigator.userAgent,"Opera",0)
$.cy=z}return z},
et:function(){var z,y
z=$.cv
if(z!=null)return z
y=$.cw
if(y==null){y=J.bC(window.navigator.userAgent,"Firefox",0)
$.cw=y}if(y)z="-moz-"
else{y=$.cx
if(y==null){y=!P.cz()&&J.bC(window.navigator.userAgent,"Trident/",0)
$.cx=y}if(y)z="-ms-"
else z=P.cz()?"-o-":"-webkit-"}$.cv=z
return z},
ez:{"^":"bQ;a,b",
gaE:function(){var z,y,x
z=this.b
y=H.af(z,"E",0)
x=W.x
return new H.cN(new H.bY(z,H.f(new P.eA(),{func:1,ret:P.t,args:[y]}),[y]),H.f(new P.eB(),{func:1,ret:x,args:[y]}),[y,x])},
j:function(a,b){this.b.a.appendChild(b)},
v:function(a,b){return!1},
gi:function(a){return J.T(this.gaE().a)},
m:function(a,b){var z=this.gaE()
return z.b.$1(J.ba(z.a,b))},
gC:function(a){var z=P.ah(this.gaE(),!1,W.x)
return new J.bE(z,z.length,0,[H.h(z,0)])},
$asE:function(){return[W.x]},
$aso:function(){return[W.x]},
$asm:function(){return[W.x]}},
eA:{"^":"e:8;",
$1:function(a){return!!J.q(H.i(a,"$isn")).$isx}},
eB:{"^":"e:38;",
$1:function(a){return H.iy(H.i(a,"$isn"),"$isx")}}}],["","",,P,{"^":""}],["","",,P,{"^":"",
du:function(a,b){a=536870911&a+b
a=536870911&a+((524287&a)<<10)
return a^a>>>6},
hx:function(a){a=536870911&a+((67108863&a)<<3)
a^=a>>>11
return 536870911&a+((16383&a)<<15)},
hw:{"^":"c;",
cm:function(a){if(a<=0||a>4294967296)throw H.b(P.fv("max must be in range 0 < max \u2264 2^32, was "+a))
return Math.random()*a>>>0},
$isjr:1},
ar:{"^":"c;p:a>,q:b>,$ti",
h:function(a){return"Point("+H.d(this.a)+", "+H.d(this.b)+")"},
O:function(a,b){var z,y,x
if(b==null)return!1
z=H.aM(b,"$isar",[P.r],null)
if(!z)return!1
z=this.a
y=J.ae(b)
x=y.gp(b)
if(z==null?x==null:z===x){z=this.b
y=y.gq(b)
y=z==null?y==null:z===y
z=y}else z=!1
return z},
gD:function(a){var z,y
z=J.an(this.a)
y=J.an(this.b)
return P.hx(P.du(P.du(0,z),y))},
G:function(a,b){var z,y,x
z=this.a
if(typeof z!=="number")return z.G()
y=H.h(this,0)
z=H.p(z*b,y)
x=this.b
if(typeof x!=="number")return x.G()
return new P.ar(z,H.p(x*b,y),this.$ti)}}}],["","",,P,{"^":"",iR:{"^":"y;0p:x=,0q:y=","%":"SVGFEBlendElement"},iS:{"^":"y;0p:x=,0q:y=","%":"SVGFEColorMatrixElement"},iT:{"^":"y;0p:x=,0q:y=","%":"SVGFEComponentTransferElement"},iU:{"^":"y;0p:x=,0q:y=","%":"SVGFECompositeElement"},iV:{"^":"y;0p:x=,0q:y=","%":"SVGFEConvolveMatrixElement"},iW:{"^":"y;0p:x=,0q:y=","%":"SVGFEDiffuseLightingElement"},iX:{"^":"y;0p:x=,0q:y=","%":"SVGFEDisplacementMapElement"},iY:{"^":"y;0p:x=,0q:y=","%":"SVGFEFloodElement"},iZ:{"^":"y;0p:x=,0q:y=","%":"SVGFEGaussianBlurElement"},j_:{"^":"y;0p:x=,0q:y=","%":"SVGFEImageElement"},j0:{"^":"y;0p:x=,0q:y=","%":"SVGFEMergeElement"},j1:{"^":"y;0p:x=,0q:y=","%":"SVGFEMorphologyElement"},j2:{"^":"y;0p:x=,0q:y=","%":"SVGFEOffsetElement"},j3:{"^":"y;0p:x=,0q:y=","%":"SVGFEPointLightElement"},j4:{"^":"y;0p:x=,0q:y=","%":"SVGFESpecularLightingElement"},j5:{"^":"y;0p:x=,0q:y=","%":"SVGFESpotLightElement"},j6:{"^":"y;0p:x=,0q:y=","%":"SVGFETileElement"},j7:{"^":"y;0p:x=,0q:y=","%":"SVGFETurbulenceElement"},j8:{"^":"y;0p:x=,0q:y=","%":"SVGFilterElement"},j9:{"^":"aD;0p:x=,0q:y=","%":"SVGForeignObjectElement"},eY:{"^":"aD;","%":"SVGCircleElement|SVGEllipseElement|SVGLineElement|SVGPathElement|SVGPolygonElement|SVGPolylineElement;SVGGeometryElement"},aD:{"^":"y;","%":"SVGAElement|SVGClipPathElement|SVGDefsElement|SVGGElement|SVGSwitchElement;SVGGraphicsElement"},jc:{"^":"aD;0p:x=,0q:y=","%":"SVGImageElement"},jg:{"^":"y;0p:x=,0q:y=","%":"SVGMaskElement"},jq:{"^":"y;0p:x=,0q:y=","%":"SVGPatternElement"},js:{"^":"eY;0p:x=,0q:y=","%":"SVGRectElement"},cU:{"^":"y;",$iscU:1,"%":"SVGScriptElement"},y:{"^":"x;",
gbh:function(a){return new P.ez(a,new W.Q(a))},
M:function(a,b,c,d){var z,y,x,w,v,u
z=H.l([],[W.X])
C.b.j(z,W.ds(null))
C.b.j(z,W.dD())
C.b.j(z,new W.hK())
c=new W.dE(new W.cP(z))
y='<svg version="1.1">'+b+"</svg>"
z=document
x=z.body
w=(x&&C.l).cb(x,y,c)
v=z.createDocumentFragment()
w.toString
z=new W.Q(w)
u=z.gV(z)
for(;z=u.firstChild,z!=null;)v.appendChild(z)
return v},
$isy:1,
"%":"SVGAnimateElement|SVGAnimateMotionElement|SVGAnimateTransformElement|SVGAnimationElement|SVGComponentTransferFunctionElement|SVGDescElement|SVGDiscardElement|SVGFEDistantLightElement|SVGFEDropShadowElement|SVGFEFuncAElement|SVGFEFuncBElement|SVGFEFuncGElement|SVGFEFuncRElement|SVGFEMergeNodeElement|SVGGradientElement|SVGLinearGradientElement|SVGMPathElement|SVGMarkerElement|SVGMetadataElement|SVGRadialGradientElement|SVGSetElement|SVGStopElement|SVGStyleElement|SVGSymbolElement|SVGTitleElement|SVGViewElement;SVGElement"},ju:{"^":"aD;0p:x=,0q:y=","%":"SVGSVGElement"},fK:{"^":"aD;","%":"SVGTextPathElement;SVGTextContentElement"},jx:{"^":"fK;0p:x=,0q:y=","%":"SVGTSpanElement|SVGTextElement|SVGTextPositioningElement"},jA:{"^":"aD;0p:x=,0q:y=","%":"SVGUseElement"}}],["","",,P,{"^":""}],["","",,P,{"^":""}],["","",,P,{"^":""}],["","",,T,{"^":"",
jL:[function(a){return C.c.a6(C.a.aa(J.bD(H.aQ(a)),16),2,"0")},"$1","e3",4,0,14],
i6:function(a){var z,y,x,w,v
z=$.$get$b3()
y=P.j
z=H.l(H.av(a,z,"").split(","),[y])
x=P.r
w=H.h(z,0)
v=new H.P(z,H.f(P.bs(),{func:1,ret:x,args:[w]}),[w,x]).E(0)
if(v.length!==3||C.b.a4(v,new T.i7()))throw H.b(P.B("Unrecognized color: '"+a+"'."))
z=H.h(v,0)
return("#"+new H.P(v,H.f(T.e3(),{func:1,ret:y,args:[z]}),[z,y]).N(0)+"FF").toUpperCase()},
i8:function(a){var z,y,x,w,v
z=$.$get$b3()
y=P.j
z=H.l(H.av(a,z,"").split(","),[y])
x=P.r
w=H.h(z,0)
v=new H.P(z,H.f(P.bs(),{func:1,ret:x,args:[w]}),[w,x]).E(0)
if(v.length!==4||C.b.a4(C.b.ad(v,0,3),new T.i9())||J.cj(C.b.gZ(v),0)||J.e6(C.b.gZ(v),1))throw H.b(P.B("Unrecognized color: '"+a+"'."))
z=C.b.ad(v,0,3)
x=H.h(z,0)
return("#"+new H.P(z,H.f(T.e3(),{func:1,ret:y,args:[x]}),[x,y]).N(0)+C.c.a6(C.a.aa(J.bD(J.bB(C.b.gZ(v),255)),16),2,"0")).toUpperCase()},
hW:function(a){var z,y
z={}
z.a=a
y=$.$get$br()
a=H.av(a,y,"")
z.a=a
if(a.length!==4)throw H.b(P.B("Unrecognized color: '"+a+"'."))
return("#"+C.b.N(P.aY(3,new T.hX(z),!0,P.j))+"FF").toUpperCase()},
hY:function(a){var z,y
z={}
z.a=a
y=$.$get$br()
a=H.av(a,y,"")
z.a=a
if(a.length!==5)throw H.b(P.B("Unrecognized color: '"+a+"'."))
return("#"+C.b.N(P.aY(4,new T.hZ(z),!0,P.j))).toUpperCase()},
dH:function(a){var z,y
z={}
z.a=a
y=$.$get$br()
a=H.av(a,y,"")
z.a=a
if(a.length!==7)throw H.b(P.B("Unrecognized color: '"+a+"'."))
return("#"+C.b.N(P.aY(3,new T.i_(z),!0,P.j))+"FF").toUpperCase()},
dI:function(a){var z,y,x,w,v,u,t,s,r,q,p,o,n,m
z=$.$get$b3()
y=P.j
z=H.l(H.av(a,z,"").split(","),[y])
x=P.r
w=H.h(z,0)
v=new H.P(z,H.f(P.bs(),{func:1,ret:x,args:[w]}),[w,x]).E(0)
z=v.length
if(z!==3)throw H.b(P.B("Unrecognized color: '"+a+"'."))
if(0>=z)return H.a(v,0)
u=v[0]
if(1>=z)return H.a(v,1)
t=J.ci(v[1],100)
if(2>=v.length)return H.a(v,2)
s=J.ci(v[2],100)
if(typeof u!=="number")return u.S()
if(u<0||u>=360)throw H.b(P.B("Unrecognized color: '"+a+"'."))
if(t<0||t>100)throw H.b(P.B("Unrecognized color: '"+a+"'."))
if(s<0||s>100)throw H.b(P.B("Unrecognized color: '"+a+"'."))
r=(1-Math.abs(2*s-1))*t
q=u/60
p=r*(1-Math.abs(C.n.w(q,2)-1))
if(q<=1){o=p
n=r
m=0}else if(q<=2){o=r
n=p
m=0}else if(q<=3){m=p
o=r
n=0}else if(q<=4){m=r
o=p
n=0}else{if(q<=5){m=r
n=p}else{m=p
n=r}o=0}z=H.l([n,o,m],[x])
x=H.h(z,0)
return("#"+new H.P(z,H.f(new T.i1(s-r/2),{func:1,ret:y,args:[x]}),[x,y]).N(0)+"FF").toUpperCase()},
ch:function(a,b){var z,y,x,w,v
a=C.c.aU(a).toUpperCase()
z=a.length
if(0>=z)return H.a(a,0)
if(a[0]==="#")switch(z){case 4:y=T.hW(a)
break
case 5:y=T.hY(a)
break
case 7:y=T.dH(a)
break
case 9:y=a
break
default:throw H.b(P.B("Unrecognized color: "+a))}else if(C.c.v(a,"RGBA"))y=T.i8(a)
else if(C.c.v(a,"RGB"))y=T.i6(a)
else if(C.c.v(a,"HSLA")){z=$.$get$b3()
z=H.l(H.av(a,z,"").split(","),[P.j])
x=P.r
w=H.h(z,0)
v=new H.P(z,H.f(P.bs(),{func:1,ret:x,args:[w]}),[w,x]).E(0)
if(v.length!==4)H.w(P.B("Unrecognized color: '"+a+"'."))
y=C.c.W(T.dI("hsl("+C.b.a5(C.b.ad(v,0,3),",")+")"),0,7)+C.c.a6(C.a.aa(J.bD(J.bB(C.b.gZ(v),255)),16),2,"0")}else if(C.c.v(a,"HSL"))y=T.dI(a)
else{a=a.toLowerCase()
if(!C.q.ak(a))throw H.b(P.B("Unrecognized color: '"+a+"'."))
y=T.dH("#"+H.d(C.q.m(0,a)))}return(C.c.W(y,0,7)+C.c.a6(C.a.aa(C.n.a7(b*255),16),2,"0")).toUpperCase()},
i7:{"^":"e:18;",
$1:function(a){H.aQ(a)
if(typeof a!=="number")return a.S()
return a<0||a>255.5}},
i9:{"^":"e:18;",
$1:function(a){H.aQ(a)
if(typeof a!=="number")return a.S()
return a<0||a>255.5}},
hX:{"^":"e:4;a",
$1:function(a){var z,y
z=this.a.a
y=a+1
if(y>=z.length)return H.a(z,y)
return C.c.G(z[y],2)}},
hZ:{"^":"e:4;a",
$1:function(a){var z,y
z=this.a.a
y=a+1
if(y>=z.length)return H.a(z,y)
return C.c.G(z[y],2)}},
i_:{"^":"e:4;a",
$1:function(a){var z=a*2
return C.c.W(this.a.a,z+1,z+3)}},
i1:{"^":"e:14;a",
$1:function(a){H.aQ(a)
if(typeof a!=="number")return a.H()
return C.c.a6(C.a.aa(C.e.a7((a+this.a)*255),16),2,"0")}}}],["","",,G,{"^":"",
a0:function(a){var z,y
if($.$get$bo().ak(a))z=$.$get$bo().m(0,a)
else if(a<2)z=$.$get$aa()
else{z=$.$get$bo()
y=P.c_(a).G(0,G.a0(a-1))
z.F(0,a,y)
z=y}return z},
dF:function(a,b,c){var z,y,x,w,v,u,t,s
if(b===0)return[]
z=c.length
y=z-0-1
x=b-1
w=G.a0(y).t(0,G.a0(y-x)).t(0,G.a0(x))
for(v=0;a.aj(0,w)>=0;){a=a.I(0,w);++v
y=z-v-1
u=G.a0(y)
t=G.a0(y-x)
u.toString
if(t.c===0)H.w(C.f)
y=u.ae(t)
t=G.a0(x)
y.toString
if(t.c===0)H.w(C.f)
w=y.ae(t)}s=C.b.aY(c,C.a.a8(v)+1)
y=C.a.a8(v)
if(y<0||y>=c.length)return H.a(c,y)
y=[c[y]]
C.b.B(y,G.dF(a,x,s))
return y},
em:{"^":"en;0c,0a,0b",
h:function(a){return"Pseudo-list containing all "+H.d(this.b)+" "+this.c+"-combinations of items from "+H.d(P.ah(this.a,!1,null))+"."},
l:{
bI:function(a,b){var z,y
z=new G.em()
y=b.length
if(a>y)H.w(P.B("Cannot take "+a+" items from "+b.length+"."))
if(P.aX(b,H.h(b,0)).a!==b.length)H.w(P.B("Items are not unique."))
z.a=P.ah(b,!1,null)
z.c=a
y=b.length
z.b=G.a0(y).t(0,G.a0(y-a)).t(0,G.a0(a))
return z}}},
en:{"^":"c;",
b8:function(a,b){var z=this
return P.i3(function(){var y=a,x=b
var w=0,v=1,u,t
return function $async$b8(c,d){if(c===1){u=d
w=v}while(true)switch(w){case 0:case 2:t=y.w(0,z.b)
if(!!!J.q(t).$isa4){H.w(P.B("Index must be an int or BigInt."))
t=null}w=5
return G.dF(t.w(0,z.b),z.c,P.ah(z.a,!1,null))
case 5:y=y.H(0,$.$get$aa())
case 3:if(!J.ax(y.w(0,z.b),x.w(0,z.b))){w=2
break}case 4:return P.hu()
case 1:return P.hv(u)}}},[P.m,P.c])},
$2:function(a,b){var z,y,x
if(a==null&&b==null){z=$.$get$L()
y=this.b}else{if(typeof a==="number"&&Math.floor(a)===a)z=P.c_(a)
else{if(!!!J.q(a).$isa4)throw H.b(P.B("Expecting int or BigInt in range."))
z=a}if(b==null){x=$.$get$L()
y=z
z=x}else if(typeof b==="number"&&Math.floor(b)===b)y=P.c_(b)
else{if(!!!J.q(b).$isa4)throw H.b(P.B("Expecting int or BigInt in range."))
y=b}}return this.b8(z,y)},
$1:function(a){return this.$2(a,null)},
$0:function(){return this.$2(null,null)},
gi:function(a){return this.b}}}],["","",,F,{"^":"",
dZ:function(){return F.eE(document.querySelector("#game"))},
ik:{"^":"e:7;",
$0:function(){var z,y,x,w,v,u,t,s
z=W.aA(600,600)
for(y=0;y<3;++y){x=$.$get$dS()[y]
for(w=(y+0.5)*200,v=0;v<3;++v){u=z.getContext("2d")
u.save()
u.lineCap="round"
u.lineJoin="round"
u.lineWidth=2
u.translate(w,(v+0.5)*200)
t=new F.il(z,v+3,v,200)
t.$3(0.85,"white","rgba(0,0,0,0)")
t.$3(0.85,T.ch(x,0.5),"rgba(0,0,0,0)")
for(u=[0.65,0.45],s=0;s<2;++s)t.$3(u[s],T.ch("white",0.25),"rgba(0,0,0,0)")
t.$3(0.85,"rgba(0,0,0,0)",x)
t.$3(0.5,"white",x)
t.$3(0.25,x,"rgba(0,0,0,0)")
z.getContext("2d").restore()}}return z}},
il:{"^":"e;a,b,c,d",
$3:function(a,b,c){var z,y,x,w,v,u,t,s,r,q,p,o
z=this.a
y=z.getContext("2d")
y.beginPath()
y.fillStyle=b
y.strokeStyle=c
x=this.b*2
for(y=6.283185307179586/x,w=[P.aO],v=this.c,u=this.d,t=0;t<x;++t){s=y*t
r=[H.l([0.22,0.5],w),H.l([0.32,0.5],w),H.l([0.38,0.5],w)]
if(v>=3)return H.a(r,v)
r=r[v][t%2]
if(typeof a!=="number")return H.a2(a)
q=r*u*a
p=Math.cos(s)*q
o=Math.sin(s)*q
if(t===0)z.getContext("2d").moveTo(p,o)
else z.getContext("2d").lineTo(p,o)}z=z.getContext("2d")
z.closePath()
z.fill()
z.stroke()}},
ii:{"^":"e:7;",
$0:function(){var z,y,x,w,v,u,t,s
z=$.$get$b8()
y=z.width
if(typeof y!=="number")return y.t()
y=C.a.k(y,3)
z=z.height
if(typeof z!=="number")return z.t()
z=C.a.k(z,3)
x=W.aA(z,y)
w=x.getContext("2d")
w.save()
v=x.width
if(typeof v!=="number")return v.t()
v=C.a.k(v,2)
u=x.height
if(typeof u!=="number")return u.t()
w.translate(v,C.a.k(u,2))
for(w=$.$get$bA(),t=0;t<3;++t){s=w[t]
v=x.getContext("2d")
v.strokeStyle="darkgray"
v.fillStyle="white"
v.beginPath()
u=s-0.1
v.ellipse(0,0,u*y/2,u*z/2,0,0,6.283185307179586,!1)
v.fill()
v.stroke()}x.getContext("2d").restore()
return x}},
ij:{"^":"e:7;",
$0:function(){var z,y,x,w,v,u,t,s
z=$.$get$b8()
y=z.width
if(typeof y!=="number")return y.t()
y=C.a.k(y,3)
z=z.height
if(typeof z!=="number")return z.t()
z=C.a.k(z,3)
x=W.aA(z,y)
w=x.getContext("2d")
w.save()
v=x.width
if(typeof v!=="number")return v.t()
v=C.a.k(v,2)
u=x.height
if(typeof u!=="number")return u.t()
w.translate(v,C.a.k(u,2))
for(w=$.$get$bA(),t=0;t<3;++t){s=w[t]
v=x.getContext("2d")
v.strokeStyle="black"
v.lineWidth=1
v.fillStyle="lightyellow"
v.beginPath()
u=s-0.1
v.ellipse(0,0,u*y/2,u*z/2,0,0,6.283185307179586,!1)
v.fill()
v.stroke()}x.getContext("2d").restore()
return x}},
v:{"^":"c;a",
bH:function(a){var z
if(a.length!==4||C.b.a4(a,new F.f1()))throw H.b(P.B("Not acceptable attributes: "+H.d(a)))
z=P.k
this.a=C.b.ce(P.aY(4,new F.f2(a,H.l([27,9,3,1],[z])),!0,z),0,new F.f3(),z)},
gaQ:function(){var z=this.a
if(typeof z!=="number")return z.w()
return C.a.k(C.a.w(C.a.w(z,27),9),3)},
gar:function(a){var z=this.a
if(typeof z!=="number")return z.w()
return C.a.w(C.a.w(C.a.w(z,27),9),3)},
cj:function(a){var z,y
z=this.a
y=a.a
return z==null?y==null:z===y},
h:function(a){var z,y,x,w,v
z=["red","green","purple"]
y=this.a
if(typeof y!=="number")return y.t()
x=C.a.k(y,27)
if(x<0||x>=3)return H.a(z,x)
x=z[x]
z=["triangle","square","pentagram"]
y=C.a.k(C.a.w(y,27),9)
if(y<0||y>=3)return H.a(z,y)
y=z[y]
z=["still","clockwise","counterclockwise"]
w=this.gaQ()
if(w<0||w>=3)return H.a(z,w)
w=z[w]
z=["large","medium","small"]
v=this.gar(this)
if(v<0||v>=3)return H.a(z,v)
return C.b.a5(H.l([x,y,w,z[v]],[P.j]),"-")},
l:{
f0:function(a){var z=new F.v(null)
z.bH(a)
return z}}},
f1:{"^":"e:20;",
$1:function(a){H.z(a)
if(typeof a!=="number")return a.S()
return a<0||a>=3}},
f2:{"^":"e:3;a,b",
$1:function(a){var z,y
z=this.a
if(a>=z.length)return H.a(z,a)
z=z[a]
y=this.b
if(a>=4)return H.a(y,a)
return J.bB(z,y[a])}},
f3:{"^":"e:11;",
$2:function(a,b){H.z(a)
H.z(b)
if(typeof a!=="number")return a.H()
if(typeof b!=="number")return H.a2(b)
return a+b}},
eD:{"^":"c;0a,0b,0c,0d,0e,0f,0r,0x,0y,0z,0Q",
bG:function(a){var z,y,x,w,v,u,t,s
z=document
y=z.createElement("div")
y.id="board-container"
C.h.R(y,"<div class='game-heading'>Board</div>")
x=$.$get$b8()
w=x.width
w=W.aA(x.height,w)
x=w.style
x.cursor="pointer"
x=W.a8
v={func:1,ret:-1,args:[x]}
W.bl(w,"click",H.f(this.gca(this),v),!1,x)
this.c=w
y.appendChild(w)
u=z.createElement("div")
u.id="messages-container"
C.h.R(u,"<div class='game-heading'>Messages</div>")
w=z.createElement("div")
w.id="messages"
this.a=w
u.appendChild(w)
w=z.createElement("button")
C.m.R(w,"Show lines.")
W.bl(w,"click",H.f(new F.eL(this),v),!1,x)
u.appendChild(w)
w=z.createElement("button")
C.m.R(w,"Show planes.")
W.bl(w,"click",H.f(new F.eM(this),v),!1,x)
u.appendChild(w)
this.b=H.l([],[P.j])
t=z.createElement("div")
t.id="lines-found-container"
C.h.R(t,"<div class='game-heading'>Lines found</div>")
w=W.aA(null,null)
this.d=w
t.appendChild(w)
s=z.createElement("div")
s.id="planes-found-container"
C.h.R(s,"<div class='game-heading'>Planes found</div>")
z=W.aA(null,null)
this.e=z
s.appendChild(z)
z=J.ea(a)
z.j(0,y)
z.j(0,u)
z.j(0,t)
z.j(0,s)
z=[[P.K,P.k]]
this.x=H.l([],z)
w=[[P.m,P.k]]
this.z=H.l([],w)
this.y=H.l([],z)
this.Q=H.l([],w)
this.r=P.aW(null,null,null,P.k)
this.bo(0)
C.u.gbf(window).aS(this.gbl(this),-1)},
bo:function(a){var z=this.b;(z&&C.b).si(z,0)
z=this.a;(z&&C.h).R(z,"")
this.c9()
this.r.aM(0)
z=this.Q;(z&&C.b).si(z,0)
z=this.z;(z&&C.b).si(z,0)},
a0:function(a){var z,y
z=this.b;(z&&C.b).j(z,a)
z=this.b
y=z.length
if(y>20){z=(z&&C.b).aY(z,y-20)
this.b=z}y=this.a;(y&&C.h).R(y,(z&&C.b).N(z))
z=this.a
y=C.e.a7(z.scrollHeight)
z.toString
z.scrollTop=C.a.a7(y)},
aP:function(a){var z,y,x
H.H(a,"$iso",[F.v],"$aso")
if(a.length!==2)throw H.b(P.B("missingPiece expecting two pieces."))
z=new F.eX(a)
z=H.l([z.$1(new F.eS()),z.$1(new F.eT()),z.$1(new F.eU()),z.$1(new F.eV())],[[P.m,P.k]])
y=P.k
x=H.h(z,0)
return F.f0(new H.P(z,H.f(new F.eW(),{func:1,ret:y,args:[x]}),[x,y]).E(0))},
bj:function(a){var z,y
z=[F.v]
H.H(a,"$ism",z,"$asm")
y=a.length
if(y!==3)return!1
if(1>=y)return H.a(a,1)
return a[1].cj(this.aP(H.l([C.b.gJ(a),C.b.gZ(a)],z)))},
cc:function(){var z,y,x,w,v
z=this.x;(z&&C.b).si(z,0)
for(z=J.S(G.bI(3,[0,1,2,3,4,5,6,7,8]).$0()),y=F.v,x=P.k;z.n();){w=z.gu()
if(this.bj(J.cl(w,new F.eN(this),y).E(0))){v=this.x;(v&&C.b).j(v,P.aX(w,x))}}},
bk:function(a){var z,y,x,w,v,u,t,s,r
z=F.v
H.H(a,"$ism",[z],"$asm")
if(a.length!==4)return!1
x=J.S(G.bI(2,a).$0())
while(!0){if(!x.n()){y=!1
break}w=P.ah(x.gu(),!0,z)
v=this.aP(w)
u=P.ah(a,!0,z)
t=H.f(new F.eP(w),{func:1,ret:P.t,args:[H.h(u,0)]})
if(!!u.fixed$length)H.w(P.I("removeWhere"))
C.b.ag(u,t,!0)
s=this.aP(u)
t=v.a
r=s.a
if(t==null?r==null:t===r){y=!0
break}}return y},
cd:function(){var z,y,x,w,v
z=this.y;(z&&C.b).si(z,0)
for(z=J.S(G.bI(4,[0,1,2,3,4,5,6,7,8]).$0()),y=F.v,x=P.k;z.n();){w=z.gu()
if(this.bk(J.cl(w,new F.eO(this),y).E(0))){v=this.y;(v&&C.b).j(v,P.aX(w,x))}}},
cA:[function(a,b){var z,y,x,w,v,u,t,s,r,q,p,o,n,m
H.i(b,"$isa8")
if(this.x.length===0&&this.y.length===0){this.bo(0)
return}z=this.c
y=z.width
x=z.clientWidth
if(typeof y!=="number")return y.a_()
if(typeof x!=="number")return H.a2(x)
w=z.height
z=z.clientHeight
if(typeof w!=="number")return w.a_()
if(typeof z!=="number")return H.a2(z)
v=J.ae(b)
u=v.gbn(b).a
if(typeof u!=="number")return u.G()
v=v.gbn(b).b
if(typeof v!=="number")return v.G()
t=this.c
s=t.height
if(typeof s!=="number")return s.t()
r=C.e.t(v*(w/z),C.a.k(s,3))
t=t.width
if(typeof t!=="number")return t.t()
q=C.e.t(u*(y/x),C.a.k(t,3))+r*3
z=this.r.v(0,q)
y=this.r
if(z)y.ao(0,q)
else y.j(0,q)
z=this.r
y=z.a
if(y===3){y=F.v
z.toString
x=H.h(z,0)
if(this.bj(P.ah(new H.cA(z,H.f(new F.eH(this),{func:1,ret:y,args:[x]}),[x,y]),!0,y))){z=this.x
y=z.length
o=0
while(!0){if(!(o<z.length)){p=!0
break}n=z[o]
x=H.H(this.r,"$isK",[H.h(n,0)],"$asK")
w=n.aH()
w.B(0,n)
w.B(0,x)
if(w.a===3){p=!1
break}z.length===y||(0,H.b9)(z);++o}if(!p){this.a0("<p>You found <b>a line</b>! Good job!</p>")
z=this.z;(z&&C.b).j(z,this.r.E(0))
z=this.x
z.toString
y=H.f(new F.eI(this),{func:1,ret:P.t,args:[H.h(z,0)]})
if(typeof z!=="object"||z===null||!!z.fixed$length)H.w(P.I("removeWhere"));(z&&C.b).ag(z,y,!0)
this.r.aM(0)
this.aq()}}}else if(y===4){y=F.v
z.toString
x=H.h(z,0)
if(this.bk(P.ah(new H.cA(z,H.f(new F.eJ(this),{func:1,ret:y,args:[x]}),[x,y]),!0,y))){z=this.y
y=z.length
o=0
while(!0){if(!(o<z.length)){p=!0
break}m=z[o]
x=H.H(this.r,"$isK",[H.h(m,0)],"$asK")
w=m.aH()
w.B(0,m)
w.B(0,x)
if(w.a===4){p=!1
break}z.length===y||(0,H.b9)(z);++o}if(!p){this.a0("<p>You found <b>a plane</b>! Good job!</p>")
z=this.Q;(z&&C.b).j(z,this.r.E(0))
z=this.y
z.toString
y=H.f(new F.eK(this),{func:1,ret:P.t,args:[H.h(z,0)]})
if(typeof z!=="object"||z===null||!!z.fixed$length)H.w(P.I("removeWhere"));(z&&C.b).ag(z,y,!0)
this.r.aM(0)
this.aq()}}}},"$1","gca",5,0,26],
cC:[function(a,b){var z,y,x,w,v,u,t,s,r
H.aQ(b)
z=this.c
z.toString
z=z.getContext("2d")
z.fillStyle="white"
y=this.c
z.fillRect(0,0,y.width,y.height)
y=this.d
y.toString
y=y.getContext("2d")
y.fillStyle="white"
z=this.d
y.fillRect(0,0,z.width,z.height)
z=this.e
z.toString
z=z.getContext("2d")
z.fillStyle="white"
y=this.e
z.fillRect(0,0,y.width,y.height)
x=new F.eQ(this,b,1e4)
for(w=0;w<this.f.length;++w)x.$4(this.c,w,C.a.k(w,3),w%3)
for(v=0;z=this.z,v<z.length;++v){u=z[v]
for(t=0;t<u.length;++t){s=u[t]
x.$4(this.d,s,v,t)}}for(v=0;z=this.Q,v<z.length;++v){r=z[v]
for(t=0;t<r.length;++t){s=r[t]
x.$4(this.e,s,v,t)}}C.u.gbf(window).aS(this.gbl(this),-1)},"$1","gbl",5,0,27],
c9:function(){var z,y,x,w
z=P.k
y=F.v
do{x=P.aY(81,new F.eF(),!0,z)
C.b.bw(x)
x=H.fI(x,0,9,H.h(x,0))
w=H.h(x,0)
this.f=new H.P(x,H.f(new F.eG(),{func:1,ret:y,args:[w]}),[w,y]).E(0)
this.cc()
this.cd()
x=this.x.length
if(x>=2)if(x<=3){w=this.y.length
w=w<2||w>3}else w=!0
else w=!0}while(w)
z=$.$get$cg()
if(x>=4)return H.a(z,x)
x="<p class='fancy'>Falco Shapes</p><p class='fancy-sub'>an implementation of Marsha Falco's game of <em>Set</em><br>by Richard Ambler, August 2018</p><p>Welcome to a new game of Falco Shapes!</p><p>There are <b>"+z[x]+" lines</b> and <b>"
y=this.y.length
if(y>=4)return H.a(z,y)
this.a0(x+z[y]+" planes</b> hidden in the  pieces on the board.</p><p>Find them by clicking on the pieces!</p>")
y=this.d
z=this.c
x=z.width
if(typeof x!=="number")return x.t()
y.width=C.a.k(x,3)*3
x=z.height
if(typeof x!=="number")return x.t()
y.height=C.a.k(x,3)*this.x.length
x=this.e
y=z.width
if(typeof y!=="number")return y.t()
x.width=C.a.k(y,3)*4
z=z.height
if(typeof z!=="number")return z.t()
x.height=C.a.k(z,3)*this.y.length},
aq:function(){var z,y,x,w
z=this.x.length
if(z===0&&this.y.length===0)this.a0("<p>That's all the lines and planes!</p><p>Click the board to start a new game!</p>")
else{y=this.y.length
x=$.$get$cg()
if(z>=4)return H.a(x,z)
w="<p>Now there are <b>"+x[z]+" line"
w=w+(z===1?"":"s")+"</b> and <b>"
if(y>=4)return H.a(x,y)
x=w+x[y]+" plane"
this.a0(x+(y===1?"":"s")+"</b> remaining.</p>")}},
l:{
eE:function(a){var z=new F.eD()
z.bG(a)
return z}}},
eL:{"^":"e:15;a",
$1:function(a){var z,y,x,w
H.i(a,"$isa8")
for(z=this.a;y=z.x,x=y.length,x!==0;){w=z.z
if(0>=x)return H.a(y,-1);(w&&C.b).j(w,y.pop().E(0))}z.aq()}},
eM:{"^":"e:15;a",
$1:function(a){var z,y,x,w
H.i(a,"$isa8")
for(z=this.a;y=z.y,x=y.length,x!==0;){w=z.Q
if(0>=x)return H.a(y,-1);(w&&C.b).j(w,y.pop().E(0))}z.aq()}},
eX:{"^":"e:29;a",
$1:function(a){var z,y
H.f(a,{func:1,ret:P.k,args:[F.v]})
z=H.l([0,1,2],[P.k])
y=this.a
C.b.ao(z,a.$1(C.b.gJ(y)))
C.b.ao(z,a.$1(C.b.gZ(y)))
return z}},
eS:{"^":"e:2;",
$1:function(a){var z=H.i(a,"$isv").a
if(typeof z!=="number")return z.t()
return C.a.k(z,27)}},
eT:{"^":"e:2;",
$1:function(a){var z=H.i(a,"$isv").a
if(typeof z!=="number")return z.w()
return C.a.k(C.a.w(z,27),9)}},
eU:{"^":"e:2;",
$1:function(a){return H.i(a,"$isv").gaQ()}},
eV:{"^":"e:2;",
$1:function(a){H.i(a,"$isv")
return a.gar(a)}},
eW:{"^":"e:31;",
$1:function(a){var z,y
z=[P.k]
H.H(a,"$ism",z,"$asm")
y=J.ad(a)
if(y.gi(a)===1)z=y.gJ(a)
else{z=H.l([0,1,2],z)
y=H.f(new F.eR(a),{func:1,ret:P.t,args:[H.h(z,0)]})
C.b.ag(z,y,!0)
z=C.b.gJ(z)}return z}},
eR:{"^":"e:20;a",
$1:function(a){return J.ck(this.a,H.z(a))}},
eN:{"^":"e:19;a",
$1:function(a){var z=this.a.f
H.z(a)
if(a>>>0!==a||a>=z.length)return H.a(z,a)
return z[a]}},
eP:{"^":"e:33;a",
$1:function(a){return C.b.v(this.a,H.i(a,"$isv"))}},
eO:{"^":"e:19;a",
$1:function(a){var z=this.a.f
H.z(a)
if(a>>>0!==a||a>=z.length)return H.a(z,a)
return z[a]}},
eH:{"^":"e:6;a",
$1:function(a){var z
H.z(a)
z=this.a.f
if(a>>>0!==a||a>=z.length)return H.a(z,a)
return z[a]}},
eI:{"^":"e:10;a",
$1:function(a){return H.H(a,"$isK",[P.k],"$asK").bs(this.a.r).a===3}},
eJ:{"^":"e:6;a",
$1:function(a){var z
H.z(a)
z=this.a.f
if(a>>>0!==a||a>=z.length)return H.a(z,a)
return z[a]}},
eK:{"^":"e:10;a",
$1:function(a){return H.H(a,"$isK",[P.k],"$asK").bs(this.a.r).a===4}},
eQ:{"^":"e;a,b,c",
$4:function(a,b,c,d){var z,y,x,w,v,u,t,s,r,q,p
z=$.$get$b8()
y=z.width
if(typeof y!=="number")return y.t()
y=C.a.k(y,3)
x=z.height
if(typeof x!=="number")return x.t()
x=C.a.k(x,3)
w=this.a
v=w.f
if(b>>>0!==b||b>=v.length)return H.a(v,b)
u=v[b]
v=this.b
t=this.c
if(typeof v!=="number")return v.w()
s=C.e.w(v,t)/t*3.141592653589793*2
t=[0,s,-s]
v=u.gaQ()
if(v<0||v>=3)return H.a(t,v)
r=t[v]
v=$.$get$bA()
t=u.gar(u)
if(t<0||t>=3)return H.a(v,t)
q=v[t]
a.toString
t=a.getContext("2d")
t.save()
t.translate((d+0.5)*y,(c+0.5)*x)
v=-x
t=-y
if(w.r.v(0,b))a.getContext("2d").drawImage($.$get$e2(),C.a.k(t,2),C.a.k(v,2))
else a.getContext("2d").drawImage($.$get$dR(),C.a.k(t,2),C.a.k(v,2))
w=a.getContext("2d")
w.rotate(r)
w.shadowColor=T.ch("black",0.2)
w.shadowOffsetX=6
w.shadowOffsetY=6
w.shadowBlur=8
p=u.a
if(typeof p!=="number")return p.t()
w.drawImage(z,C.a.k(p,27)*y,C.a.k(C.a.w(p,27),9)*x,y,x,t/2*q,v/2*q,y*q,x*q)
w.restore()}},
eF:{"^":"e:3;",
$1:function(a){return a}},
eG:{"^":"e:6;",
$1:function(a){return new F.v(H.z(a))}}},1]]
setupProgram(dart,0,0)
J.q=function(a){if(typeof a=="number"){if(Math.floor(a)==a)return J.cG.prototype
return J.cF.prototype}if(typeof a=="string")return J.aU.prototype
if(a==null)return J.f9.prototype
if(typeof a=="boolean")return J.f8.prototype
if(a.constructor==Array)return J.aS.prototype
if(typeof a!="object"){if(typeof a=="function")return J.aV.prototype
return a}if(a instanceof P.c)return a
return J.bw(a)}
J.ad=function(a){if(typeof a=="string")return J.aU.prototype
if(a==null)return a
if(a.constructor==Array)return J.aS.prototype
if(typeof a!="object"){if(typeof a=="function")return J.aV.prototype
return a}if(a instanceof P.c)return a
return J.bw(a)}
J.bv=function(a){if(a==null)return a
if(a.constructor==Array)return J.aS.prototype
if(typeof a!="object"){if(typeof a=="function")return J.aV.prototype
return a}if(a instanceof P.c)return a
return J.bw(a)}
J.b5=function(a){if(typeof a=="number")return J.aT.prototype
if(a==null)return a
if(!(a instanceof P.c))return J.b_.prototype
return a}
J.io=function(a){if(typeof a=="number")return J.aT.prototype
if(typeof a=="string")return J.aU.prototype
if(a==null)return a
if(!(a instanceof P.c))return J.b_.prototype
return a}
J.dU=function(a){if(typeof a=="string")return J.aU.prototype
if(a==null)return a
if(!(a instanceof P.c))return J.b_.prototype
return a}
J.ae=function(a){if(a==null)return a
if(typeof a!="object"){if(typeof a=="function")return J.aV.prototype
return a}if(a instanceof P.c)return a
return J.bw(a)}
J.e5=function(a,b){if(typeof a=="number"&&typeof b=="number")return(a&b)>>>0
return J.b5(a).aW(a,b)}
J.ci=function(a,b){if(typeof a=="number"&&typeof b=="number")return a/b
return J.b5(a).a_(a,b)}
J.ax=function(a,b){if(a==null)return b==null
if(typeof a!="object")return b!=null&&a===b
return J.q(a).O(a,b)}
J.e6=function(a,b){if(typeof a=="number"&&typeof b=="number")return a>b
return J.b5(a).ab(a,b)}
J.cj=function(a,b){if(typeof a=="number"&&typeof b=="number")return a<b
return J.b5(a).S(a,b)}
J.bB=function(a,b){if(typeof a=="number"&&typeof b=="number")return a*b
return J.io(a).G(a,b)}
J.e7=function(a,b){if(typeof b==="number")if(a.constructor==Array||typeof a=="string"||H.iA(a,a[init.dispatchPropertyName]))if(b>>>0===b&&b<a.length)return a[b]
return J.ad(a).m(a,b)}
J.e8=function(a,b,c,d){return J.ae(a).be(a,b,c,d)}
J.ck=function(a,b){return J.ad(a).v(a,b)}
J.bC=function(a,b,c){return J.ad(a).bi(a,b,c)}
J.ba=function(a,b){return J.bv(a).A(a,b)}
J.e9=function(a){return J.ae(a).gc6(a)}
J.ea=function(a){return J.ae(a).gbh(a)}
J.an=function(a){return J.q(a).gD(a)}
J.S=function(a){return J.bv(a).gC(a)}
J.T=function(a){return J.ad(a).gi(a)}
J.eb=function(a){return J.ae(a).gcn(a)}
J.ec=function(a){return J.ae(a).gct(a)}
J.cl=function(a,b,c){return J.bv(a).bm(a,b,c)}
J.cm=function(a){return J.bv(a).cp(a)}
J.bD=function(a){return J.b5(a).a7(a)}
J.ed=function(a){return J.dU(a).cu(a)}
J.ay=function(a){return J.q(a).h(a)}
J.ee=function(a){return J.dU(a).aU(a)}
I.al=function(a){a.immutable$list=Array
a.fixed$length=Array
return a}
var $=I.p
C.l=W.bc.prototype
C.m=W.ef.prototype
C.h=W.eu.prototype
C.x=J.C.prototype
C.b=J.aS.prototype
C.n=J.cF.prototype
C.a=J.cG.prototype
C.e=J.aT.prototype
C.c=J.aU.prototype
C.E=J.aV.prototype
C.J=H.fl.prototype
C.K=W.fn.prototype
C.r=J.fs.prototype
C.t=W.fJ.prototype
C.k=J.b_.prototype
C.u=W.fS.prototype
C.f=new P.f_()
C.v=new P.fr()
C.w=new P.hw()
C.d=new P.hC()
C.y=function(hooks) {
  if (typeof dartExperimentalFixupGetTag != "function") return hooks;
  hooks.getTag = dartExperimentalFixupGetTag(hooks.getTag);
}
C.z=function(hooks) {
  var userAgent = typeof navigator == "object" ? navigator.userAgent : "";
  if (userAgent.indexOf("Firefox") == -1) return hooks;
  var getTag = hooks.getTag;
  var quickMap = {
    "BeforeUnloadEvent": "Event",
    "DataTransfer": "Clipboard",
    "GeoGeolocation": "Geolocation",
    "Location": "!Location",
    "WorkerMessageEvent": "MessageEvent",
    "XMLDocument": "!Document"};
  function getTagFirefox(o) {
    var tag = getTag(o);
    return quickMap[tag] || tag;
  }
  hooks.getTag = getTagFirefox;
}
C.o=function(hooks) { return hooks; }

C.A=function(getTagFallback) {
  return function(hooks) {
    if (typeof navigator != "object") return hooks;
    var ua = navigator.userAgent;
    if (ua.indexOf("DumpRenderTree") >= 0) return hooks;
    if (ua.indexOf("Chrome") >= 0) {
      function confirm(p) {
        return typeof window == "object" && window[p] && window[p].name == p;
      }
      if (confirm("Window") && confirm("HTMLElement")) return hooks;
    }
    hooks.getTag = getTagFallback;
  };
}
C.B=function() {
  var toStringFunction = Object.prototype.toString;
  function getTag(o) {
    var s = toStringFunction.call(o);
    return s.substring(8, s.length - 1);
  }
  function getUnknownTag(object, tag) {
    if (/^HTML[A-Z].*Element$/.test(tag)) {
      var name = toStringFunction.call(object);
      if (name == "[object Object]") return null;
      return "HTMLElement";
    }
  }
  function getUnknownTagGenericBrowser(object, tag) {
    if (self.HTMLElement && object instanceof HTMLElement) return "HTMLElement";
    return getUnknownTag(object, tag);
  }
  function prototypeForTag(tag) {
    if (typeof window == "undefined") return null;
    if (typeof window[tag] == "undefined") return null;
    var constructor = window[tag];
    if (typeof constructor != "function") return null;
    return constructor.prototype;
  }
  function discriminator(tag) { return null; }
  var isBrowser = typeof navigator == "object";
  return {
    getTag: getTag,
    getUnknownTag: isBrowser ? getUnknownTagGenericBrowser : getUnknownTag,
    prototypeForTag: prototypeForTag,
    discriminator: discriminator };
}
C.C=function(hooks) {
  var userAgent = typeof navigator == "object" ? navigator.userAgent : "";
  if (userAgent.indexOf("Trident/") == -1) return hooks;
  var getTag = hooks.getTag;
  var quickMap = {
    "BeforeUnloadEvent": "Event",
    "DataTransfer": "Clipboard",
    "HTMLDDElement": "HTMLElement",
    "HTMLDTElement": "HTMLElement",
    "HTMLPhraseElement": "HTMLElement",
    "Position": "Geoposition"
  };
  function getTagIE(o) {
    var tag = getTag(o);
    var newTag = quickMap[tag];
    if (newTag) return newTag;
    if (tag == "Object") {
      if (window.DataView && (o instanceof window.DataView)) return "DataView";
    }
    return tag;
  }
  function prototypeForTagIE(tag) {
    var constructor = window[tag];
    if (constructor == null) return null;
    return constructor.prototype;
  }
  hooks.getTag = getTagIE;
  hooks.prototypeForTag = prototypeForTagIE;
}
C.D=function(hooks) {
  var getTag = hooks.getTag;
  var prototypeForTag = hooks.prototypeForTag;
  function getTagFixed(o) {
    var tag = getTag(o);
    if (tag == "Document") {
      if (!!o.xmlVersion) return "!Document";
      return "!HTMLDocument";
    }
    return tag;
  }
  function prototypeForTagFixed(tag) {
    if (tag == "Document") return null;
    return prototypeForTag(tag);
  }
  hooks.getTag = getTagFixed;
  hooks.prototypeForTag = prototypeForTagFixed;
}
C.p=function getTagFallback(o) {
  var s = Object.prototype.toString.call(o);
  return s.substring(8, s.length - 1);
}
C.F=H.l(I.al(["*::class","*::dir","*::draggable","*::hidden","*::id","*::inert","*::itemprop","*::itemref","*::itemscope","*::lang","*::spellcheck","*::title","*::translate","A::accesskey","A::coords","A::hreflang","A::name","A::shape","A::tabindex","A::target","A::type","AREA::accesskey","AREA::alt","AREA::coords","AREA::nohref","AREA::shape","AREA::tabindex","AREA::target","AUDIO::controls","AUDIO::loop","AUDIO::mediagroup","AUDIO::muted","AUDIO::preload","BDO::dir","BODY::alink","BODY::bgcolor","BODY::link","BODY::text","BODY::vlink","BR::clear","BUTTON::accesskey","BUTTON::disabled","BUTTON::name","BUTTON::tabindex","BUTTON::type","BUTTON::value","CANVAS::height","CANVAS::width","CAPTION::align","COL::align","COL::char","COL::charoff","COL::span","COL::valign","COL::width","COLGROUP::align","COLGROUP::char","COLGROUP::charoff","COLGROUP::span","COLGROUP::valign","COLGROUP::width","COMMAND::checked","COMMAND::command","COMMAND::disabled","COMMAND::label","COMMAND::radiogroup","COMMAND::type","DATA::value","DEL::datetime","DETAILS::open","DIR::compact","DIV::align","DL::compact","FIELDSET::disabled","FONT::color","FONT::face","FONT::size","FORM::accept","FORM::autocomplete","FORM::enctype","FORM::method","FORM::name","FORM::novalidate","FORM::target","FRAME::name","H1::align","H2::align","H3::align","H4::align","H5::align","H6::align","HR::align","HR::noshade","HR::size","HR::width","HTML::version","IFRAME::align","IFRAME::frameborder","IFRAME::height","IFRAME::marginheight","IFRAME::marginwidth","IFRAME::width","IMG::align","IMG::alt","IMG::border","IMG::height","IMG::hspace","IMG::ismap","IMG::name","IMG::usemap","IMG::vspace","IMG::width","INPUT::accept","INPUT::accesskey","INPUT::align","INPUT::alt","INPUT::autocomplete","INPUT::autofocus","INPUT::checked","INPUT::disabled","INPUT::inputmode","INPUT::ismap","INPUT::list","INPUT::max","INPUT::maxlength","INPUT::min","INPUT::multiple","INPUT::name","INPUT::placeholder","INPUT::readonly","INPUT::required","INPUT::size","INPUT::step","INPUT::tabindex","INPUT::type","INPUT::usemap","INPUT::value","INS::datetime","KEYGEN::disabled","KEYGEN::keytype","KEYGEN::name","LABEL::accesskey","LABEL::for","LEGEND::accesskey","LEGEND::align","LI::type","LI::value","LINK::sizes","MAP::name","MENU::compact","MENU::label","MENU::type","METER::high","METER::low","METER::max","METER::min","METER::value","OBJECT::typemustmatch","OL::compact","OL::reversed","OL::start","OL::type","OPTGROUP::disabled","OPTGROUP::label","OPTION::disabled","OPTION::label","OPTION::selected","OPTION::value","OUTPUT::for","OUTPUT::name","P::align","PRE::width","PROGRESS::max","PROGRESS::min","PROGRESS::value","SELECT::autocomplete","SELECT::disabled","SELECT::multiple","SELECT::name","SELECT::required","SELECT::size","SELECT::tabindex","SOURCE::type","TABLE::align","TABLE::bgcolor","TABLE::border","TABLE::cellpadding","TABLE::cellspacing","TABLE::frame","TABLE::rules","TABLE::summary","TABLE::width","TBODY::align","TBODY::char","TBODY::charoff","TBODY::valign","TD::abbr","TD::align","TD::axis","TD::bgcolor","TD::char","TD::charoff","TD::colspan","TD::headers","TD::height","TD::nowrap","TD::rowspan","TD::scope","TD::valign","TD::width","TEXTAREA::accesskey","TEXTAREA::autocomplete","TEXTAREA::cols","TEXTAREA::disabled","TEXTAREA::inputmode","TEXTAREA::name","TEXTAREA::placeholder","TEXTAREA::readonly","TEXTAREA::required","TEXTAREA::rows","TEXTAREA::tabindex","TEXTAREA::wrap","TFOOT::align","TFOOT::char","TFOOT::charoff","TFOOT::valign","TH::abbr","TH::align","TH::axis","TH::bgcolor","TH::char","TH::charoff","TH::colspan","TH::headers","TH::height","TH::nowrap","TH::rowspan","TH::scope","TH::valign","TH::width","THEAD::align","THEAD::char","THEAD::charoff","THEAD::valign","TR::align","TR::bgcolor","TR::char","TR::charoff","TR::valign","TRACK::default","TRACK::kind","TRACK::label","TRACK::srclang","UL::compact","UL::type","VIDEO::controls","VIDEO::height","VIDEO::loop","VIDEO::mediagroup","VIDEO::muted","VIDEO::preload","VIDEO::width"]),[P.j])
C.G=H.l(I.al(["HEAD","AREA","BASE","BASEFONT","BR","COL","COLGROUP","EMBED","FRAME","FRAMESET","HR","IMAGE","IMG","INPUT","ISINDEX","LINK","META","PARAM","SOURCE","STYLE","TITLE","WBR"]),[P.j])
C.H=H.l(I.al([]),[P.j])
C.i=H.l(I.al(["bind","if","ref","repeat","syntax"]),[P.j])
C.j=H.l(I.al(["A::href","AREA::href","BLOCKQUOTE::cite","BODY::background","COMMAND::icon","DEL::cite","FORM::action","IMG::src","INPUT::src","INS::cite","Q::cite","VIDEO::poster"]),[P.j])
C.I=H.l(I.al(["antiquewhite","aqua","aquamarine","azure","beige","bisque","black","blanchedalmond","blue","blueviolet","brown","burlywood","cadetblue","chartreuse","chocolate","coral","cornflowerblue","cornsilk","crimson","cyan","darkblue","darkcyan","darkgoldenrod","darkgray","darkgreen","darkkhaki","darkmagenta","darkolivegreen","darkorange","darkorchid","darkred","darksalmon","darkseagreen","darkslateblue","darkslategray","darkturquoise","darkviolet","deeppink","deepskyblue","dimgray","dodgerblue","firebrick","floralwhite","forestgreen","fuchsia","gainsboro","ghostwhite","gold","goldenrod","gray","green","greenyellow","honeydew","hotpink","indianred","indigo","ivory","khaki","lavender","lavenderblush","lawngreen","lemonchiffon","lightblue","lightcoral","lightcyan","lightgoldenrodyellow","lightgray","lightgreen","lightpink","lightsalmon","lightseagreen","lightskyblue","lightslategray","lightsteelblue","lightyellow","lime","limegreen","linen","magenta","maroon","mediumaquamarine","mediumblue","mediumorchid","mediumpurple","mediumseagreen","mediumslateblue","mediumspringgreen","mediumturquoise","mediumvioletred","midnightblue","mintcream","mistyrose","moccasin","navajowhite","navy","oldlace","olive","olivedrab","orange","orangered","orchid","palegoldenrod","palegreen","paleturquoise","palevioletred","papayawhip","peachpuff","peru","pink","plum","powderblue","purple","rebeccapurple","red","rosybrown","royalblue","saddlebrown","salmon","sandybrown","seagreen","seashell","sienna","silver","skyblue","slateblue","slategray","snow","springgreen","steelblue","tan","teal","thistle","tomato","turquoise","violet","wheat","white","whitesmoke","yellow","yellowgreen"]),[P.j])
C.q=new H.eq(140,{antiquewhite:"FAEBD7",aqua:"00FFFF",aquamarine:"7FFFD4",azure:"F0FFFF",beige:"F5F5DC",bisque:"FFE4C4",black:"000000",blanchedalmond:"FFEBCD",blue:"0000FF",blueviolet:"8A2BE2",brown:"A52A2A",burlywood:"DEB887",cadetblue:"5F9EA0",chartreuse:"7FFF00",chocolate:"D2691E",coral:"FF7F50",cornflowerblue:"6495ED",cornsilk:"FFF8DC",crimson:"DC143C",cyan:"00FFFF",darkblue:"00008B",darkcyan:"008B8B",darkgoldenrod:"B8860B",darkgray:"A9A9A9",darkgreen:"006400",darkkhaki:"BDB76B",darkmagenta:"8B008B",darkolivegreen:"556B2F",darkorange:"FF8C00",darkorchid:"9932CC",darkred:"8B0000",darksalmon:"E9967A",darkseagreen:"8FBC8F",darkslateblue:"483D8B",darkslategray:"2F4F4F",darkturquoise:"00CED1",darkviolet:"9400D3",deeppink:"FF1493",deepskyblue:"00BFFF",dimgray:"696969",dodgerblue:"1E90FF",firebrick:"B22222",floralwhite:"FFFAF0",forestgreen:"228B22",fuchsia:"FF00FF",gainsboro:"DCDCDC",ghostwhite:"F8F8FF",gold:"FFD700",goldenrod:"DAA520",gray:"808080",green:"008000",greenyellow:"ADFF2F",honeydew:"F0FFF0",hotpink:"FF69B4",indianred:"CD5C5C",indigo:"4B0082",ivory:"FFFFF0",khaki:"F0E68C",lavender:"E6E6FA",lavenderblush:"FFF0F5",lawngreen:"7CFC00",lemonchiffon:"FFFACD",lightblue:"ADD8E6",lightcoral:"F08080",lightcyan:"E0FFFF",lightgoldenrodyellow:"FAFAD2",lightgray:"D3D3D3",lightgreen:"90EE90",lightpink:"FFB6C1",lightsalmon:"FFA07A",lightseagreen:"20B2AA",lightskyblue:"87CEFA",lightslategray:"778899",lightsteelblue:"B0C4DE",lightyellow:"FFFFE0",lime:"00FF00",limegreen:"32CD32",linen:"FAF0E6",magenta:"FF00FF",maroon:"800000",mediumaquamarine:"66CDAA",mediumblue:"0000CD",mediumorchid:"BA55D3",mediumpurple:"9370DB",mediumseagreen:"3CB371",mediumslateblue:"7B68EE",mediumspringgreen:"00FA9A",mediumturquoise:"48D1CC",mediumvioletred:"C71585",midnightblue:"191970",mintcream:"F5FFFA",mistyrose:"FFE4E1",moccasin:"FFE4B5",navajowhite:"FFDEAD",navy:"000080",oldlace:"FDF5E6",olive:"808000",olivedrab:"6B8E23",orange:"FFA500",orangered:"FF4500",orchid:"DA70D6",palegoldenrod:"EEE8AA",palegreen:"98FB98",paleturquoise:"AFEEEE",palevioletred:"DB7093",papayawhip:"FFEFD5",peachpuff:"FFDAB9",peru:"CD853F",pink:"FFC0CB",plum:"DDA0DD",powderblue:"B0E0E6",purple:"800080",rebeccapurple:"663399",red:"FF0000",rosybrown:"BC8F8F",royalblue:"4169E1",saddlebrown:"8B4513",salmon:"FA8072",sandybrown:"F4A460",seagreen:"2E8B57",seashell:"FFF5EE",sienna:"A0522D",silver:"C0C0C0",skyblue:"87CEEB",slateblue:"6A5ACD",slategray:"708090",snow:"FFFAFA",springgreen:"00FF7F",steelblue:"4682B4",tan:"D2B48C",teal:"008080",thistle:"D8BFD8",tomato:"FF6347",turquoise:"40E0D0",violet:"EE82EE",wheat:"F5DEB3",white:"FFFFFF",whitesmoke:"F5F5F5",yellow:"FFFF00",yellowgreen:"9ACD32"},C.I,[P.j,P.j])
C.L=new P.bm(null,2)
$.W=0
$.az=null
$.cq=null
$.c7=!1
$.dW=null
$.dO=null
$.e1=null
$.bt=null
$.bx=null
$.cd=null
$.at=null
$.aJ=null
$.aK=null
$.c8=!1
$.A=C.d
$.dh=null
$.di=null
$.dj=null
$.dk=null
$.c2=null
$.dl=null
$.bk=null
$.dm=null
$.a5=null
$.bK=null
$.cC=null
$.cB=null
$.cy=null
$.cx=null
$.cw=null
$.cv=null
$=null
init.isHunkLoaded=function(a){return!!$dart_deferred_initializers$[a]}
init.deferredInitialized=new Object(null)
init.isHunkInitialized=function(a){return init.deferredInitialized[a]}
init.initializeLoadedHunk=function(a){var z=$dart_deferred_initializers$[a]
if(z==null)throw"DeferredLoading state error: code with hash '"+a+"' was not loaded"
z($globals$,$)
init.deferredInitialized[a]=true}
init.deferredLibraryParts={}
init.deferredPartUris=[]
init.deferredPartHashes=[];(function(a){for(var z=0;z<a.length;){var y=a[z++]
var x=a[z++]
var w=a[z++]
I.$lazy(y,x,w)}})(["cu","$get$cu",function(){return H.dV("_$dart_dartClosure")},"bN","$get$bN",function(){return H.dV("_$dart_js")},"cZ","$get$cZ",function(){return H.Y(H.bj({
toString:function(){return"$receiver$"}}))},"d_","$get$d_",function(){return H.Y(H.bj({$method$:null,
toString:function(){return"$receiver$"}}))},"d0","$get$d0",function(){return H.Y(H.bj(null))},"d1","$get$d1",function(){return H.Y(function(){var $argumentsExpr$='$arguments$'
try{null.$method$($argumentsExpr$)}catch(z){return z.message}}())},"d5","$get$d5",function(){return H.Y(H.bj(void 0))},"d6","$get$d6",function(){return H.Y(function(){var $argumentsExpr$='$arguments$'
try{(void 0).$method$($argumentsExpr$)}catch(z){return z.message}}())},"d3","$get$d3",function(){return H.Y(H.d4(null))},"d2","$get$d2",function(){return H.Y(function(){try{null.$method$}catch(z){return z.message}}())},"d8","$get$d8",function(){return H.Y(H.d4(void 0))},"d7","$get$d7",function(){return H.Y(function(){try{(void 0).$method$}catch(z){return z.message}}())},"bZ","$get$bZ",function(){return P.fU()},"aL","$get$aL",function(){return[]},"L","$get$L",function(){return P.aH(0)},"aa","$get$aa",function(){return P.aH(1)},"dq","$get$dq",function(){return P.aH(2)},"c3","$get$c3",function(){return $.$get$aa().P(0)},"dd","$get$dd",function(){return P.aH(1e4)},"de","$get$de",function(){return H.fm(8)},"ct","$get$ct",function(){return{}},"dt","$get$dt",function(){return P.aX(["A","ABBR","ACRONYM","ADDRESS","AREA","ARTICLE","ASIDE","AUDIO","B","BDI","BDO","BIG","BLOCKQUOTE","BR","BUTTON","CANVAS","CAPTION","CENTER","CITE","CODE","COL","COLGROUP","COMMAND","DATA","DATALIST","DD","DEL","DETAILS","DFN","DIR","DIV","DL","DT","EM","FIELDSET","FIGCAPTION","FIGURE","FONT","FOOTER","FORM","H1","H2","H3","H4","H5","H6","HEADER","HGROUP","HR","I","IFRAME","IMG","INPUT","INS","KBD","LABEL","LEGEND","LI","MAP","MARK","MENU","METER","NAV","NOBR","OL","OPTGROUP","OPTION","OUTPUT","P","PRE","PROGRESS","Q","S","SAMP","SECTION","SELECT","SMALL","SOURCE","SPAN","STRIKE","STRONG","SUB","SUMMARY","SUP","TABLE","TBODY","TD","TEXTAREA","TFOOT","TH","THEAD","TIME","TR","TRACK","TT","U","UL","VAR","VIDEO","WBR"],P.j)},"c4","$get$c4",function(){return P.cK(P.j,P.aR)},"b3","$get$b3",function(){return P.cT("[^0-9,.]",!0,!1)},"br","$get$br",function(){return P.cT("[^#0-9a-fA-F]",!0,!1)},"bo","$get$bo",function(){return P.cK(P.k,P.a4)},"dS","$get$dS",function(){return H.l(["indianred","darkolivegreen","indigo"],[P.j])},"b8","$get$b8",function(){return new F.ik().$0()},"bA","$get$bA",function(){return H.l([1,0.75,0.5],[P.r])},"dR","$get$dR",function(){return new F.ii().$0()},"e2","$get$e2",function(){return new F.ij().$0()},"cg","$get$cg",function(){return H.l(["no","a","two","three"],[P.j])}])
I=I.$finishIsolateConstructor(I)
$=new I()
init.metadata=[]
init.types=[{func:1,ret:P.D},{func:1,ret:-1},{func:1,ret:P.k,args:[F.v]},{func:1,ret:P.k,args:[P.k]},{func:1,ret:P.j,args:[P.k]},{func:1,ret:-1,args:[{func:1,ret:-1}]},{func:1,ret:F.v,args:[P.k]},{func:1,ret:W.bH},{func:1,ret:P.t,args:[W.n]},{func:1,ret:P.t,args:[W.x,P.j,P.j,W.b1]},{func:1,ret:P.t,args:[[P.K,P.k]]},{func:1,ret:P.k,args:[P.k,P.k]},{func:1,ret:P.D,args:[,]},{func:1,args:[,]},{func:1,ret:P.j,args:[P.r]},{func:1,ret:P.D,args:[W.a8]},{func:1,ret:P.t,args:[W.X]},{func:1,ret:P.t,args:[P.j]},{func:1,ret:P.t,args:[P.r]},{func:1,ret:F.v,args:[P.c]},{func:1,ret:P.t,args:[P.k]},{func:1,ret:P.j,args:[P.j]},{func:1,ret:-1,args:[W.a6]},{func:1,ret:P.D,args:[P.r]},{func:1,ret:P.D,args:[,,]},{func:1,ret:[P.a_,,],args:[,]},{func:1,ret:-1,args:[W.a8]},{func:1,ret:-1,args:[P.r]},{func:1,ret:P.D,args:[,],opt:[,]},{func:1,ret:[P.m,P.k],args:[{func:1,ret:P.k,args:[F.v]}]},{func:1,ret:-1,args:[P.c],opt:[P.U]},{func:1,ret:P.k,args:[[P.m,P.k]]},{func:1,ret:-1,args:[W.n,W.n]},{func:1,ret:P.t,args:[F.v]},{func:1,ret:P.D,args:[{func:1,ret:-1}]},{func:1,args:[P.j]},{func:1,args:[,P.j]},{func:1,ret:P.r,args:[P.j],opt:[{func:1,ret:P.r,args:[P.j]}]},{func:1,ret:W.x,args:[W.n]}]
function convertToFastObject(a){function MyClass(){}MyClass.prototype=a
new MyClass()
return a}function convertToSlowObject(a){a.__MAGIC_SLOW_PROPERTY=1
delete a.__MAGIC_SLOW_PROPERTY
return a}A=convertToFastObject(A)
B=convertToFastObject(B)
C=convertToFastObject(C)
D=convertToFastObject(D)
E=convertToFastObject(E)
F=convertToFastObject(F)
G=convertToFastObject(G)
H=convertToFastObject(H)
J=convertToFastObject(J)
K=convertToFastObject(K)
L=convertToFastObject(L)
M=convertToFastObject(M)
N=convertToFastObject(N)
O=convertToFastObject(O)
P=convertToFastObject(P)
Q=convertToFastObject(Q)
R=convertToFastObject(R)
S=convertToFastObject(S)
T=convertToFastObject(T)
U=convertToFastObject(U)
V=convertToFastObject(V)
W=convertToFastObject(W)
X=convertToFastObject(X)
Y=convertToFastObject(Y)
Z=convertToFastObject(Z)
function init(){I.p=Object.create(null)
init.allClasses=map()
init.getTypeFromName=function(a){return init.allClasses[a]}
init.interceptorsByTag=map()
init.leafTags=map()
init.finishedClasses=map()
I.$lazy=function(a,b,c,d,e){if(!init.lazies)init.lazies=Object.create(null)
init.lazies[a]=b
e=e||I.p
var z={}
var y={}
e[a]=z
e[b]=function(){var x=this[a]
if(x==y)H.iJ(d||a)
try{if(x===z){this[a]=y
try{x=this[a]=c()}finally{if(x===z)this[a]=null}}return x}finally{this[b]=function(){return this[a]}}}}
I.$finishIsolateConstructor=function(a){var z=a.p
function Isolate(){var y=Object.keys(z)
for(var x=0;x<y.length;x++){var w=y[x]
this[w]=z[w]}var v=init.lazies
var u=v?Object.keys(v):[]
for(var x=0;x<u.length;x++)this[v[u[x]]]=null
function ForceEfficientMap(){}ForceEfficientMap.prototype=this
new ForceEfficientMap()
for(var x=0;x<u.length;x++){var t=v[u[x]]
this[t]=z[t]}}Isolate.prototype=a.prototype
Isolate.prototype.constructor=Isolate
Isolate.p=z
Isolate.al=a.al
Isolate.cc=a.cc
return Isolate}}!function(){var z=function(a){var t={}
t[a]=1
return Object.keys(convertToFastObject(t))[0]}
init.getIsolateTag=function(a){return z("___dart_"+a+init.isolateTag)}
var y="___dart_isolate_tags_"
var x=Object[y]||(Object[y]=Object.create(null))
var w="_ZxYxX"
for(var v=0;;v++){var u=z(w+"_"+v+"_")
if(!(u in x)){x[u]=1
init.isolateTag=u
break}}init.dispatchPropertyName=init.getIsolateTag("dispatch_record")}();(function(a){if(typeof document==="undefined"){a(null)
return}if(typeof document.currentScript!='undefined'){a(document.currentScript)
return}var z=document.scripts
function onLoad(b){for(var x=0;x<z.length;++x)z[x].removeEventListener("load",onLoad,false)
a(b.target)}for(var y=0;y<z.length;++y)z[y].addEventListener("load",onLoad,false)})(function(a){init.currentScript=a
if(typeof dartMainRunner==="function")dartMainRunner(F.dZ,[])
else F.dZ([])})})()
//# sourceMappingURL=main.dart.js.map

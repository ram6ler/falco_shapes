# Falco Shapes

Welcome to *Falco Shapes*, an implementation of Marsha Falco's [combinatorics game, Set](https://en.wikipedia.org/wiki/Set_(card_game)), created to demonstrate some use cases for the Dart library, [trotter](https://bitbucket.org/ram6ler/dart_trotter).

You can play the demo [here](https://falco-shapes.netlify.com/).
